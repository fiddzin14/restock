$('body').on('click', '.modal-show', function(event) {
    event.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');

    $('#modal-title').text(title);
    $('#btn-save').text(me.hasClass('edit') ? 'Update' : 'Create');

    $.ajax({
        url: url,
        dataType: 'html',
        success: function(response) {
            $('#modal-body').html(response);
        }
    });
    $('#modals').modal('show');
});

$('body').on('click','#btn-save',function (event) {
    event.preventDefault();

    var form = $('#form'),
        url = form.attr('action'),
        title = form.attr('title'),
        method = 'POST';

    form.find('.form-text').remove();
    form.find('.form-group').removeClass('has-error');
    form.find('.form-control').removeClass('is-invalid');

    $.ajax({
        url: url,
        method: method,
        data: form.serialize(),
        success: function (response) {
            if (response.status == false) {
                $.each(response.errors, function (key, value) {
                    $('.form-group').addClass('has-error');
                    $('#' + key)
                        .closest('.form-control')
                        .addClass('is-invalid')
                        .closest('.col-inp')
                        .addClass('has-error')
                        .append('<span class="form-text text-danger"><strong>' + value + '</strong></span>')
                });
            }else{
                form.trigger('reset');
                $('#addModal .modal-content #select2-warehouse-container').html('<span class="select2-selection__placeholder">Select a State</span>').attr('title', "");
                $('#addModal .modal-content #select2-slot-container').html('<span class="select2-selection__placeholder">Select a State</span>').attr('title', "");
                $('#addModal .modal-content #slot').empty();
                $('#card-title').text(title);
                $('.out').click();
                $('#addModal').modal('hide');
                $('#table').DataTable().ajax.reload();       
    
                Swal.fire({
                    type: 'success',
                    title: 'success!',
                    text: 'Data has been saved!',
                    confirmButtonClass: 'btn btn-primary',
                    timer:2000,
                });
            }
        },
        error: function (xhr) {
            Swal.fire({
                type: 'error',
                title: 'Error...!',
                confirmButtonClass: 'btn btn-primary',
                text: 'Something went wrong!',
            });
        }
    });
});

$('body').on('click', '.btn-delete', function (event) {
    event.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');

    Swal.fire({
        title: 'Are you sure?',
        text: "you won't to revert this data!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function(result) {
        if(result.value) {
            $.ajax({
                url: url,
                success: function (response) {
                        $('#table').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'success!',
                            text: 'Data has been deleted!',
                            type: 'success',
                            confirmButtonClass: 'btn btn-primary',
                            timer: 2000
                        });
                },
                error: function (xhr) {
                    Swal.fire({
                        type: 'error',
                        title: 'Error...!',
                        confirmButtonClass: 'btn btn-primary',
                        text: 'Something went wrong!',
                    });
                }
            });
        }
    });
});

$('body').on('click', '#btback', function (event) {
    event.preventDefault();

    var item = $('.item').val();
    var url = $(this).attr('href');

    if (item == '' || item == null) {
        window.location.href = url;
    }else{
        Swal.fire({
            title: 'Are you sure?',
            text: "your transaction progress will be lose!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Leave!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                window.location.href = url;
            }
        });
    }
});
// $(document).ajaxStart(function() {
//     $('#loading').show();
// }).ajaxStop(function() {
//     $('#loading').hide();
// });
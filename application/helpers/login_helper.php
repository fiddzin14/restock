<?php

function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('username')) {
        redirect('auth');
    } else {
        $user = $ci->db->get_where('login_web', ['username' => $ci->session->userdata('username')])->row();
        if ($ci->session->userdata('level') != "ADMIN") {
            $loginlevel = $user->loginlevel;
            $url = $ci->uri->segment(1);

            $queryMenu = $ci->db->get_where('user_sub_menu', ['url' => $url, 'isactive' => 1])->row();
            $subid = $queryMenu->subid;

            $userAccess = $ci->db->get_where('user_access_menu', [
                'loginlevel' => $loginlevel,
                'sub_menu_id' => $subid
            ]);

            if ($userAccess->num_rows() < 1) {
                show_404();
            }
        }
    }

    function check_access($loginlevel, $menu_id)
    {
        $ci = get_instance();

        $ci->db->where('loginlevel', $loginlevel);
        $ci->db->where('menu_id', $menu_id);
        $result = $ci->db->get('user_access_menu');

        if ($result->num_rows() > 0) {
            return "checked='checked'";
        }
    }
}

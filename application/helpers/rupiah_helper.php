<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!function_exists('rupiah')) {
    function rupiah($value)
    {
        $hasil_rupiah = "Rp  " . number_format($value, 0, ',', ',') . ",-";
        return $hasil_rupiah;
    }
    function nominal($value)
    {
        $hasil = number_format($value, 0, ',', ',');
        return $hasil;
    }
}

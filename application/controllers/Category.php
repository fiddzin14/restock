<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Category_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Category Item";
        $data['action'] = base_url('category/add');
        $data['action_edit'] = base_url('category/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('category/index');
        $this->load->view('_partials/footer');
        $this->load->view('category/js');
    }

    public function ajax_list()
    {
        $list = $this->Category_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $category) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $category->catid;
            $row[] = $category->catname;
            $row[] = $category->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';

            //add html for action
            $row[] = '<a href="' . base_url() . 'category/edit?catid=' . strtr(base64_encode($category->catid), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit Class"><i class="fa fa-pencil mr-2"></i>Edit</a>&nbsp; 
            <a href="category/delete?catid=' . strtr(base64_encode($category->catid), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-2"></i>Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Category_model->count_all(),
            "recordsFiltered" => $this->Category_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('catid', 'Category ID', 'required|trim|is_unique[category.catid]', [
            'is_unique' => 'This Category ID has already exist'
        ]);
        $this->form_validation->set_rules('catname', 'Category Name', 'required|trim|is_unique[category.catname]', [
            'is_unique' => 'This Category name has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Category_model->addCategory();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $catid = base64_decode(strtr($this->input->get('catid'), '._-', '+/='));
        if ($catid == null) {
            return redirect('category');
        }
        $data = $this->Category_model->getById($catid);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('catname', 'Category Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Category_model->editCategory();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $catid = base64_decode(strtr($this->input->get('catid'), '._-', '+/='));
        if ($catid == null) {
            return redirect('category');
        }
        return $this->Category_model->deleteCategory($catid);
    }
}

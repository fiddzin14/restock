<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Movement extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'Movement_model',
            'Auth_model',
            'Warehouse_model',
            'Cust_model',
            'SlotRack_model',
            'Product_model',
        ]);
        $this->load->helper('rupiah_helper');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['date'] = $this->Movement_model->getDate();
        $data['title'] = "Stock Movement";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('movement/index');
        $this->load->view('_partials/footer');
        $this->load->view('movement/js');
    }

    public function ajax_list()
    {
        $list = $this->Movement_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dt->invusenum;
            $row[] = $dt->description;
            $row[] = date('d M y', strtotime($dt->usedate));
            $row[] = $dt->fromwarehouse;
            $row[] = $dt->towarehouse;
            $row[] = $dt->status;

            if ($dt->status == "IN PROGRESS") {
                $link = '<a href="' . base_url() . 'movement/complete?id=' . strtr(base64_encode($dt->invusenum), '+/=', '._-') . '" class="dropdown-item" id="btcomplete"><i class="fa fa-check"></i> Completion</a>';
            } elseif ($dt->status == "DRAFT") {
                $link = '<a href="' . base_url() . 'movement/edit?id=' . strtr(base64_encode($dt->invusenum), '+/=', '._-') . '" class="dropdown-item" id="btedit"><i class="fas fa-pencil"></i> Edit</a>';
            } else {
                $link = '';
            }

            //add html for action
            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'movement/detail?id=' . strtr(base64_encode($dt->invusenum), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'movement/print?id=' . strtr(base64_encode($dt->invusenum), '+/=', '._-') . '" target="_blank" class="dropdown-item"><i class="icon-file-pdf"></i> Print</a>
                                ' . $link . '
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Movement_model->count_all(),
            "recordsFiltered" => $this->Movement_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function complete()
    {
        $invusenum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($invusenum == null) {
            return redirect('movement');
        }
        $response = true;
        $this->Movement_model->completeTrans($invusenum);
        echo json_encode($response);
    }

    public function getSlotByWarehouse()
    {
        $warehouse = $this->input->post('warehouse', true);
        $data['slot'] = $this->SlotRack_model->getSlotByWarehouse($warehouse);
        $data['warehouse'] = $this->Warehouse_model->getToWarehouse($warehouse);
        echo json_encode($data);
    }

    public function getItemBySlot()
    {
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $data = $this->Product_model->getItemByWarehouse($warehouse, $slot);
        echo json_encode($data);
    }

    public function getSelectedItem()
    {
        $fromWarehouse = $this->input->post('fromWarehouse', true);
        $fromSlot = $this->input->post('fromSlot', true);
        $toWarehouse = $this->input->post('toWarehouse', true);
        $toSlot = $this->input->post('toSlot', true);
        $kditem = $this->input->post('item', true);
        $selectItem = $this->input->post('itemSelect', true);
        $Sitem = [];
        if ($selectItem != '') {
            for ($i = 0; $i < count($selectItem); $i++) {
                $Sitem[] = $selectItem[$i]['value'];
            }
        }

        if (in_array($kditem, $Sitem)) {
            $data['error'] = true;
        } else {
            $data['item'] = $this->Product_model->getSelectedItem($kditem);
            $data['detail_from'] = $this->Product_model->getDetail($kditem, $fromWarehouse, $fromSlot)->row();
            $data['detail_to'] = $this->Product_model->getDetail($kditem, $toWarehouse, $toSlot)->row();
        }
        echo json_encode($data);
    }

    public function checkQty()
    {
        $kditem = $this->input->post('item', true);
        $slot = $this->input->post('slot', true);
        $warehouse = $this->input->post('warehouse', true);
        $qty = $this->input->post('qty', true);

        $data = $this->Product_model->getDetail($kditem, $warehouse, $slot)->row();

        if ($qty <= $data->curbal) {
            $response['status'] = true;
        } else {
            $response['status'] = false;
        }
        $response['data'] = $data;
        echo json_encode($response);
    }

    function getAutocomplete()
    {
        $key = $this->input->post('term');
        $item = $this->input->post('item');
        $warehouse = $this->input->post('warehouse');
        $slot = $this->input->post('slot');
        if ($key) {
            $result = $this->Product_model->searchItemByWarehouse($key, $item, $warehouse, $slot);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $data[] = [
                        'label' => $row->kditem . ' ' . $row->itemname,
                    ];
                echo json_encode($data);
            }
        }
    }

    public function add()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['customer'] = $this->Cust_model->getCustomer();
        $data['title'] = "Move Stock";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('movement/add');
        $this->load->view('_partials/footer');
        $this->load->view('movement/js');
    }

    public function create()
    {
        $this->Auth_model->getSales($this->session->userdata('username'));
        $item = $this->input->post('item');
        $quant = $this->input->post('qty');

        if ($item) {
            $product = [];
            $qty = [];
            for ($i = 0; $i < count($item); $i++) {
                $product[] = $item[$i];
                $qty[] = $quant[$i];
            }
        }

        $this->form_validation->set_rules('fromWarehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('toWarehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('fromSlot', 'Slot', 'required');
        $this->form_validation->set_rules('toSlot', 'Slot', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array('', $product)) {
            $response['status'] = false;
            $response['error_item'] = true;
        } else if (in_array(0, $qty)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Movement_model->addTrans();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function detail()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $invusenum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($invusenum == null) {
            return redirect('movement');
        }

        $data['row'] = $this->Movement_model->getById($invusenum);
        $data['item'] = $this->Movement_model->getDetailTrans($invusenum)->result();
        $data['no'] = 1;
        $data['title'] = "Detail Transaction";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('movement/detail', $data);
    }

    public function print()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $invusenum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($invusenum == null) {
            return redirect('movement');
        }

        $data['row'] = $this->Movement_model->getById($invusenum);
        $data['item'] = $this->Movement_model->getDetailTrans($invusenum);
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "Stock_movement.pdf";
        $this->pdf->load_view('movement/print_trans', $data);
    }

    public function edit()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $invusenum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($invusenum == null) {
            return redirect('movement');
        }
        $row = $this->Movement_model->getById($invusenum);
        if ($row->status != "DRAFT") {
            return redirect('movement');
        }

        $query = $this->Movement_model->getDetailTrans($invusenum);
        $itemWh = $this->Movement_model->getItemWhByTrans($invusenum, $row->towarehouse);

        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['slot'] = $this->SlotRack_model->getSlotByWarehouse($row->fromwarehouse);
        $data['slot_to'] = $this->SlotRack_model->getSlotByWarehouse($row->towarehouse);
        $data['customer'] = $this->Cust_model->getCustomer();
        $data['item'] = $this->Movement_model->getItemByTrans($invusenum);
        $data['detailFrom'] = $this->Movement_model->getItemWhByTrans($invusenum, $row->fromwarehouse)->result();
        $data['detail_to'] = $itemWh->num_rows() > 0 ? $itemWh->result() : '';
        $data['row'] = $row;
        $data['detail'] = $query->result();
        $data['no'] = 0;
        $data['num'] = $query->num_rows();
        $data['title'] = "Edit Transaction";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('movement/edit');
        $this->load->view('_partials/footer');
        $this->load->view('movement/js');
    }

    public function update()
    {
        $this->Auth_model->getSales($this->session->userdata('username'));
        $item = $this->input->post('item');
        $quant = $this->input->post('qty');

        if ($item) {
            $product = [];
            $qty = [];
            for ($i = 0; $i < count($item); $i++) {
                $product[] = $item[$i];
                $qty[] = $quant[$i];
            }
        }

        $this->form_validation->set_rules('fromWarehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('toWarehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('fromSlot', 'Slot', 'required');
        $this->form_validation->set_rules('toSlot', 'Slot', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array('', $product)) {
            $response['status'] = false;
            $response['error_item'] = true;
        } else if (in_array(0, $qty)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Movement_model->editTrans();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    // public function delete()
    // {
    //     $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
    //     if ($idwarehouse == null) {
    //         return redirect('warehouse');
    //     }
    //     return $this->Movement_model->deleteWarehouse($idwarehouse);
    // }
}

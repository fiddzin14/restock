<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class SubMenu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('SubMenu_model');
        $this->load->model('Menu_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['menu'] = $this->Menu_model->getAll();
        $data['title'] = "Sub Menu";
        $data['action'] = base_url('subMenu/add');
        $data['action_edit'] = base_url('subMenu/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('subMenu/index');
        $this->load->view('_partials/footer');
        $this->load->view('subMenu/js');
    }

    public function ajax_list()
    {
        $list = $this->SubMenu_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->menu;
            $row[] = $dt->title;
            $row[] = $dt->url;
            $row[] = $dt->icon;
            $row[] = $dt->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';

            //add html for action
            $row[] = '<a href="' . base_url() . 'subMenu/edit?id=' . strtr(base64_encode($dt->subid), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit Sub Menu"><i class="fa fa-pencil mr-2"></i>Edit</a>&nbsp; 
            <a href="subMenu/delete?id=' . strtr(base64_encode($dt->subid), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-2"></i>Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->SubMenu_model->count_all(),
            "recordsFiltered" => $this->SubMenu_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('url', 'URL', 'required|trim');
        $this->form_validation->set_rules('icon', 'Icon', 'required|trim');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->SubMenu_model->addSub();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('subMenu');
        }
        $data = $this->SubMenu_model->getById($id);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('url', 'URL', 'required|trim');
        $this->form_validation->set_rules('icon', 'Icon', 'required|trim');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->SubMenu_model->editSub();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('subMenu');
        }
        return $this->SubMenu_model->deleteSUb($id);
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Receive extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'Receive_model',
            'Auth_model',
            'Warehouse_model',
            'SlotRack_model',
            'Product_model',
            'Suppl_model',
        ]);
        $this->load->helper('rupiah_helper');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['date'] = $this->Receive_model->getDate();
        $data['title'] = "Purchase Receiving";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('receive/index');
        $this->load->view('_partials/footer');
        $this->load->view('receive/js');
    }

    public function ajax_list()
    {
        $list = $this->Receive_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dt->reqnum;
            $row[] = $dt->descr;
            $row[] = date('d M y', strtotime($dt->reqdate));
            $row[] = $dt->descsupplier;
            $row[] = $dt->descwarehouse;
            $row[] = $dt->actionby;
            $row[] = $dt->stat;

            //add html for action
            if ($dt->stat == "OPEN" or $dt->stat == "PARTIAL") {
                $link = '<a href="' . base_url() . 'receive/getReceive?id=' . strtr(base64_encode($dt->reqnum), '+/=', '._-') . '" class="dropdown-item" id="btreceive"><i class="fas fa-download"></i> Receive</a>
                <a href="' . base_url() . 'receive/complete?id=' . strtr(base64_encode($dt->reqnum), '+/=', '._-') . '" class="dropdown-item" id="btcomplete"><i class="fas fa-check"></i> Complete</a>';
            } elseif ($dt->stat == "DRAFT") {
                $link = '<a href="' . base_url() . 'receive/edit?id=' . strtr(base64_encode($dt->reqnum), '+/=', '._-') . '" class="dropdown-item" id="btedit"><i class="fas fa-pencil"></i> Edit</a>';
            } else {
                $link = '';
            }

            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'receive/detail?id=' . strtr(base64_encode($dt->reqnum), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'receive/print_receive?id=' . strtr(base64_encode($dt->reqnum), '+/=', '._-') . '" target="_blank" class="dropdown-item"><i class="icon-file-pdf"></i> Print</a>
                                ' . $link . '
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Receive_model->count_all(),
            "recordsFiltered" => $this->Receive_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function getSlotByWarehouse()
    {
        $warehouse = $this->input->post('warehouse', true);
        $data = $this->SlotRack_model->getSlotByWarehouse($warehouse);
        echo json_encode($data);
    }

    public function getAllItem()
    {
        $data = $this->Product_model->getAllItem();
        echo json_encode($data);
    }

    public function getSelectedItem()
    {
        $slot = $this->input->post('slot', true);
        $warehouse = $this->input->post('warehouse', true);
        $kditem = $this->input->post('item', true);
        $selectItem = $this->input->post('itemSelect', true);
        $Sitem = [];
        if ($selectItem != '') {
            for ($i = 0; $i < count($selectItem); $i++) {
                $Sitem[] = $selectItem[$i]['value'];
            }
        }

        if (in_array($kditem, $Sitem)) {
            $data['error'] = true;
        } else {
            $data['row'] = $this->Product_model->getSelectedItem($kditem);
            $data['detail'] = $this->Product_model->getDetail($kditem, $warehouse, $slot)->row();
        }
        echo json_encode($data);
    }

    public function complete()
    {
        $reqnum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($reqnum == null) {
            return redirect('receive');
        }
        // $row = $this->db->get_where('invreqdt', ['reqnum' => $reqnum])->result();
        // $qty = [];
        // foreach ($row as $dt) {
        //     $qty[] = $dt->qty;
        // }
        $response = true;
        $this->Receive_model->completeTrans($reqnum);
        echo json_encode($response);
    }

    public function getReceive()
    {
        $reqnum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($reqnum == null) {
            return redirect('receive');
        }
        $data = $this->Receive_model->getReceive($reqnum);
        echo json_encode($data);
    }

    public function searchReceiveItem()
    {
        $reqnum = $this->input->post('reqnum', true);
        $key = $this->input->post('key', true);

        $data = $this->Receive_model->searchReceive($reqnum, $key);

        if ($key == "") {
            $response['status'] = true;
            $response['data'] = $this->Receive_model->getReceive($reqnum);
        } elseif ($data) {
            $response['status'] = true;
            $response['data'] = $this->Receive_model->searchReceive($reqnum, $key);
        } else {
            $response['status'] = false;
        }

        echo json_encode($response);
    }

    public function receiveItem()
    {
        $reqnum = $this->input->post('reqnum');
        $qtyreceive = $this->input->post('qtyreceive');

        if ($qtyreceive) {
            $qty = [];
            for ($i = 0; $i < count($qtyreceive); $i++) {
                $qty[] = $qtyreceive[$i];
            }
        }

        if (in_array('', $qty)) {
            $response['status'] = false;
            $response['error'] = true;
        } else {
            $response['status'] = true;
            return $this->Receive_model->receivedItem($reqnum);
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    function getAutocomplete()
    {
        $key = $this->input->post('term');
        $item = $this->input->post('item');
        if ($key) {
            $result = $this->Product_model->searchItem($key, $item);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $data[] = [
                        'label' => $row->kditem . ' ' . $row->itemname,
                    ];
                echo json_encode($data);
            }
        }
    }

    public function add()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['supplier'] = $this->Suppl_model->getSupplier();
        $data['item'] = $this->Product_model->getAllItem();
        $data['no'] = 1;
        $data['title'] = "Add Purchase Receiving";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('receive/add');
        $this->load->view('_partials/footer');
        $this->load->view('receive/js');
    }

    public function create()
    {
        $row = $this->Auth_model->getSales($this->session->userdata('username'));
        $item = $this->input->post('item');
        $quantity = $this->input->post('qty');
        $price = $this->input->post('price');

        if ($item) {
            $product = [];
            $qty = [];
            $sell = [];
            for ($i = 0; $i < count($item); $i++) {
                $product[] = $item[$i];
                $sell[] = $price[$i];
                $qty[] = $quantity[$i];
            }
        }

        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('slot', 'Slot', 'required');
        $this->form_validation->set_rules('supplier', 'Supplier', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array('', $product)) {
            $response['status'] = false;
            $response['error_item'] = true;
        } else if (in_array(0, $qty) or in_array('', $qty) or in_array(0, $sell) or in_array('', $sell)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Receive_model->addReceive();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function detail()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $reqnum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($reqnum == null) {
            return redirect('receive');
        }

        $data['row'] = $this->Receive_model->getById($reqnum);
        $data['item'] = $this->Receive_model->getDetailTrans($reqnum)->result();
        $data['no'] = 1;
        $data['title'] = "Detail Transaction";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('receive/detail', $data);
    }

    public function print_receive()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $reqnum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($reqnum == null) {
            return redirect('receive');
        }

        $data['row'] = $this->Receive_model->getById($reqnum);
        $data['item'] = $this->Receive_model->getDetailTrans($reqnum)->result();
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "print.pdf";
        $this->pdf->load_view('receive/print_receive', $data);
    }

    public function edit()
    {
        $reqnum = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($reqnum == null) {
            return redirect('receive');
        }
        $row = $this->Receive_model->getById($reqnum);
        if ($row->stat != "DRAFT") {
            return redirect('receive');
        }

        $query = $this->Receive_model->getDetailTrans($reqnum);
        $itemWh = $this->Receive_model->getItemWhByTrans($reqnum, $row->kdwarehouse);

        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Edit Purchase Receiving";
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['slot'] = $this->SlotRack_model->getSlotByWarehouse($row->kdwarehouse);
        $data['supplier'] = $this->Suppl_model->getSupplier();
        $data['item'] = $this->Product_model->getAllItemExcapt($query->result());
        $data['selItem'] = $this->Receive_model->getItemByTrans($reqnum);
        $data['itemWh'] = $itemWh->num_rows() > 0 ? $itemWh->result() : '';
        $data['row'] = $row;
        $data['detail'] = $query->result();
        $data['no'] = 0;
        $data['num'] = $query->num_rows();

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('receive/edit');
        $this->load->view('_partials/footer');
        $this->load->view('receive/js');
    }

    public function update()
    {
        $this->Auth_model->getSales($this->session->userdata('username'));
        $item = $this->input->post('item');
        $quantity = $this->input->post('qty');
        $price = $this->input->post('price');

        if ($item) {
            $product = [];
            $qty = [];
            $sell = [];
            for ($i = 0; $i < count($item); $i++) {
                $product[] = $item[$i];
                $sell[] = $price[$i];
                $qty[] = $quantity[$i];
            }
        }

        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('slot', 'Slot', 'required');
        $this->form_validation->set_rules('supplier', 'Supplier', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array('', $product)) {
            $response['status'] = false;
            $response['error_item'] = true;
        } else if (in_array(0, $qty) or in_array(0, $sell)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Receive_model->updateReceive();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
}

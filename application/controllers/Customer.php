<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Customer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        is_logged_in();
        $this->load->model('Auth_model');
        $this->load->model('Cust_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Customer";
        $data['action'] = base_url('customer/add');
        $data['action_edit'] = base_url('customer/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('customer/index', $data);
        $this->load->view('_partials/footer');
        $this->load->view('customer/js');
    }

    public function ajax_list()
    {
        $list = $this->Cust_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->custid;
            $row[] = $dt->custname;
            $row[] = $dt->alamat;
            $row[] = $dt->telp;
            $row[] = $dt->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
            $row[] = $dt->createdt;

            //add html for action
            $row[] = '<a href="' . base_url() . 'customer/edit?id=' . strtr(base64_encode($dt->custid), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil mr-1"></i> Edit</a>&nbsp;
            <a href="customer/delete?id=' . strtr(base64_encode($dt->custid), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-1"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Cust_model->count_all(),
            "recordsFiltered" => $this->Cust_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $row = $this->Auth_model->getSales($this->session->userdata('username'));
        $salesid = $row->salesid;

        $this->form_validation->set_rules('custname', 'Customer Name', 'required|trim|is_unique[customer.custname]', [
            'is_unique' => 'This Customer Name has already exist'
        ]);
        $this->form_validation->set_rules('telp', 'Contact', 'required|numeric|max_length[13]');
        $this->form_validation->set_rules('alamat', 'Address', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Cust_model->addCustomer($salesid);
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $custid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($custid == null) {
            return redirect('customer');
        }
        $data = $this->Cust_model->getById($custid);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('custname', 'Customer Name', 'required|trim');
        $this->form_validation->set_rules('telp', 'Contact', 'required|numeric|max_length[13]');
        $this->form_validation->set_rules('alamat', 'Address', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Cust_model->editCustomer();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $custid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($custid == null) {
            return redirect('customer');
        }
        return $this->Cust_model->deleteCustomer($custid);
    }
}

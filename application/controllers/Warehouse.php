<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Warehouse extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Warehouse_model');
        $this->load->model('SlotRack_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Warehouse";
        $data['action'] = base_url('warehouse/add');
        $data['action_edit'] = base_url('warehouse/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('warehouse/index');
        $this->load->view('_partials/footer');
        $this->load->view('warehouse/js');
    }

    public function ajax_list()
    {
        $list = $this->Warehouse_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->kdwarehouse;
            $row[] = $dt->descwarehouse;
            $row[] = $dt->address;
            $row[] = $dt->contact;

            //add html for action

            // if ($this->session->userdata('level') == 1) {
            //     $link = '<a href="" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
            //     <a href="warehouse/delete?id=' . strtr(base64_encode($dt->idwarehouse), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>';
            // } else {
            //     $link = '';
            // }

            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'warehouse/detail?id=' . strtr(base64_encode($dt->idwarehouse), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'warehouse/edit?id=' . strtr(base64_encode($dt->idwarehouse), '+/=', '._-') . '" class="dropdown-item act edit collapse-show" title="Edit"><i class="fas fa-pencil-alt"></i> Edit</a>
                                <a href="' . base_url() . 'warehouse/delete?id=' . strtr(base64_encode($dt->idwarehouse), '+/=', '._-') . '" class="dropdown-item btn-delete"><i class="fas fa-trash"></i> Delete</a>
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Warehouse_model->count_all(),
            "recordsFiltered" => $this->Warehouse_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required|is_unique[warehouse.kdwarehouse]', [
            'is_unique' => 'This Warehouse has already exist'
        ]);
        $this->form_validation->set_rules('desc', 'Warehouse Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('contact', 'Contact', 'required|numeric|max_length[13]');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Warehouse_model->addWarehouse();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function edit()
    {
        $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($idwarehouse == null) {
            return redirect('warehouse');
        }
        $data = $this->Warehouse_model->getById($idwarehouse);
        echo json_encode($data);
    }

    public function detail()
    {
        $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($idwarehouse == null) {
            return redirect('warehouse');
        }
        $row = $this->Warehouse_model->getById($idwarehouse);

        $warehouse = $row->kdwarehouse;

        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['slotid'] = $this->input->post('slotid');
        $data['product'] = $this->Warehouse_model->getProduct($warehouse);
        $data['row'] = $this->Warehouse_model->getById($idwarehouse);
        $data['slot'] = $this->SlotRack_model->getSlotByWarehouse($warehouse);
        $data['countSlot'] = $this->SlotRack_model->countSlot($warehouse);
        $data['countQty'] = $this->Warehouse_model->sumQtyProduct($warehouse);
        $data['no'] = 1;
        $data['title'] = "Detail Warehouse";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('warehouse/detail');
        $this->load->view('_partials/footer');
    }

    public function update()
    {
        $id = $this->input->post('warehouse');
        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('contact', 'Contact', 'required|numeric|max_length[13]');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Warehouse_model->editWarehouse();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($idwarehouse == null) {
            return redirect('warehouse');
        }
        return $this->Warehouse_model->deleteWarehouse($idwarehouse);
    }
}

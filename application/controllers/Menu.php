<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Menu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Menu_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Menu Management";
        $data['no'] = 1;
        $data['menu'] = $this->Menu_model->getAll();

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('menu/index');
        $this->load->view('_partials/footer');
        $this->load->view('menu/js');
    }

    public function add()
    {
        $this->form_validation->set_rules('menu', 'Menu', 'required|is_unique[user_menu.menu]', [
            'is_unique' => 'This Menu has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $response['status'] = false;
        } else {
            $response['status'] = true;
            $this->Menu_model->addMenu();
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('menu');
        }
        $this->Menu_model->deleteMenu($id);
        return redirect('menu');
    }

    // public function edit()
    // {
    //     $groupid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
    //     if ($groupid == null) {
    //         return redirect('group');
    //     }

    //     $data['row'] = $this->Menu_model->getById($groupid);
    //     $data['detail'] = $this->Menu_model->getDetailId($groupid);
    //     $data['user'] = $this->Menu_model->getUser();
    //     echo json_encode($data);
    // }

    // public function update()
    // {
    //     $this->form_validation->set_rules('groupname', 'Group Name', 'required');

    //     if ($this->form_validation->run() == false) {
    //         $errors = [];
    //         foreach ($this->input->post() as $key => $value) {
    //             $errors[$key] = form_error($key);
    //         }
    //         $response['errors'] = array_filter($errors); // Some might be empty
    //         $response['status'] = FALSE;
    //     } else {
    //         return $this->Menu_model->editMenu();
    //     }
    //     header('Content-type: application/json');
    //     exit(json_encode($response));
    // }
}

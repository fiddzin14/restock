<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\SheetView;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\DataType;

class ReportPenjualanAll extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'ReportAll_model',
            'Auth_model',
            'Warehouse_model',
            'Category_model',
            'Product_model',
        ]);
        $this->load->library('form_validation');
        $this->load->helper('rupiah_helper');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['date'] = $this->ReportAll_model->getDate();
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['category'] = $this->Category_model->getCategory();
        $data['item'] = $this->Product_model->getAllItem();
        $data['count'] = $this->ReportAll_model->sumCount();
        $data['title'] = "Report Penjualan All";

        if ($this->input->post('export')) {
            $this->export();
        } else {
            $this->load->view('_partials/header', $data);
            $this->load->view('_partials/sidebar');
            $this->load->view('report/penjualanAll');
            $this->load->view('_partials/footer');
            $this->load->view('report/js');
        }
    }

    public function ajax_list()
    {
        $list = $this->ReportAll_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $dt->TransID;
            $row[] = date('d M y', strtotime($dt->TransDt));
            $row[] = $dt->KdItem;
            $row[] = $dt->ItemName;
            $row[] = $dt->Qty;
            $row[] = rupiah($dt->HargaJual);
            $row[] = rupiah($dt->Harga_Modal);
            $row[] = rupiah($dt->TotalSalesPrice);
            $row[] = rupiah($dt->TotalHPP);
            $row[] = rupiah($dt->Benefit);

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->ReportAll_model->count_all(),
            "recordsFiltered" => $this->ReportAll_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function getItemName()
    {
        $item = $this->input->post('item', true);
        $data = $this->Product_model->getItemName($item);
        echo json_encode($data);
    }

    public function getCount()
    {
        $catname = $this->input->post('catname');
        $kdwarehouse = $this->input->post('kdwarehouse');
        $item = $this->input->post('item');
        $date = $this->input->post('date');
        $row = $this->ReportAll_model->sumCount($catname, $item, $date, $kdwarehouse);
        $data['hpp'] = rupiah($row->hpp);
        $data['qty'] = nominal($row->qty);
        $data['total'] = rupiah($row->total);
        $data['benefit'] = rupiah($row->benefit);
        echo json_encode($data);
    }

    public function export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getStyle('A1:K1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A1:K1')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:K1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A1:K1')
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A1:K1')
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A1:K1')
            ->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
        $sheet->getRowDimension('1')->setRowHeight('20');
        $sheet->getStyle('A1:K1')->getFont()->setBold(true);

        $sheet->setCellValue('A1', 'No')->getColumnDimension('A')->setAutoSize(true);
        $sheet->setCellValue('B1', 'Transaction ID')->getColumnDimension('B')->setAutoSize(true);
        $sheet->setCellValue('C1', 'Date')->getColumnDimension('C')->setAutoSize(true);
        $sheet->setCellValue('D1', 'Item Code')->getColumnDimension('D')->setAutoSize(true);
        $sheet->setCellValue('E1', 'Item Name')->getColumnDimension('E')->setAutoSize(true);
        $sheet->setCellValue('F1', 'Sell Price')->getColumnDimension('F')->setAutoSize(true);
        $sheet->setCellValue('G1', 'Cost')->getColumnDimension('G')->setAutoSize(true);
        $sheet->setCellValue('H1', 'Qty')->getColumnDimension('H')->setAutoSize(true);
        $sheet->setCellValue('I1', 'Total')->getColumnDimension('I')->setAutoSize(true);
        $sheet->setCellValue('J1', 'Total HPP')->getColumnDimension('J')->setAutoSize(true);
        $sheet->setCellValue('K1', 'Benefit')->getColumnDimension('K')->setAutoSize(true);

        // $catname = $this->input->post('catname');
        // $warehouse = $this->input->post('warehouse');
        // $item = $this->input->post('item');
        $dateR = $this->input->post('date');
        $data = $this->ReportAll_model->getAll();
        $no = 1;
        $x = 2;
        foreach ($data as $row) {
            $sheet->getStyle('A' . $x . ':K' . $x)
                ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getRowDimension($x)->setRowHeight('20');
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->TransID);
            $sheet->setCellValue('C' . $x, date('d M y', strtotime($row->TransDt)));
            $sheet->setCellValue('D' . $x, $row->KdItem);
            $sheet->setCellValue('E' . $x, $row->ItemName);
            $sheet->getStyle('F' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('F' . $x, $row->HargaJual, DataType::TYPE_NUMERIC);
            $sheet->getStyle('G' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('G' . $x, $row->Harga_Modal, DataType::TYPE_NUMERIC);
            $sheet->setCellValue('H' . $x, $row->Qty);
            $sheet->getStyle('I' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('I' . $x, $row->TotalSalesPrice, DataType::TYPE_NUMERIC);
            $sheet->getStyle('J' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('J' . $x, $row->TotalHPP, DataType::TYPE_NUMERIC);
            $sheet->getStyle('K' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('K' . $x, $row->Benefit, DataType::TYPE_NUMERIC);
            $x++;
        }
        $x3 = $x - 1;
        $sheet->getStyle('A' . $x . ':K' . $x)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A' . $x . ':K' . $x)->getFont()->setBold(true);
        $sheet->getStyle('A' . $x . ':K' . $x)
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A' . $x . ':K' . $x)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A' . $x . ':K' . $x)
            ->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
        $sheet->getStyle('A' . $x . ':K' . $x)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getRowDimension($x)->setRowHeight('20');

        $sheet->getStyle('A' . $x)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->mergeCells('A' . $x . ':G' . $x)->setCellValue('A' . $x, 'Total');
        $sheet->setCellValue('H' . $x, '=SUM(H2:H' . $x3 . ')');
        $sheet->getStyle('I' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('I' . $x, '=SUM(I2:I' . $x3 . ')');
        $sheet->getStyle('J' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('J' . $x, '=SUM(J2:J' . $x3 . ')');
        $sheet->getStyle('K' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('K' . $x, '=SUM(K2:K' . $x3 . ')');

        $writer = new Xlsx($spreadsheet);

        if ($dateR) {
            $date = explode('-', $dateR);
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $filename = 'Laporan-penjualan periode ' . date('d M y', strtotime($SDate)) . ' s/d ' . date('d M y', strtotime($EDate));
        } else {
            $filename = 'Laporan-penjualan';
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.Xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}

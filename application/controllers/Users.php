<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('User_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['level'] = $this->User_model->getLevel();
        $data['title'] = "User Login";
        $data['action'] = base_url('users/add');
        $data['action_edit'] = base_url('users/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('user/index');
        $this->load->view('_partials/footer');
        $this->load->view('user/js');
    }

    public function ajax_list()
    {
        $list = $this->User_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $ls) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $ls->username;
            $row[] = $ls->displayname;
            $row[] = $ls->email;
            $row[] = $ls->level;
            $row[] = $ls->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';

            //add html for action
            $row[] = '<a href="' . base_url() . 'users/edit?id=' . strtr(base64_encode($ls->username), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit Class"><i class="fa fa-pencil mr-2"></i>Edit</a>&nbsp; 
            <a href="users/delete?id=' . strtr(base64_encode($ls->username), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-2"></i>Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->count_all(),
            "recordsFiltered" => $this->User_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[login_web.username]', [
            'is_unique' => 'This Username has already exist'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[login_web.email]|valid_email', [
            'is_unique' => 'This Email has already exist'
        ]);
        $this->form_validation->set_rules('displayname', 'Display Name', 'required');
        // $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|matches[confirmPassword]', [
        //     'matches' => 'Password dont match!',
        //     'min_length' => 'Password too short'
        // ]);

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->User_model->addUser();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('users');
        }
        $data = $this->User_model->getById($id);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('displayname', 'Display Name', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->User_model->editUser();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('users');
        }
        return $this->User_model->deleteUser($id);
    }
}

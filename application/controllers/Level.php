<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Level extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'Level_model',
            'Menu_model',
            'SubMenu_model',
            'Auth_model',
        ]);
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "User Level";
        $data['no'] = 1;
        $data['level'] = $this->Level_model->getAll();

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('level/index');
        $this->load->view('_partials/footer');
        $this->load->view('level/js');
    }

    public function add()
    {
        $this->form_validation->set_rules('level', 'User Level', 'required|is_unique[loginlevel.level]', [
            'is_unique' => 'This Level has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $response['status'] = false;
        } else {
            $response['status'] = true;
            $this->Level_model->addLevel();
        }
        echo json_encode($response);
    }

    public function access()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $level = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($level == null) {
            return redirect('level');
        }

        $data['title'] = "Manage Access";
        $data['row'] = $this->Level_model->getById($level);
        $data['to'] = $this->Level_model->getMenuBySub(21);
        $data['menu'] = $this->Menu_model->getAll();
        $data['sub'] = $this->SubMenu_model->getAll();
        $data['access'] = $this->Level_model->getAccess($level);

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('level/access');
        $this->load->view('_partials/footer');
        $this->load->view('level/js');
    }

    public function update()
    {
        $response['status'] = TRUE;
        return $this->Level_model->SetAccess();
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('level');
        }
        $this->Level_model->deleteLevel($id);
        return redirect('level');
    }
}

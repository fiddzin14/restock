<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class SlotRack extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('SlotRack_model');
        $this->load->model('Warehouse_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Slot Rack";
        $data['action'] = base_url('slotRack/add');
        $data['action_edit'] = base_url('slotRack/update');
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('slotRack/index', $data);
        $this->load->view('_partials/footer');
        $this->load->view('slotRack/js');
    }

    public function ajax_list()
    {
        $list = $this->SlotRack_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->slotid;
            $row[] = $dt->slotnm;
            $row[] = $dt->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
            $row[] = $dt->kdwarehouse;

            //add html for action
            $row[] = '<a href="' . base_url() . 'slotRack/edit?slotid=' . strtr(base64_encode($dt->slotid), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil mr-1"></i> Edit</a>&nbsp;
            <a href="slotRack/delete?slotid=' . strtr(base64_encode($dt->slotid), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-1"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->SlotRack_model->count_all(),
            "recordsFiltered" => $this->SlotRack_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('slotid', 'Slot ID', 'required|trim|is_unique[slot.slotid]', [
            'is_unique' => 'This Slot ID has already exist'
        ]);
        $this->form_validation->set_rules('slotnm', 'Slot Name', 'required|trim|is_unique[slot.slotnm]', [
            'is_unique' => 'This Slot Name has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->SlotRack_model->addSlot();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $slotid = base64_decode(strtr($this->input->get('slotid'), '._-', '+/='));
        if ($slotid == null) {
            return redirect('slotRack');
        }
        $data = $this->SlotRack_model->getById($slotid);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('slotid', 'Slot ID', 'required|trim');
        $this->form_validation->set_rules('slotnm', 'Slot Name', 'required|trim');
        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->SlotRack_model->editSlot();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $slotid = base64_decode(strtr($this->input->get('slotid'), '._-', '+/='));
        if ($slotid == null) {
            return redirect('slotRack');
        }
        return $this->SlotRack_model->deleteSlot($slotid);
    }
}

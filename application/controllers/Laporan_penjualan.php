<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\SheetView;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\DataType;

class Laporan_penjualan extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        // Load model
        $this->load->model('Penjualan_model');
        $this->load->model('Warehouse_model');
        $this->load->model('Auth_model');
        $this->load->helper('rupiah_helper');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['kdwarehouse'] = $this->input->post('kdwarehouse');
        $data['title'] = "Laporan Penjualan";
        $data['date'] = $this->Penjualan_model->getDate();
        $data['data'] = $this->Penjualan_model->getAll();
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['count'] = $this->Penjualan_model->sumCount();
        $data['no'] = 1;
        // load view
        if ($this->input->post('export')) {
            $this->export();
        } else {
            $this->load->view('_partials/header', $data);
            $this->load->view('_partials/sidebar');
            $this->load->view('report/view_laporanpenjualan', $data);
        }
    }

    public function export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getStyle('A1:J1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A1:J1')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:J1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A1:J1')
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A1:J1')
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A1:J1')
            ->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
        $sheet->getRowDimension('1')->setRowHeight('20');
        $sheet->getStyle('A1:J1')->getFont()->setBold(true);

        $sheet->setCellValue('A1', 'No')->getColumnDimension('A')->setAutoSize(true);
        $sheet->setCellValue('B1', 'Transaction ID')->getColumnDimension('B')->setAutoSize(true);
        $sheet->setCellValue('C1', 'Date')->getColumnDimension('C')->setAutoSize(true);
        $sheet->setCellValue('D1', 'Gudang')->getColumnDimension('D')->setAutoSize(true);
        $sheet->setCellValue('E1', 'Customer')->getColumnDimension('E')->setAutoSize(true);
        $sheet->setCellValue('F1', 'Status')->getColumnDimension('F')->setAutoSize(true);
        $sheet->setCellValue('G1', 'QTY')->getColumnDimension('G')->setAutoSize(true);
        $sheet->setCellValue('H1', 'TotalHPP')->getColumnDimension('H')->setAutoSize(true);
        $sheet->setCellValue('I1', 'Total')->getColumnDimension('I')->setAutoSize(true);
        $sheet->setCellValue('J1', 'Profit')->getColumnDimension('J')->setAutoSize(true);

        $data = $this->Penjualan_model->getAll();
        $no = 1;
        $x = 2;
        foreach ($data as $row) {
            $sheet->getStyle('A' . $x . ':J' . $x)
                ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getRowDimension($x)->setRowHeight('20');
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->TransID);
            $sheet->setCellValue('C' . $x, date('d M y', strtotime($row->TransDt)));
            $sheet->setCellValue('D' . $x, $row->kdwh);
            $sheet->setCellValue('E' . $x, $row->CustName);
            $sheet->setCellValue('F' . $x, $row->TransStatus);
            $sheet->setCellValue('G' . $x, $row->NumOfItem);
            $sheet->getStyle('H' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('H' . $x, $row->TotalHPP, DataType::TYPE_NUMERIC);
            $sheet->getStyle('I' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('I' . $x, $row->Total, DataType::TYPE_NUMERIC);
            $sheet->getStyle('J' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
            $sheet->setCellValueExplicit('J' . $x, $row->Total - $row->TotalHPP, DataType::TYPE_NUMERIC);
            $x++;
        }
        $x3 = $x - 1;
        $sheet->getStyle('A' . $x . ':J' . $x)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A' . $x . ':J' . $x)->getFont()->setBold(true);
        $sheet->getStyle('A' . $x . ':J' . $x)
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A' . $x . ':J' . $x)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A' . $x . ':J' . $x)
            ->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLUE);
        $sheet->getStyle('A' . $x . ':J' . $x)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getRowDimension($x)->setRowHeight('20');
        // $sheet->getStyle('F' . $x . ':J' . $x)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        // $sheet->getStyle('F' . $x1 . ':J' . $x1)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        // $sheet->getStyle('F' . $x2 . ':J' . $x2)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet->getStyle('A' . $x)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $sheet->getStyle('F' . $x)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $sheet->getStyle('F' . $x1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $sheet->getStyle('F' . $x2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        // $sheet->getRowDimension($x)->setRowHeight('20');
        // $sheet->getRowDimension($x1)->setRowHeight('20');
        // $sheet->getRowDimension($x2)->setRowHeight('20');
        $sheet->mergeCells('A' . $x . ':F' . $x)->setCellValue('A' . $x, 'Total');
        $sheet->setCellValue('G' . $x, '=SUM(G2:G' . $x3 . ')');
        // $sheet->mergeCells('F' . $x . ':J' . $x)->setCellValue('F' . $x, 'TotalHPP');
        $sheet->getStyle('H' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('H' . $x, '=SUM(H2:H' . $x3 . ')');
        $sheet->getStyle('I' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('I' . $x, '=SUM(I2:I' . $x3 . ')');
        $sheet->getStyle('J' . $x)->getNumberFormat()->setFormatCode('Rp #,##0');
        $sheet->setCellValue('J' . $x, '=SUM(J2:J' . $x3 . ')');

        $writer = new Xlsx($spreadsheet);

        if ($this->input->post('transdt')) {
            $date = explode('-', $this->input->post('transdt'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $filename = 'Laporan-penjualan periode ' . date('d M y', strtotime($SDate)) . ' s/d ' . date('d M y', strtotime($EDate));
        } else {
            $filename = 'Laporan-penjualan';
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.Xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Supplier extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Auth_model');
        $this->load->model('Suppl_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Supplier";
        $data['action'] = base_url('supplier/add');
        $data['action_edit'] = base_url('supplier/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('supplier/index', $data);
        $this->load->view('_partials/footer');
        $this->load->view('supplier/js');
    }

    public function ajax_list()
    {
        $list = $this->Suppl_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->kdsupplier;
            $row[] = $dt->descsupplier;
            $row[] = $dt->alamat;
            $row[] = $dt->telp;
            $row[] = $dt->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';

            //add html for action
            $row[] = '<a href="' . base_url() . 'supplier/edit?id=' . strtr(base64_encode($dt->idsupplier), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil mr-1"></i> Edit</a>&nbsp;
            <a href="supplier/delete?id=' . strtr(base64_encode($dt->idsupplier), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-1"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Suppl_model->count_all(),
            "recordsFiltered" => $this->Suppl_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $row = $this->Auth_model->getSales($this->session->userdata('username'));
        $salesid = $row->salesid;

        $this->form_validation->set_rules('descsupplier', 'Description', 'required');
        $this->form_validation->set_rules('telp', 'Contact', 'required|numeric|max_length[13]');
        $this->form_validation->set_rules('alamat', 'Address', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Suppl_model->addSupplier($salesid);
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $idsupplier = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($idsupplier == null) {
            return redirect('supplier');
        }
        $data = $this->Suppl_model->getById($idsupplier);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('descsupplier', 'Description', 'required|trim');
        $this->form_validation->set_rules('telp', 'Contact', 'required|numeric|max_length[13]');
        $this->form_validation->set_rules('alamat', 'Address', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Suppl_model->editSupplier();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $idsupplier = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($idsupplier == null) {
            return redirect('supplier');
        }
        return $this->Suppl_model->deleteSupplier($idsupplier);
    }
}

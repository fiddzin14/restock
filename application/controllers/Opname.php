<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Opname extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'Opname_model',
            'Auth_model',
            'Warehouse_model',
            // 'Cust_model',
            // 'SlotRack_model',
            'Product_model',
        ]);
        $this->load->helper('rupiah_helper');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['date'] = time();
        $data['title'] = "Stock Opname";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('opname/index');
        $this->load->view('_partials/footer');
        $this->load->view('opname/js');
    }

    public function ajax_list()
    {
        $list = $this->Opname_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dt->idopname;
            $row[] = date('d M y', strtotime($dt->stodate));
            $row[] = $dt->notes;
            $row[] = $dt->status;
            $row[] = $dt->countitems;
            $row[] = $dt->descwarehouse;

            //add html for action
            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'opname/detail?id=' . strtr(base64_encode($dt->idopname), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'opname/print_invoice?id=' . strtr(base64_encode($dt->idopname), '+/=', '._-') . '" target="_blank" class="dropdown-item"><i class="icon-file-pdf"></i> Print</a>
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Opname_model->count_all(),
            "recordsFiltered" => $this->Opname_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function getItemWarehouse()
    {
        $warehouse = $this->input->get('warehouse');
        $data = $this->Product_model->getItemWarehouse($warehouse);

        echo json_encode($data);
    }

    public function add()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['title'] = "Add Stock Opname";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('opname/add');
        $this->load->view('_partials/footer');
        $this->load->view('opname/js');
    }

    public function create()
    {
        $this->Auth_model->getSales($this->session->userdata('username'));
        $quantity = $this->input->post('qtyact');

        if ($quantity) {
            $qty = [];
            for ($i = 0; $i < count($quantity); $i++) {
                $qty[] = $quantity[$i];
            }
        }

        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        // $this->form_validation->set_rules('slot', 'Slot', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array(0, $qty) or in_array('', $qty)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Opname_model->addOpname();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
}

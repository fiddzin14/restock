<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class UnitOfMeasure extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Auth_model');
        $this->load->model('Uom_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Unit Of Measure";
        $data['action'] = base_url('UnitOfMeasure/add');
        $data['action_edit'] = base_url('UnitOfMeasure/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('UnitOfMeasure/index', $data);
        $this->load->view('_partials/footer');
        $this->load->view('UnitOfMeasure/js');
    }

    public function ajax_list()
    {
        $list = $this->Uom_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->uom;
            $row[] = $dt->descr;

            //add html for action
            $row[] = '<a href="' . base_url() . 'UnitOfMeasure/edit?id=' . strtr(base64_encode($dt->id), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil mr-1"></i> Edit</a>&nbsp;
            <a href="UnitOfMeasure/delete?id=' . strtr(base64_encode($dt->id), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash mr-1"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Uom_model->count_all(),
            "recordsFiltered" => $this->Uom_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('uom', 'Unit Of Measure', 'required|trim|is_unique[uom.uom]', [
            'is_unique' => 'This Unit Of Measure has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Uom_model->addUnit();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }
    public function edit()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('UnitOfMeasure');
        }
        $data = $this->Uom_model->getById($id);
        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('uom', 'Unit Of Measure', 'required|trim');
        $this->form_validation->set_rules('descr', ' Description', 'required|trim');
        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Uom_model->editUnit();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $id = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($id == null) {
            return redirect('UnitOfMeasure');
        }
        return $this->Uom_model->deleteUnit($id);
    }
}

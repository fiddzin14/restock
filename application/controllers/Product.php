<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Product_model');
        $this->load->model('To_warehouse_model');
        $this->load->model('Category_model');
        $this->load->model('Uom_model');
        $this->load->model('SlotRack_model');
        $this->load->model('Warehouse_model');
        $this->load->model('Auth_model');
        $this->load->helper('rupiah_helper');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $username = $this->session->userdata('username');
        $data['login'] = $this->Auth_model->getUserLogin($username);
        $data['category'] = $this->Category_model->getCategory();
        $data['uom'] = $this->Uom_model->getUom();
        $data['title'] = "Product";
        $data['action'] = base_url('product/add');
        $data['action_edit'] = base_url('product/update');

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('product/index');
        $this->load->view('_partials/footer');
        $this->load->view('product/js');
    }

    public function ajax_list()
    {
        $list = $this->Product_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->kditem;
            $row[] = $dt->itemname;
            $row[] = $dt->brand;
            $row[] = $dt->catname;
            $row[] = $dt->stocktype;

            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'product/detail?id=' . strtr(base64_encode($dt->kditem), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'product/edit?id=' . strtr(base64_encode($dt->kditem), '+/=', '._-') . '" class="dropdown-item act edit collapse-show" title="Edit"><i class="fas fa-pencil-alt"></i> Edit</a>
                                <a href="' . base_url() . 'product/delete?id=' . strtr(base64_encode($dt->kditem), '+/=', '._-') . '" class="dropdown-item btn-delete"><i class="fas fa-trash"></i> Delete</a>
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Product_model->count_all(),
            "recordsFiltered" => $this->Product_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function item_list()
    {
        $kditem = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        $list = $this->To_warehouse_model->get_datatables($kditem);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $dt->descwarehouse;
            $row[] = $dt->slotnm;
            $row[] = $dt->lastin;
            $row[] = $dt->lastout;
            $row[] = $dt->curbal;
            $row[] = $dt->issueunit;
            $row[] = $dt->qty_reserved;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->To_warehouse_model->count_all($kditem),
            "recordsFiltered" => $this->To_warehouse_model->count_filtered($kditem),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('kditem', 'Item Code', 'required|is_unique[itemmaster.kditem]', [
            'is_unique' => 'This Item Code has already exist'
        ]);
        $this->form_validation->set_rules('itemname', 'Item Name', 'required');
        $this->form_validation->set_rules('brand', 'Brand', 'required');
        $this->form_validation->set_rules('minqty', 'Min Qty', 'required');
        $this->form_validation->set_rules('hargamodal', 'Harga Modal', 'required');
        $this->form_validation->set_rules('hargajual', 'Harga Jual', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Product_model->addProduct();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function toWarehouse()
    {
        $item = $this->input->post('kditem');
        $kdwh = $this->input->post('warehouse');
        $slotid = $this->input->post('slot');

        $checkwh = $this->db->get_where('detailitemwh', ['kditem' => $item, 'kdwarehouse' => $kdwh, 'slotid' => $slotid])->row();

        $this->form_validation->set_rules('kditem', 'Item Code', 'required');
        $this->form_validation->set_rules('itemname', 'Item Name', 'required');
        $this->form_validation->set_rules('slot', 'Slot', 'required');
        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('qty', 'Quantity', 'required');
        $this->form_validation->set_rules('hargamodal', 'Harga Modal', 'required');
        $this->form_validation->set_rules('hargajual', 'Harga Jual', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            if ($checkwh) {
                $curbal = $checkwh->curbal;
                $warehouse = $checkwh->kdwarehouse;
                $slot = $checkwh->slotid;
                $add = $this->To_warehouse_model->stockMutasi($curbal);
                $add = $this->To_warehouse_model->updatetowh($curbal, $warehouse, $slot);
                return $add;
            } else {
                $curbal = 0;
                $add = $this->To_warehouse_model->stockMutasi($curbal);
                $add = $this->To_warehouse_model->addtowh();
                return $add;
            }
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function detail()
    {
        $kditem = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($kditem == null) {
            return redirect('product');
        }

        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['row'] = $this->Product_model->getById($kditem);
        $data['uom'] = $this->Uom_model->getUom();
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['title'] = "Detail Product";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('product/detail', $data);
    }

    public function sendSlot()
    {
        $warehouse = $this->input->post('id', true);
        $data = $this->SlotRack_model->getSlotByWarehouse($warehouse);
        echo json_encode($data);
    }

    public function edit()
    {
        $kditem = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($kditem == null) {
            return redirect('product');
        }

        $row = $this->Product_model->getById($kditem);
        $data['row'] = $row;
        $data['hargaJ'] = nominal($row->hargajual);
        $data['hargaM'] = nominal($row->hargamodal);

        echo json_encode($data);
    }

    public function update()
    {
        $this->form_validation->set_rules('kditem', 'Item Code', 'required');
        $this->form_validation->set_rules('itemname', 'Item Name', 'required');
        $this->form_validation->set_rules('brand', 'Brand', 'required');
        $this->form_validation->set_rules('minqty', 'Min Qty', 'required');
        $this->form_validation->set_rules('hargamodal', 'Harga Modal', 'required');
        $this->form_validation->set_rules('hargajual', 'Harga Jual', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Product_model->editProduct();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $kditem = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($kditem == null) {
            return redirect('product');
        }
        return $this->Product_model->deleteProduct($kditem);
    }
}

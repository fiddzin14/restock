<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model([
            'Order_model',
            'Auth_model',
            'Warehouse_model',
            'Cust_model',
            'SlotRack_model',
            'Product_model',
        ]);
        $this->load->helper('rupiah_helper');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['date'] = $this->Order_model->getDate();
        $data['title'] = "Sales Order";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('order/index');
        $this->load->view('_partials/footer');
        $this->load->view('order/js');
    }

    public function ajax_list()
    {
        $list = $this->Order_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dt->transid;
            $row[] = date('d M y', strtotime($dt->transdt));
            $row[] = $dt->actionby;
            $row[] = $dt->custname;
            $row[] = $dt->numofitem;
            $row[] = $dt->transstatus;
            $row[] = rupiah($dt->total);

            //add html for action
            $row[] = '<div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="' . base_url() . 'order/detail?id=' . strtr(base64_encode($dt->transid), '+/=', '._-') . '" class="dropdown-item"><i class="fas fa-eye"></i> Detail</a>
                                <a href="' . base_url() . 'order/print_invoice?id=' . strtr(base64_encode($dt->transid), '+/=', '._-') . '" target="_blank" class="dropdown-item"><i class="icon-file-pdf"></i> Print</a>
                            </div>
                        </div>
                    </div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Order_model->count_all(),
            "recordsFiltered" => $this->Order_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function getSlotByWarehouse()
    {
        $warehouse = $this->input->post('warehouse', true);
        $data = $this->SlotRack_model->getSlotByWarehouse($warehouse);
        echo json_encode($data);
    }

    public function getItemBySlot()
    {
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $data = $this->Product_model->getItemByWarehouse($warehouse, $slot);
        echo json_encode($data);
    }

    public function getSelectedItem()
    {
        $slot = $this->input->post('slot', true);
        $warehouse = $this->input->post('warehouse', true);
        $kditem = $this->input->post('item', true);
        $selectItem = $this->input->post('itemSelect', true);
        $Sitem = [];
        if ($selectItem != '') {
            for ($i = 0; $i < count($selectItem); $i++) {
                $Sitem[] = $selectItem[$i]['value'];
            }
        }

        if (in_array($kditem, $Sitem)) {
            $data['error'] = true;
        } else {
            $data['row'] = $this->Product_model->getSelectedItem($kditem);
            $data['detail'] = $this->Product_model->getDetail($kditem, $warehouse, $slot)->row();
        }
        echo json_encode($data);
    }

    public function checkQty()
    {
        $kditem = $this->input->post('item', true);
        $slot = $this->input->post('slot', true);
        $warehouse = $this->input->post('warehouse', true);
        $qty = $this->input->post('qty', true);

        $data = $this->Product_model->getDetail($kditem, $warehouse, $slot)->row();

        if ($qty <= $data->curbal) {
            $response['status'] = true;
        } else {
            $response['status'] = false;
        }
        $response['data'] = $data;
        echo json_encode($response);
    }

    function getAutocomplete()
    {
        $key = $this->input->post('term');
        $item = $this->input->post('item');
        $warehouse = $this->input->post('warehouse');
        $slot = $this->input->post('slot');
        if ($key) {
            $result = $this->Product_model->searchItemByWarehouse($key, $item, $warehouse, $slot);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $data[] = [
                        'label' => $row->kditem . ' ' . $row->itemname,
                    ];
                echo json_encode($data);
            }
        }
    }

    public function add()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['customer'] = $this->Cust_model->getCustomer();
        $data['title'] = "Add Sales Order";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('order/add');
        $this->load->view('_partials/footer');
        $this->load->view('order/js');
    }

    public function create()
    {
        $this->Auth_model->getSales($this->session->userdata('username'));
        $item = $this->input->post('item');
        $quantity = $this->input->post('qty');
        $price = $this->input->post('price');

        if ($item) {
            $product = [];
            $sell = [];
            $qty = [];
            for ($i = 0; $i < count($item); $i++) {
                $product[] = $item[$i];
                $sell[] = $price[$i];
                $qty[] = $quantity[$i];
            }
        }

        $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
        $this->form_validation->set_rules('slot', 'Slot', 'required');
        $this->form_validation->set_rules('customer', 'Customer', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else if (in_array('', $product)) {
            $response['status'] = false;
            $response['error_item'] = true;
        } else if (in_array(0, $qty) or in_array(0, $sell) or in_array('', $qty) or in_array('', $sell)) {
            $response['status'] = false;
            $response['error_qty'] = true;
        } else {
            $response['status'] = true;
            return $this->Order_model->addOrder();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function detail()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $transid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($transid == null) {
            return redirect('warehouse');
        }

        $data['row'] = $this->Order_model->getByTransId($transid);
        $data['item'] = $this->Order_model->getDetailTrans($transid);
        // $data['count'] = $this->Order_model->countTotal($transid);
        $data['no'] = 1;
        $data['title'] = "Detail Transaction";

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('order/detail', $data);
    }

    public function print_invoice()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $transid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($transid == null) {
            return redirect('warehouse');
        }

        $data['row'] = $this->Order_model->getByTransId($transid);
        $data['item'] = $this->Order_model->getDetailTrans($transid);
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "invoice.pdf";
        $this->pdf->load_view('order/print_invoice', $data);
    }

    // public function edit()
    // {
    //     $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
    //     if ($idwarehouse == null) {
    //         return redirect('warehouse');
    //     }
    //     $data = $this->Order_model->getById($idwarehouse);
    //     echo json_encode($data);
    // }

    // public function update()
    // {
    //     $id = $this->input->post('warehouse');
    //     $this->form_validation->set_rules('warehouse', 'Warehouse', 'required');
    //     $this->form_validation->set_rules('address', 'Address', 'required');
    //     $this->form_validation->set_rules('contact', 'Contact', 'required|numeric|max_length[13]');

    //     if ($this->form_validation->run() == false) {
    //         $errors = [];
    //         foreach ($this->input->post() as $key => $value) {
    //             $errors[$key] = form_error($key);
    //         }
    //         $response['errors'] = array_filter($errors); // Some might be empty
    //         $response['status'] = FALSE;
    //     } else {
    //         return $this->Order_model->editWarehouse();
    //     }
    //     header('Content-type: application/json');
    //     exit(json_encode($response));
    // }

    // public function delete()
    // {
    //     $idwarehouse = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
    //     if ($idwarehouse == null) {
    //         return redirect('warehouse');
    //     }
    //     return $this->Order_model->deleteWarehouse($idwarehouse);
    // }
}

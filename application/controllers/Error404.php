<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Error404 extends CI_Controller
{
    public function index()
    {
        $this->load->view('errors/html/error_404');
    }
}

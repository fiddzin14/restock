<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan_itemmasuk extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        // Load model
        $this->load->model('Itemmasuk_model');
        $this->load->model('Warehouse_model');
        $this->load->model('Auth_model');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "Laporan Item Masuk";
        $data['kdwarehouse'] = $this->input->post('kdwarehouse');
        $data['oldate'] = $this->input->post('transdt');
        $data['date'] = $this->Itemmasuk_model->getDate();
        $data['data'] = $this->Itemmasuk_model->getAll();
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['count'] = $this->Itemmasuk_model->sumCount()->row();
        $data['no'] = 1;
        // load view
        if ($this->input->post('export')) {
            $this->export();
        } else {
            $this->load->view('_partials/header', $data);
            $this->load->view('_partials/sidebar');
            $this->load->view('report/view_laporanitemmasuk', $data);
            $this->load->view('_partials/footer');
        }
    }

    public function export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getStyle('A1:I1')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A1:I1')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:I1')
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A1:I1')
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A1:I1')
            ->getFill()->getStartColor()->setRGB('0,112,245');
        $sheet->getRowDimension('1')->setRowHeight('20');

        $sheet->setCellValue('A1', 'No')->getColumnDimension('A')->setAutoSize(true);
        $sheet->setCellValue('B1', 'Item Name')->getColumnDimension('B')->setAutoSize(true);
        $sheet->setCellValue('C1', 'Transaction ID')->getColumnDimension('C')->setAutoSize(true);
        $sheet->setCellValue('D1', 'Date')->getColumnDimension('D')->setAutoSize(true);
        $sheet->setCellValue('E1', 'Supplier')->getColumnDimension('E')->setAutoSize(true);
        $sheet->setCellValue('F1', 'Type')->getColumnDimension('F')->setAutoSize(true);
        $sheet->setCellValue('G1', 'Gudang')->getColumnDimension('G')->setAutoSize(true);
        $sheet->setCellValue('H1', 'Action By')->getColumnDimension('H')->setAutoSize(true);
        $sheet->setCellValue('I1', 'QTY')->getColumnDimension('I')->setAutoSize(true);

        $data = $this->Itemmasuk_model->getAll();
        $no = 1;
        $x = 2;
        foreach ($data as $row) {
            $sheet->getStyle('A' . $x . ':I' . $x)
                ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getRowDimension($x)->setRowHeight('20');
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->ItemName);
            $sheet->setCellValue('C' . $x, $row->ReqNum);
            $sheet->setCellValue('D' . $x, date('d M y', strtotime($row->TransDt)));
            $sheet->setCellValue('E' . $x, $row->Suppl);
            $sheet->setCellValue('F' . $x, $row->Type);
            $sheet->setCellValue('G' . $x, $row->KdWarehouse);
            $sheet->setCellValue('H' . $x, $row->ReqBy);
            $sheet->setCellValue('I' . $x, $row->JumlahMasuk);
            $x++;
        }
        $x3 = $x - 1;
        $sheet->getStyle('A' . $x . ':I' . $x)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet->getStyle('A' . $x)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getRowDimension($x)->setRowHeight('20');
        $sheet->mergeCells('A' . $x . ':H' . $x)->setCellValue('A' . $x, 'Total Qty');
        $sheet->setCellValue('I' . $x, '=SUM(I2:I' . $x3 . ')');
        $writer = new Xlsx($spreadsheet);

        $SDate = $this->input->post('startDate');
        $EDate = $this->input->post('endDate');

        if ($SDate && $EDate) {
            $filename = 'Laporan-item-masuk periode ' . date('d M y', strtotime($SDate)) . ' s/d ' . date('d M y', strtotime($EDate));
        } else {
            $filename = 'Laporan-item-masuk';
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('./application/third_party/vendor/autoload.php');

class Group extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load model
        $this->load->model('Group_model');
        $this->load->model('Warehouse_model');
        $this->load->model('Auth_model');
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index()
    {
        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "User Group";
        $data['action'] = base_url('group/add');
        $data['action_edit'] = base_url('group/update');
        $data['warehouse'] = $this->Warehouse_model->getWarehouse();
        $data['user'] = $this->Group_model->getUser();

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('group/index');
        $this->load->view('_partials/footer');
        $this->load->view('group/js');
    }

    public function ajax_list()
    {
        $list = $this->Group_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $no2 = $no + 1;
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $no2++;
            $row[] = $dt->groupname;
            $row[] = $dt->descwarehouse;
            $row[] = $dt->isactive == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';

            //add html for action
            $row[] = '<a href="' . base_url() . 'group/detail?id=' . strtr(base64_encode($dt->groupid), '+/=', '._-') . '" id="btDetail" class="btn btn-sm btn-outline-dark" title="Detail"><i class="fa fa-eye"></i> Detail</a>&nbsp;
            <a href="' . base_url() . 'group/edit?id=' . strtr(base64_encode($dt->groupid), '+/=', '._-') . '" class="btn btn-sm btn-outline-primary act edit collapse-show" title="Edit"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
            <a href="group/delete?id=' . strtr(base64_encode($dt->groupid), '+/=', '._-') . '" class="btn btn-sm btn-outline-danger btn-delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Group_model->count_all(),
            "recordsFiltered" => $this->Group_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $this->form_validation->set_rules('groupname', 'Group Name', 'required|is_unique[sales_group.groupname]', [
            'is_unique' => 'This Group Name has already exist'
        ]);

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors);
            $response['status'] = false;
        } else {
            $response['status'] = true;
            return $this->Group_model->addGroup();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function edit()
    {
        $groupid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($groupid == null) {
            return redirect('group');
        }

        $data['row'] = $this->Group_model->getById($groupid);
        $data['detail'] = $this->Group_model->getDetailId($groupid);
        $data['user'] = $this->Group_model->getUser();
        echo json_encode($data);
    }

    public function detail()
    {
        $groupid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($groupid == null) {
            return redirect('group');
        }

        $data['login'] = $this->Auth_model->getUserLogin($this->session->userdata('username'));
        $data['title'] = "User Group Detail";
        $data['no'] = 1;
        $data['group'] = $this->Group_model->getDetailGroup($groupid);
        $data['row'] = $this->Group_model->getById($groupid);
        $data['user'] = $this->Group_model->getUser($groupid);

        $this->load->view('_partials/header', $data);
        $this->load->view('_partials/sidebar');
        $this->load->view('group/detail', $data);
        $this->load->view('group/modal_group', $data);
        $this->load->view('_partials/footer');
    }

    public function add_user()
    {
        $this->Group_model->addUser();
        return redirect('group');
    }

    public function update()
    {
        $this->form_validation->set_rules('groupname', 'Group Name', 'required');

        if ($this->form_validation->run() == false) {
            $errors = [];
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
        } else {
            return $this->Group_model->editGroup();
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    public function delete()
    {
        $groupid = base64_decode(strtr($this->input->get('id'), '._-', '+/='));
        if ($groupid == null) {
            return redirect('group');
        }
        return $this->Group_model->deleteGroup($groupid);
    }
}

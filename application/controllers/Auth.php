<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('Auth_model');
    }
    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('dashboard');
        }
        $data['title'] = "INVINIX Stock | Login";

        $this->load->view('_partials/auth_header', $data);
        $this->load->view('auth/login');
        $this->load->view('_partials/auth_footer');
        // if ($this->form_validation->run() == false) {
        // } else {
        //     $this->_login();
        // }
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->db->join('loginlevel', 'loginlevel.id = login_web.loginlevel')
            ->get_where('login_web', ['username' => $username])->row();
        // if ($user != 1) {
        //     $user = $this->db->get_where('login_web', ['email' => $username])->row();
        // }

        if ($user) {
            if ($user->isactive == 1) {
                if (password_verify($password, $user->password)) {
                    $data = [
                        'username' => $user->username,
                        'level' => $user->level,
                    ];

                    $this->session->set_userdata($data);
                    $response['status'] = true;
                } else {
                    $response['errors'] = 'Invalid Password!';
                    $response['status'] = false;
                    // $this->_messege('Invalid Password!', 'auth', 'danger');
                }
            } else {
                $response['errors'] = 'This Email has not been activated! click this link to activated your email : <b><a class="text-primary" href="' . base_url('auth/activated?email=' . $user->email) . '">link</a></b>';
                $response['status'] = false;
                // $this->_messege('This Email has not been activated! click this link to activated your email : <b><a class="text-primary" href="' . base_url('auth/activated?email=' . $user->email) . '">link</a></b>', 'auth', 'danger');
            }
        } else {
            $response['errors'] = 'Username is not registered!';
            $response['status'] = false;
            // $this->_messege('Username or Email is not registered!', 'auth', 'danger');
        }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

    private function _sendEmail($token, $type, $dname, $email = null)
    {
        //setting email
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'muhamadhafidz486@gmail.com',
            'smtp_pass' => 'anandafauziah',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];

        $this->email->initialize($config);

        $this->email->from('muhamadhafidz486@gmail.com', 'Jasa Solution');
        if ($email != null) {
            $this->email->to($email);
        } else {
            $this->email->to($this->input->post('email'));
        }

        if ($type == 'verify') {
            $this->email->subject('Account Verification INVINIX Dashboard');
            $this->email->message('<p>DEAR ' . $dname . '</p><p>Please! Verify your account?click link below to continue<p>
            <a href="' . base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '">' . base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '</a><br>
            <p>If this action not created by you,IGNORE this and verify your account as soon as posible</p><br>
            <p>Thank</p>');
        } else if ($type == 'forgot') {
            $this->email->subject('Forgot Password INVINIX Dashboard');
            $this->email->message(
                '<p>DEAR ' . $dname . '</p><p>You have looking for new Password?click link below to continue<p>
                <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '</a><br>
                <p>If this action not created by you,IGNORE this and change your password as soon as posible</p><br>
                <p>Thank</p>'
            );
        }

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function activated()
    {
        $email = $this->input->get('email');
        $user = $this->db->get_where('login_web', ['email' => $email])->row();
        $dname = $user->displayname;
        if ($user) {
            $token = base64_encode(random_bytes(32));
            $login_token = [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];

            $this->db->insert('login_token', $login_token);
            $this->_sendEmail($token, 'verify', $dname, $email);

            $this->_messege('Please check your email to activated your account!', 'auth', 'success');
        }
    }
    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('login_web', ['email' => $email])->row();

        if ($user) {
            $login_token = $this->db->get_where('login_token', ['token' => $token])->row();

            if ($login_token) {
                if (time() - $login_token->date_created < (60 * 60 * 24)) {
                    $this->db->set('isactive', 1);
                    $this->db->where('email', $email);
                    $this->db->update('login_web');

                    $this->db->delete('login_token', ['email' => $email]);

                    $this->_messege($email . ' has been activated! Please login.', 'auth', 'success');
                } else {
                    $this->db->delete('login_token', ['email' => $email]);

                    $this->_messege('Account activation failed! Token expired.', 'auth', 'danger');
                }
            } else {
                $this->_messege('Account activation failed! Wrong token.', 'auth', 'danger');
            }
        } else {
            $this->_messege('Account activation failed! Wrong email.', 'auth', 'danger');
        }
    }

    public function forgotPassword()
    {
        $data['title'] = "INVINIX Stock | Forgot Password";
        if ($this->session->userdata('username')) {
            redirect('dashboard');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == false) {
            $this->load->view('_partials/auth_header', $data);
            $this->load->view('auth/forgot-password');
            $this->load->view('_partials/auth_footer');
        } else {
            $email = $this->input->post('email');
            $user = $this->db->get_where('login_web', ['email' => $email, 'isactive' => 1])->row();
            $dname = $user->displayname;
            if ($user) {
                $token = base64_encode(random_bytes(32));
                $login_token = [
                    'email' => $email,
                    'token' => $token,
                    'date_created' => time()
                ];

                $this->db->insert('login_token', $login_token);
                $this->_sendEmail($token, 'forgot', $dname);

                $this->_messege('Please check your email to reset your password!', 'auth/forgotpassword', 'success');
            } else {
                $this->_messege('Email is not registered or activated!', 'auth/forgotpassword', 'danger');
            }
        }
    }

    public function resetPassword()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('login_web', ['email' => $email])->row();

        if ($user) {
            $login_token = $this->db->get_where('login_token', ['token' => $token])->row();
            if (time() - $login_token->date_created < 3600) {
                if ($login_token) {
                    $this->session->set_userdata('reset_email', $email);
                    $this->changePassword();
                } elseif (empty($login_token)) {
                    $this->_messege('This link has already use.', 'auth', 'danger');
                } else {
                    $this->_messege('Reset password failed! Wrong token.', 'auth', 'danger');
                }
            } else {
                $this->db->delete('login_token', ['email' => $email]);

                $this->_messege('Forgot password failed! Link expired.', 'auth', 'danger');
            }
        } else {
            $this->_messege('Reset password failed! Wrong email.', 'auth', 'danger');
        }
    }


    public function changePassword()
    {
        $data['title'] = "INVINIX Stock | Change Password";
        if ($this->session->userdata('username')) {
            redirect('dashboard');
        }
        if (!$this->session->userdata('reset_email')) {
            redirect('auth');
        }

        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]', [
            'min_length' => 'Password too short'
        ]);
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|min_length[4]|matches[password]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('_partials/auth_header', $data);
            $this->load->view('auth/change-password');
            $this->load->view('_partials/auth_footer');
        } else {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');

            $this->db->set('password', $password);
            $this->db->where('email', $email);
            $this->db->update('login_web');

            $this->session->unset_userdata('reset_email');

            $this->db->delete('login_token', ['email' => $email]);

            $this->_messege('Password has been changed! Please login.', 'auth', 'success');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->_messege('You have been logged out', 'auth', 'success');
    }
    private function _messege($message, $url, $alert)
    {
        $this->session->set_flashdata('message', '<div class="alert alert-' . $alert . '" role="alert">' . $message . '</div>');
        redirect($url);
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Receive_model extends CI_Model
{
    private $table = "invreq";
    var $column_order = array(null, 'invreq.reqnum', 'descr', 'reqdate', 'descsupplier', 'actionby', 'stat', null); //set column field database for datatable orderable
    var $column_search = array('invreq.reqnum', 'descr', 'reqdate', 'descsupplier', 'actionby', 'stat'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('invreq.reqnum' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        // $this->db->join('invreqdt', 'invreqdt.reqnum = invreq.reqnum');
        $this->db->join('warehouse', 'warehouse.kdwarehouse = invreq.kdwarehouse');
        $this->db->join('supplier', 'supplier.kdsupplier = invreq.idsupplier');

        if ($this->input->post('kdwarehouse')) {
            $this->db->where('invreq.kdwarehouse', $this->input->post('kdwarehouse'));
        }

        if ($this->input->post('reqdate')) {
            $date = explode('-', $this->input->post('reqdate'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('reqdate BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($reqnum)
    {
        $this->db->join('supplier', 'supplier.kdsupplier = invreq.idsupplier');
        return $this->db->get_where($this->table, ['reqnum' => $reqnum])->row();
    }

    public function getDetailTrans($reqnum)
    {
        $this->db->select('*');
        $this->db->from('itemmaster as item');
        $this->db->join('invreqdt detail', 'detail.kditem = item.kditem');
        $this->db->where('detail.reqnum', $reqnum);
        return $this->db->get();
    }

    public function getItemByTrans($reqnum)
    {
        $row = $this->getDetailTrans($reqnum);
        $data = $row->result();
        $row = [];
        foreach ($data as $dt) {
            $row[] = $dt->kditem;
        }
        $this->db->where_in('kditem', $row);
        return $this->db->get('itemmaster')->result();
    }

    public function getItemWhByTrans($reqnum, $warehouse)
    {
        $row = $this->getDetailTrans($reqnum);
        $data = $row->result();
        $row = [];
        foreach ($data as $dt) {
            $row[] = $dt->kditem;
        }
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where_in('kditem', $row);
        return $this->db->get('detailitemwh');
    }

    public function checkItem($warehouse, $slot, $item)
    {
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('slotid', $slot);
        $this->db->where('kditem', $item);
        $query = $this->db->get('detailitemwh')->row();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function getReqnumDetail($reqnum, $item)
    {
        $this->db->where('reqnum', $reqnum);
        $this->db->where('kditem', $item);
        $query = $this->db->get('invreqdt')->row();
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function getDate()
    {
        $this->db->select_min('reqdate');
        $query = $this->db->get($this->table)->row();
        if ($query->reqdate == NULL) {
            return time();
        } else {
            $date = strtotime($query->reqdate);
            return $date;
        }
    }

    public function getReqNum()
    {
        $this->db->select_max('reqnum');
        $query = $this->db->get($this->table)->row();
        $inv = $query->reqnum;
        if ($inv == NULL) {
            $reqnum = "PROC00001";
        } else {
            $ID = substr($inv, 4, 5);
            $No = $ID + 1;
            $reqnum = "PROC" . sprintf("%05s", $No);
        }
        return $reqnum;
    }

    public function searchReceive($reqnum, $key)
    {
        $this->db->select('reqnum,qtyoutstanding,qtyreceived,invreqdt.kditem,itemname,qty,invreqdt.status,brand');
        $this->db->from('invreqdt');
        $this->db->join('itemmaster', 'itemmaster.kditem = invreqdt.kditem');
        $this->db->where('reqnum', $reqnum);
        $this->db->where_not_in('invreqdt.status', "COMPLETE");
        $this->db->group_start();
        $this->db->like('itemname', $key);
        $this->db->or_like('invreqdt.kditem', $key);
        $this->db->group_end();
        return $this->db->get()->result();
    }

    public function getReceive($reqnum)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = invreqdt.kditem');
        $this->db->where('reqnum', $reqnum);
        $this->db->where_not_in('invreqdt.status', "COMPLETE");
        return $this->db->get('invreqdt')->result();
    }

    public function completeTrans($reqnum)
    {
        $this->db->where('reqnum', $reqnum);
        $this->db->update($this->table, [
            'stat' => "COMPLETE",
        ]);

        $row = $this->db->get_where('invreqdt', ['reqnum' => $reqnum])->result();
        $data = [];

        foreach ($row as $rw) {
            $data[] = $rw->qty;
        }

        for ($i = 0; $i < count($row); $i++) {
            $this->db->where('reqnum', $reqnum);
            $this->db->update('invreqdt', [
                'status' => "COMPLETE",
                'qtyreceived' => $data[$i],
                'qtyoutstanding' => 0,
            ]);
        }
    }

    public function receivedItem($reqnum)
    {
        $item = $this->input->post('item', true);
        $receiveOld = $this->input->post('receiveold', true);
        $qtyR = $this->input->post('qtyreceive', true);
        $qtyOut = $this->input->post('qtyoutstanding', true);
        $num = count($item);


        for ($i = 0; $i < $num; $i++) {
            $sumQtyOut[] = $qtyOut[$i] - $qtyR[$i];
        }

        if (array_sum($sumQtyOut) == 0) {
            $stat = "COMPLETE";
        } else {
            $stat = "PARTIAL";
        }

        $this->db->where('reqnum', $reqnum);
        $this->db->update($this->table, [
            'stat' => $stat,
        ]);

        for ($i = 0; $i < $num; $i++) {
            if ($qtyOut[$i] - $qtyR[$i] == 0) {
                $status = "COMPLETE";
            } else {
                $status = "PARTIAL";
            }

            $this->db->where('reqnum', $reqnum);
            $this->db->where('kditem', $item[$i]);
            $this->db->update('invreqdt', [
                'status' => $status,
                'qtyreceived' => $receiveOld[$i] + $qtyR[$i],
                'qtyoutstanding' => $qtyOut[$i] - $qtyR[$i],
            ]);
        }
    }

    public function addReceive()
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $totalrp = $this->input->post('total', true);
        $qty = $this->input->post('qty', true);
        $hpprp = $this->input->post('hpp', true);
        $uom = $this->input->post('uom', true);
        $remarks = $this->input->post('remarks', true);
        $status = $this->input->post('status', true);

        $hpp = str_replace(',', '', $hpprp);
        $total = str_replace(',', '', $totalrp);
        $num = count($item);
        $reqnum = $this->getReqNum();

        $this->db->insert($this->table, [
            'reqnum' => $reqnum,
            'descr' => $this->input->post('description', true),
            'reqdate' => date('Y-m-d'),
            'reqby' => $this->session->userdata('username'),
            'kdwarehouse' => $warehouse,
            'actionby' => $this->session->userdata('username'),
            'stat' => $status,
            'idsupplier' => $this->input->post('supplier', true),
            'typereceive' => $this->input->post('type', true),
        ]);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('invreqdt', [
                'reqnum' => $reqnum,
                'kditem' => $item[$i],
                'kdwarehouse' => $warehouse,
                'qty' => $qty[$i],
                'remarks' => $remarks[$i],
                'issueunit' => $uom[$i],
                'price' => $hpp[$i],
                'slotid' => $slot,
                'total' => $total[$i],
                'status' => $status,
                'qtyreceived' => 0,
                'qtyoutstanding' => $qty[$i],
                'dtreceipt' => date('Y-m-d H:I:S'),
            ]);
        }
        if ($status == "OPEN") {
            $this->AddMutasi($reqnum);
            $this->updateWh();
        }
    }

    public function updateReceive()
    {
        $item = $this->input->post('item', true);
        $reqnum = $this->input->post('reqnum', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $totalrp = $this->input->post('total', true);
        $qty = $this->input->post('qty', true);
        $hpprp = $this->input->post('hpp', true);
        $uom = $this->input->post('uom', true);
        $remarks = $this->input->post('remarks', true);
        $status = $this->input->post('status', true);

        $hpp = str_replace(',', '', $hpprp);
        $total = str_replace(',', '', $totalrp);
        $num = count($item);

        $this->db->where('reqnum', $reqnum);
        $this->db->update($this->table, [
            'descr' => $this->input->post('description', true),
            'kdwarehouse' => $warehouse,
            'stat' => $status,
            'idsupplier' => $this->input->post('supplier', true),
            'typereceive' => $this->input->post('type', true),
        ]);

        $this->db->delete('invreqdt', ['reqnum' => $reqnum]);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('invreqdt', [
                'reqnum' => $reqnum,
                'kditem' => $item[$i],
                'kdwarehouse' => $warehouse,
                'qty' => $qty[$i],
                'remarks' => $remarks[$i],
                'issueunit' => $uom[$i],
                'price' => $hpp[$i],
                'slotid' => $slot,
                'total' => $total[$i],
                'status' => $status,
                'qtyreceived' => 0,
                'qtyoutstanding' => $qty[$i],
                'dtreceipt' => date('Y-m-d H:I:S'),
            ]);
        }

        if ($status == "OPEN") {
            $this->AddMutasi($reqnum);
            $this->updateWh();
        }
    }

    public function updateWh()
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $qty = $this->input->post('qty', true);
        $price = $this->input->post('price', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);
        $hpprp = $this->input->post('hpp', true);

        $hpp = str_replace(',', '', $hpprp);
        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            if ($this->checkItem($warehouse, $slot, $item[$i]) == true) {
                $this->db->where('kdwarehouse', $warehouse);
                $this->db->where('slotid', $slot);
                $this->db->where('kditem', $item[$i]);
                $this->db->update('detailitemwh', [
                    'lastin' => $qty[$i],
                    'curbal' => $curbal[$i] + $qty[$i],
                    'issueunit' => $uom[$i]
                ]);
            } else {
                $this->db->insert('detailitemwh', [
                    'kditem' => $item[$i],
                    'kdwarehouse' => $warehouse,
                    'slotid' => $slot,
                    'curbal' => $qty[$i],
                    'lastin' => $qty[$i],
                    'cost' => $hpp[$i],
                    'hargajual' => $price[$i],
                    'issueunit' => $uom[$i],
                ]);
            }
        }
    }

    public function AddMutasi($reqnum)
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $qty = $this->input->post('qty', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);

        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('mutasi_stock', [
                'noref' => $reqnum,
                'refname' => "Purchase Receiving",
                'tanggal' => date('Y-m-d'),
                'kditem' => $item[$i],
                'kdwh' => $warehouse,
                'slotid' => $slot,
                'unit' => $uom[$i],
                'awal' => $curbal[$i],
                'masuk' => $qty[$i],
                'balance' => $curbal[$i] + $qty[$i],
            ]);
        }
    }
}

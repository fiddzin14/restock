<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SubMenu_model extends CI_Model
{
    private $table = "user_sub_menu";
    var $column_order = array(null, 'menu', 'title', 'url', 'icon', 'isactive', null);
    var $column_search = array('menu', 'title', 'url', 'icon',  'isactive');
    var $order = array('subid' => 'asc');

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        $this->db->join('user_menu', 'user_menu.menuid = user_sub_menu.menu_id');
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getAll()
    {
        return $this->db->get($this->table)->result();
    }

    public function getById($id)
    {
        $this->db->join('user_menu', 'user_menu.menuid = user_sub_menu.menu_id');
        return $this->db->get_where($this->table, ['subid' => $id])->row();
    }

    public function addSub()
    {
        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'menu_id' => $this->input->post('menu'),
            'title' => $this->input->post('title'),
            'url' => $this->input->post('url'),
            'icon' => $this->input->post('icon'),
            'isactive' => $isactive,
        ];

        $this->db->insert($this->table, $data);
    }

    public function editSub()
    {
        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'menu_id' => $this->input->post('menu'),
            'title' => $this->input->post('title'),
            'url' => $this->input->post('url'),
            'icon' => $this->input->post('icon'),
            'isactive' => $isactive,
        ];

        $this->db->where('subid', $this->input->post('id'));
        $this->db->update($this->table, $data);
    }

    public function deleteSub($id)
    {
        $this->db->delete($this->table, ['subid' => $id]);
    }
}

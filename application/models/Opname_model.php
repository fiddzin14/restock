<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Opname_model extends CI_Model
{
    private $table = "stopname";
    var $column_order = array(null, 'idopname', 'stodate', 'notes', 'status', 'countitems', 'kdwh', null); //set column field database for datatable orderable
    var $column_search = array('idopname', 'stodate', 'notes', 'status', 'countitems', 'kdwh'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('idopname' => 'asc');

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->join('warehouse', 'warehouse.kdwarehouse = stopname.kdwh');

        if ($this->input->post('kdwh')) {
            $this->db->where('kdwh', $this->input->post('kdwh'));
        }

        if ($this->input->post('stodate')) {
            $date = explode('-', $this->input->post('stodate'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('stodate BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getID()
    {
        $this->db->select_max('idopname');
        $query = $this->db->get($this->table)->row();
        $inv = $query->idopname;
        if ($inv == NULL) {
            $idopname = "OP00001";
        } else {
            $ID = substr($inv, 4, 5);
            $No = $ID + 1;
            $idopname = "OP" . sprintf("%05s", $No);
        }
        return $idopname;
    }

    public function addOpname()
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        // $slot = $this->input->post('slot', true);
        $cost = $this->input->post('cost', true);
        $qtyact = $this->input->post('qtyact', true);
        $qtydiff = $this->input->post('qtydif', true);
        $qtysys = $this->input->post('qtysys', true);
        $num = count($item);

        $id = $this->getID();

        $this->db->insert($this->table, [
            'idopname' => $id,
            'stodate' => date('Y-m-d'),
            'notes' => $this->input->post('notes', true),
            'status' => "COMPLETE",
            'countitems' => $num,
            'totalqty' => array_sum($qtyact),
            'totaldiff' => array_sum($qtydiff),
            'amount' => 0,
            'amountdiff' => 0,
            'kdwh' => $warehouse,
        ]);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('stopname_dt', [
                'idopname' => $id,
                'kditem' => $item[$i],
                'slotid' => "GUDANG",
                'qty_sys' => $qtysys[$i],
                'qty_act' => $qtyact[$i],
                'qty_diff' => $qtydiff[$i],
                'unitcost' => $cost[$i],
                'totalcost' => $cost[$i] * $qtyact[$i],
            ]);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Warehouse_model extends CI_Model
{
    private $table = "warehouse";
    var $column_order = array(null, 'kdwarehouse', 'descwarehouse', 'address', 'contact', null); //set column field database for datatable orderable
    var $column_search = array('kdwarehouse', 'descwarehouse', 'address', 'contact'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('idwarehouse' => 'asc'); // default order 

    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($idwarehouse)
    {
        return $this->db->get_where($this->table, ['idwarehouse' => $idwarehouse])->row();
    }

    public function getWarehouse()
    {
        return $this->db->get($this->table)->result();
    }

    public function getToWarehouse($warehouse)
    {
        $this->db->where_not_in('kdwarehouse', $warehouse);
        return $this->db->get($this->table)->result();
    }

    public function getProduct($warehouse)
    {
        $slot = $this->input->post('slotid');
        $this->db->join('itemmaster', 'itemmaster.kditem = detailitemwh.kditem');
        $this->db->where_not_in('curbal', 0);
        $this->db->where('kdwarehouse', $warehouse);
        if ($slot != "") {
            $this->db->where('slotid', $slot);
        }
        return $this->db->get('detailitemwh')->result();
    }

    function sumQtyProduct($warehouse)
    {
        $slot = $this->input->post('slotid');
        $this->db->select_sum('curbal', 'qty');
        $this->db->where_not_in('curbal', 0);
        $this->db->where('kdwarehouse', $warehouse);
        if ($slot != "") {
            $this->db->where('slotid', $slot);
        }
        $this->db->from('detailitemwh');
        return $this->db->get()->row();
    }

    public function addWarehouse()
    {
        $data = [
            'kdwarehouse' => $this->input->post('warehouse', true),
            'descwarehouse' => $this->input->post('desc', true),
            'address' => $this->input->post('address', true),
            'contact' => $this->input->post('contact', true),
        ];

        $this->db->insert($this->table, $data);
    }

    public function editWarehouse()
    {
        $data = [
            'kdwarehouse' => $this->input->post('warehouse', true),
            'descwarehouse' => $this->input->post('desc', true),
            'address' => $this->input->post('address', true),
            'contact' => $this->input->post('contact', true),
        ];

        $this->db->where('idwarehouse', $this->input->post('idwarehouse'));
        $this->db->update($this->table, $data);
    }

    public function deleteWarehouse($idwarehouse)
    {
        $this->db->delete($this->table, ['idwarehouse' => $idwarehouse]);
    }
}

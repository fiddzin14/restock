<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Movement_model extends CI_Model
{
    private $table = "invuse";
    var $column_order = array(null, 'invusenum', 'description', 'usedate', 'fromwarehouse', 'towarehouse', 'status', null); //set column field database for datatable orderable
    var $column_search = array('invusenum', 'description', 'usedate', 'fromwarehouse', 'towarehouse', 'status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('invusenum' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        // $this->db->join('sales', 'sales.salesid = transorder.salesid');
        // $this->db->join('customer', 'customer.custid = transorder.custid');
        if ($this->input->post('fromwarehouse')) {
            $this->db->where('fromwarehouse', $this->input->post('fromwarehouse'));
        }
        if ($this->input->post('usedate')) {
            $date = explode('-', $this->input->post('usedate'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('usedate BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($invusenum)
    {
        $this->db->join('detailinvuse', 'detailinvuse.invusenum = invuse.invusenum');
        return $this->db->get_where($this->table, ['invuse.invusenum' => $invusenum])->row();
    }

    public function getDetailTrans($invusenum)
    {
        $this->db->select('*');
        $this->db->from('itemmaster as item');
        $this->db->join('detailinvuse as detail', 'detail.kditem = item.kditem');
        $this->db->where('detail.invusenum', $invusenum);
        return $this->db->get();
    }

    public function getItemByTrans($invusenum)
    {
        $row = $this->getDetailTrans($invusenum);
        $data = $row->result();
        $row = [];
        foreach ($data as $dt) {
            $row[] = $dt->kditem;
        }
        $this->db->where_in('kditem', $row);
        return $this->db->get('itemmaster')->result();
    }

    public function getItemWhByTrans($invusenum, $warehouse)
    {
        $row = $this->getDetailTrans($invusenum);
        $data = $row->result();
        $row = [];
        foreach ($data as $dt) {
            $row[] = $dt->kditem;
        }
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where_in('kditem', $row);
        return $this->db->get('detailitemwh');
    }

    public function checkItem($invusenum)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = detailinvuse.kditem');
        $this->db->where('detailinvuse.invusenum', $invusenum);
        return $this->db->get('detailinvuse')->result();
    }

    public function getNum()
    {
        $this->db->select_max('invusenum');
        $query = $this->db->get($this->table)->row();
        return $query->invusenum;
    }

    public function getDate()
    {
        $this->db->select_min('usedate');
        $query = $this->db->get($this->table)->row();
        if ($query->usedate == NULL) {
            return time();
        } else {
            $date = strtotime($query->usedate);
            return $date;
        }
    }

    public function getDetail($toWarehouse, $toSlot, $item)
    {
        $this->db->where('kdwarehouse', $toWarehouse);
        $this->db->where('slotid', $toSlot);
        $this->db->where('kditem', $item);
        $query = $this->db->get('detailitemwh');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function completeTrans($invusenum)
    {
        $this->db->where('invusenum', $invusenum);
        return $this->db->update($this->table, [
            'status' => "COMPLETE",
        ]);
    }

    public function addTrans()
    {
        $item = $this->input->post('item', true);
        $fromWarehouse = $this->input->post('fromWarehouse', true);
        $toWarehouse = $this->input->post('toWarehouse', true);
        $fromSlot = $this->input->post('fromSlot', true);
        $toSlot = $this->input->post('toSlot', true);
        $qty = $this->input->post('qty', true);
        $status = $this->input->post('status', true);
        $num = count($item);

        $inv = $this->getNum();
        if ($inv == NULL) {
            $invusenum = "USE00001";
        } else {
            $ID = substr($inv, 3, 5);
            $No = $ID + 1;
            $invusenum = "USE" . sprintf("%05s", $No);
        }

        $data = [
            'invusenum' => $invusenum,
            'description' => $this->input->post('description', true),
            'usedate' => date('Y-m-d'),
            'fromwarehouse' => $fromWarehouse,
            'status' => $status,
            'towarehouse' => $toWarehouse,
            'issuedby' => $this->session->userdata('username'),
        ];

        $this->db->insert($this->table, $data);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('detailinvuse', [
                'invusenum' => $invusenum,
                'usetype' => $this->input->post('type', true),
                'kditem' => $item[$i],
                'qty' => $qty[$i],
                'trxwarehouse' => $toWarehouse,
                'fromslotid' => $fromSlot,
                'toslotid' => $toSlot,
            ]);
        }

        if ($status == "IN PROGRESS") {
            $this->addMutasi($invusenum);
            $this->addToWh();
        }
    }

    public function editTrans()
    {
        $item = $this->input->post('item', true);
        $invusenum = $this->input->post('invusenum', true);
        $fromWarehouse = $this->input->post('fromWarehouse', true);
        $toWarehouse = $this->input->post('toWarehouse', true);
        $fromSlot = $this->input->post('fromSlot', true);
        $toSlot = $this->input->post('toSlot', true);
        $qty = $this->input->post('qty', true);
        $status = $this->input->post('status', true);
        $num = count($item);

        $this->db->where('invusenum', $invusenum);
        $this->db->update($this->table, [
            'description' => $this->input->post('description', true),
            'usedate' => date('Y-m-d'),
            'fromwarehouse' => $fromWarehouse,
            'status' => $status,
            'towarehouse' => $toWarehouse,
            'issuedby' => $this->session->userdata('username'),
        ]);

        $this->db->delete('detailinvuse', ['invusenum' => $invusenum]);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('detailinvuse', [
                'invusenum' => $invusenum,
                'usetype' => $this->input->post('type', true),
                'kditem' => $item[$i],
                'qty' => $qty[$i],
                'trxwarehouse' => $toWarehouse,
                'fromslotid' => $fromSlot,
                'toslotid' => $toSlot,
            ]);
        }

        if ($status == "IN PROGRESS") {
            $this->addMutasi($invusenum);
            $this->addToWh();
        }
    }

    public function addMutasi($invusenum)
    {
        $item = $this->input->post('item', true);
        $fromWarehouse = $this->input->post('fromWarehouse', true);
        $toWarehouse = $this->input->post('toWarehouse', true);
        $fromSlot = $this->input->post('fromSlot', true);
        $toSlot = $this->input->post('toSlot', true);
        $qty = $this->input->post('qty', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);
        $curbal_to = $this->input->post('curbal_to', true);
        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('mutasi_stock', [
                'noref' => $invusenum,
                'refname' => "Stock Movement",
                'tanggal' => date('Y-m-d'),
                'kditem' => $item[$i],
                'kdwh' => $fromWarehouse,
                'slotid' => $fromSlot,
                'unit' => $uom[$i],
                'awal' => $curbal[$i],
                'keluar' => $qty[$i],
                'balance' => $curbal[$i] - $qty[$i],
            ]);
        }

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('mutasi_stock', [
                'noref' => $invusenum,
                'refname' => "Stock Movement",
                'tanggal' => date('Y-m-d'),
                'kditem' => $item[$i],
                'kdwh' => $toWarehouse,
                'slotid' => $toSlot,
                'unit' => $uom[$i],
                'awal' => $curbal_to[$i],
                'masuk' => $qty[$i],
                'balance' => $curbal_to[$i] + $qty[$i],
            ]);
        }
    }

    public function addToWh()
    {
        $item = $this->input->post('item', true);
        $fromWarehouse = $this->input->post('fromWarehouse', true);
        $toWarehouse = $this->input->post('toWarehouse', true);
        $fromSlot = $this->input->post('fromSlot', true);
        $toSlot = $this->input->post('toSlot', true);
        $qty = $this->input->post('qty', true);
        $cost = $this->input->post('cost', true);
        $hargajual = $this->input->post('hargajual', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);
        $curbal_to = $this->input->post('curbal_to', true);
        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            $this->db->where('kdwarehouse', $fromWarehouse);
            $this->db->where('slotid', $fromSlot);
            $this->db->where('kditem', $item[$i]);
            $this->db->update('detailitemwh', [
                'lastout' => $qty[$i],
                'curbal' => $curbal[$i] - $qty[$i],
                'issueunit' => $uom[$i]
            ]);
        }

        for ($i = 0; $i < $num; $i++) {
            if ($this->getDetail($toWarehouse, $toSlot, $item[$i]) == true) {
                $this->db->where('kdwarehouse', $toWarehouse);
                $this->db->where('slotid', $toSlot);
                $this->db->where('kditem', $item[$i]);
                $this->db->update('detailitemwh', [
                    'lastin' => $qty[$i],
                    'curbal' => $curbal_to[$i] + $qty[$i],
                    'issueunit' => $uom[$i]
                ]);
            } else {
                $this->db->insert('detailitemwh', [
                    'kditem' => $item[$i],
                    'kdwarehouse' => $toWarehouse,
                    'slotid' => $toSlot,
                    'curbal' => $qty[$i],
                    'lastin' => $qty[$i],
                    'cost' => $cost[$i],
                    'hargajual' => $hargajual[$i],
                    'issueunit' => $uom[$i],
                ]);
            }
        }
    }
}

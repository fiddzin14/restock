<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model
{
    private $table = "transorder";
    var $column_order = array(null, 'transid', 'transdt', 'actionby', 'custname', 'numofitem', 'transstatus', 'total', null); //set column field database for datatable orderable
    var $column_search = array('transid', 'transdt', 'actionby', 'custname', 'numofitem', 'transstatus', 'total'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('transid' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->join('customer', 'customer.custid = transorder.custid');
        if ($this->input->post('kdwh')) {
            $this->db->where('kdwh', $this->input->post('kdwh'));
        }
        if ($this->input->post('transdt')) {
            $date = explode('-', $this->input->post('transdt'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('transdt BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getByTransId($transid)
    {
        $this->db->join('customer', 'customer.custid = transorder.custid');
        return $this->db->get_where($this->table, ['transid' => $transid])->row();
    }

    public function getDetailTrans($transid)
    {
        $this->db->select('detail.qty,detail.kditem,detail.hargajual,itemname,brand,type');
        $this->db->from('itemmaster as item');
        $this->db->join('transorderdetail detail', 'detail.kditem = item.kditem');
        $this->db->where('detail.transid', $transid);
        return $this->db->get()->result();
    }

    public function getDate()
    {
        $this->db->select_min('transdt');
        $query = $this->db->get($this->table)->row();
        if ($query->transdt == NULL) {
            return time();
        } else {
            $date = strtotime($query->transdt);
            return $date;
        }
    }

    // function countTotal($transid)
    // {
    //     $this->db->select_sum('hargajual', 'price');
    //     $this->db->from('transorderdetail');
    //     $this->db->where('transid', $transid);
    //     return $this->db->get()->row();
    // }

    public function getTransID($prefix)
    {
        $this->db->select_max('transid');
        $query = $this->db->get($this->table)->row();
        $inv = $query->transid;
        if ($inv == NULL) {
            $transid = $prefix . "00001";
        } else {
            $ID = substr($inv, 4, 5);
            $No = $ID + 1;
            $transid = $prefix . sprintf("%05s", $No);
        }
        return $transid;
    }

    public function addOrder()
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $total = $this->input->post('total', true);
        $qty = $this->input->post('qty', true);
        $price = $this->input->post('price', true);
        $hpp = $this->input->post('hpp', true);
        $uom = $this->input->post('uom', true);
        $num = count($item);
        $totalhpp = [];

        for ($i = 0; $i < $num; $i++) {
            $totalhpp[] = $hpp[$i] * $qty[$i];
        }

        $transid = $this->getTransID('PO-');
        $donum = $this->getTransID('DO-');

        $price = str_replace(',', '', $price);
        $total = str_replace(',', '', $total);

        $this->db->insert($this->table, [
            'transid' => $transid,
            'transdt' => date('Y-m-d'),
            'salesid' => $this->session->userdata('username'),
            'custid' => $this->input->post('customer', true),
            'numofitem' => $num,
            'total' => array_sum($total),
            'transstatus' => "COMPLETE",
            'totalhpp' => array_sum($totalhpp),
            'actionby' => $this->session->userdata('username'),
            'duedate' => date('Y-m-d'),
            'donum' => $donum,
            'tipeppn' => "JKT",
            'notes' => $this->input->post('memo', true),
            'deliveryaddress' => $this->input->post('address', true),
            'kdwh' => $warehouse,
            'slotid' => $slot,
        ]);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('transorderdetail', [
                'transid' => $transid,
                'kditem' => $item[$i],
                'qty' => $qty[$i],
                'hargajual' => $price[$i],
                'kdwarehouse' => $warehouse,
                'slotid' => $slot,
                'uom' => $uom[$i],
                'is_return' => 0,
                'subtotal' => $total[$i],
                'hpp' => $hpp[$i],
            ]);
        }

        $this->addMutasi($transid);
        $this->addToWh();
    }

    public function addMutasi($transid)
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $qty = $this->input->post('qty', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);
        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            $this->db->insert('mutasi_stock', [
                'noref' => $transid,
                'refname' => "Sales Order",
                'tanggal' => date('Y-m-d'),
                'kditem' => $item[$i],
                'kdwh' => $warehouse,
                'slotid' => $slot,
                'unit' => $uom[$i],
                'awal' => $curbal[$i],
                'keluar' => $qty[$i],
                'balance' => $curbal[$i] - $qty[$i],
            ]);
        }
    }

    public function addToWh()
    {
        $item = $this->input->post('item', true);
        $warehouse = $this->input->post('warehouse', true);
        $slot = $this->input->post('slot', true);
        $qty = $this->input->post('qty', true);
        $uom = $this->input->post('uom', true);
        $curbal = $this->input->post('curbal', true);
        $num = count($item);

        for ($i = 0; $i < $num; $i++) {
            $this->db->where('kdwarehouse', $warehouse);
            $this->db->where('slotid', $slot);
            $this->db->where('kditem', $item[$i]);
            $this->db->update('detailitemwh', [
                'lastout' => $qty[$i],
                'curbal' => $curbal[$i] - $qty[$i],
                'issueunit' => $uom[$i]
            ]);
        }
    }
}

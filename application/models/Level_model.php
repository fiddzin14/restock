<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Level_model extends CI_Model
{
    private $table = 'loginlevel';

    public function getAll()
    {
        return $this->db->get($this->table)->result();
    }

    public function getById($level)
    {
        return $this->db->get_where($this->table, ['id' => $level])->row();
    }

    public function getAccess($level)
    {
        $this->db->select('sub_menu_id');
        $access = $this->db->get_where('user_access_menu', ['loginlevel' => $level])->result();
        $row = [];
        foreach ($access as $acc) {
            $row[] = $acc->sub_menu_id;
        }
        return $row;
    }

    public function getMenuBySub($sub)
    {
        $menu = $this->db->get_where('user_sub_menu', ['subid' => $sub])->row();
        return $menu->menu_id;
    }

    public function addLevel()
    {
        $data = [
            'level' => $this->input->post('level'),
            'createdate' => date('Y-m-d h:i:s'),
        ];

        $this->db->insert($this->table, $data);
    }

    public function SetAccess()
    {
        $sub = $this->input->post('sub');
        $level = $this->input->post('level');

        $this->db->delete('user_access_menu', ['loginlevel' => $level]);

        for ($i = 0; $i < count($sub); $i++) {
            $this->db->insert('user_access_menu', [
                'loginlevel' => $level,
                'menu_id' => $this->getMenuBySub($sub[$i]),
                'sub_menu_id' => $sub[$i],
                'views' => 1,
                'created' => 0,
                'updated' => 0,
                'deleted' => 0,
            ]);
        }
    }

    public function deleteLevel($id)
    {
        return $this->db->delete($this->table, ['id' => $id]);
    }
}

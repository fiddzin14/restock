<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SlotRack_model extends CI_Model
{
    private $table = "slot";
    var $column_order = array(null, 'slotid', 'slotnm', 'isactive', 'kdwarehouse', null);
    var $column_search = array('slotid', 'slotnm', 'isactive', 'kdwarehouse');
    var $order = array('slotid' => 'asc');

    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($slotid)
    {
        return $this->db->get_where($this->table, ['slotid' => $slotid])->row();
    }

    public function getSlotByWarehouse($warehouse)
    {
        return $this->db->get_where('slot', ['kdwarehouse' => $warehouse, 'isactive' => 1])->result();
    }

    public function countSlot($warehouse)
    {
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('isactive', 1);
        $this->db->from('slot');
        return $this->db->count_all_results();
    }

    public function addSlot()
    {
        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'slotid' => $this->input->post('slotid', true),
            'slotnm' => $this->input->post('slotnm', true),
            'isactive' => $isactive,
            'kdwarehouse' => $this->input->post('warehouse', true),
        ];

        $this->db->insert($this->table, $data);
    }

    public function editSlot()
    {
        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'slotid' => $this->input->post('slotid', true),
            'slotnm' => $this->input->post('slotnm', true),
            'isactive' => $isactive,
            'kdwarehouse' => $this->input->post('warehouse', true),
        ];

        $this->db->where('slotid', $this->input->post('slotid'));
        $this->db->update($this->table, $data);
    }

    public function deleteSlot($slotid)
    {
        $this->db->delete($this->table, ['slotid' => $slotid]);
    }
}

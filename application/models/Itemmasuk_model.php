<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Itemmasuk_model extends CI_Model
{
    public function where()
    {
        $kdwarehouse = $this->input->post('kdwarehouse');
        if ($kdwarehouse != "") {
            $this->db->where('KdWarehouse', $kdwarehouse);
        }
        if ($this->input->post('transdt')) {
            $data = explode('-', $this->input->post('transdt'));
            $SDate = date('Y-m-d 00:00:00', strtotime($data[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($data[1]));
            $this->db->where('TransDt BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }
    }

    function getAll()
    {
        $this->where();
        return $this->db->get('view_laporanitemmasuk')->result();
    }

    public function getDate()
    {
        $this->db->select_min('TransDt');
        $query = $this->db->get('view_laporanitemmasuk')->row();
        if ($query->TransDt == NULL) {
            return time();
        } else {
            $date = strtotime($query->TransDt);
            return $date;
        }
    }

    function sumCount()
    {
        $this->db->select_sum('JumlahMasuk', 'qty');
        $this->where();
        $this->db->from('view_laporanitemmasuk');
        return $this->db->get();
    }
}

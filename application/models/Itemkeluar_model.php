<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Itemkeluar_model extends CI_Model
{
    public function where()
    {
        $kdwarehouse = $this->input->post('kdwarehouse');
        if ($kdwarehouse != "") {
            $this->db->where('FromWarehouse', $kdwarehouse);
        }
        if ($this->input->post('transdt')) {
            $data = explode('-', $this->input->post('transdt'));
            $SDate = date('Y-m-d 00:00:00', strtotime($data[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($data[1]));
            $this->db->where('usedate BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }
    }

    public function getAll()
    {
        $this->where();
        return $this->db->get('view_laporanitemkeluar')->result();
    }

    public function getDate()
    {
        $this->db->select_min('usedate');
        $query = $this->db->get('view_laporanitemkeluar')->row();
        if ($query->usedate == NULL) {
            return time();
        } else {
            $date = strtotime($query->usedate);
            return $date;
        }
    }

    public function sumCount()
    {
        $this->db->select_sum('JumlahKeluar', 'qty');
        $this->where();
        $this->db->from('view_laporanitemkeluar');
        return $this->db->get();
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class To_warehouse_model extends CI_Model
{
    private $table = "detailitemwh";
    var $column_order = array('descwarehouse', 'slotnm', 'lastin', 'lastout', 'curbal', 'issueunit', 'qty_reserved'); //set column field database for datatable orderable
    var $column_search = array('descwarehouse', 'slotnm', 'lastin', 'lastout', 'curbal', 'issueunit', 'qty_reserved'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('descwarehouse' => 'asc'); // default order 

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        $this->db->join('warehouse', 'warehouse.kdwarehouse = detailitemwh.kdwarehouse');
        $this->db->join('slot', 'slot.slotid = detailitemwh.slotid');
        $this->db->where_not_in('curbal', 0);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($kditem)
    {
        $this->_get_datatables_query();
        $this->db->where('kditem', $kditem);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($kditem)
    {
        $this->_get_datatables_query();
        $this->db->where('kditem', $kditem);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($kditem)
    {
        $this->db->from($this->table);
        $this->db->where('kditem', $kditem);
        return $this->db->count_all_results();
    }

    public function getById($kditem)
    {
        $this->db->join('category', 'category.catid = itemmaster.catid');
        $this->db->join('uom', 'uom.uom = itemmaster.uom');
        return $this->db->get_where($this->table, ['kditem' => $kditem])->row();
    }

    public function getNoref()
    {
        $this->db->like('noref', 'ID');
        $this->db->select_max('noref');
        $query = $this->db->get('mutasi_stock')->row();
        return $query->noref;
    }

    public function addtowh()
    {
        $hargamodal = explode(",", $this->input->post('hargamodal', true));
        $hargajual = explode(",", $this->input->post('hargajual', true));
        $qty = $this->input->post('qty');

        $data = [
            'kditem' => $this->input->post('kditem', true),
            'kdwarehouse' => $this->input->post('warehouse', true),
            'slotid' => $this->input->post('slot', true),
            'cost' => implode($hargamodal),
            'hargajual' => implode($hargajual),
            'curbal' => $qty,
            'lastin' => $qty,
            'issueunit' => $this->input->post('uom', true),
        ];
        $this->db->insert($this->table, $data);
    }

    public function updatetowh($curbal, $warehouse, $slot)
    {
        $hargamodal = explode(",", $this->input->post('hargamodal', true));
        $hargajual = explode(",", $this->input->post('hargajual', true));
        $qty = $this->input->post('qty');

        $data = [
            'kditem' => $this->input->post('kditem', true),
            'kdwarehouse' => $this->input->post('warehouse', true),
            'slotid' => $this->input->post('slot', true),
            'cost' => implode($hargamodal),
            'hargajual' => implode($hargajual),
            'curbal' => $qty + $curbal,
            'lastin' => $qty,
            'issueunit' => $this->input->post('uom', true),
        ];

        $this->db->where('kditem', $this->input->post('kditem'));
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('slotid', $slot);
        $this->db->update($this->table, $data);
    }

    public function stockMutasi($curbal)
    {
        $ref = $this->getNoref();
        if ($ref == NULL) {
            $noref = "ID00001";
        } else {
            $ID = substr($ref, 2, 5);
            $No = $ID + 1;
            $noref = "ID" . sprintf("%05s", $No);
            $qty = $this->input->post('qty');
        }

        $data = [
            'noref' => $noref,
            'refname' => "&Add Item to Warehouse",
            'tanggal' => date('Y-m-d H:i:s'),
            'kditem' => $this->input->post('kditem', true),
            'kdwh' => $this->input->post('warehouse', true),
            'slotid' => $this->input->post('slot', true),
            'unit' => $this->input->post('uom', true),
            'awal' => $curbal,
            'masuk' => $qty,
            'balance' => $curbal + $qty,
        ];
        $this->db->insert('mutasi_stock', $data);
    }
}

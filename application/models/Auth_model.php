<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function getUser()
    {
        return $this->db->get('sales')->result();
    }
    public function add()
    {
        if ($this->input->post('isActive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'username' => $this->input->post('username'),
            'displayname' => $this->input->post('displayName'),
            'email' => $this->input->post('email'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'isactive' => $isactive,
        ];

        $this->db->insert('login_web', $data);
    }

    public function getUserLogin($username)
    {
        $this->db->join('loginlevel', 'loginlevel.id = login_web.loginlevel');
        return $this->db->get_where('login_web', ['username' => $username])->row();
    }

    public function getSales($username)
    {
        return $this->db->get_where('sales', ['username' => $username])->row();
    }
}

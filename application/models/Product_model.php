<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $table = "itemmaster";
    var $column_order = array(null, 'kditem', 'itemname', 'brand', 'catname', 'stocktype', null); //set column field database for datatable orderable
    var $column_search = array('kditem', 'itemname', 'brand', 'catname', 'stocktype'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('kditem' => 'asc'); // default order 

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        $this->db->join('category', 'category.catid = itemmaster.catid');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getAllItem()
    {
        return $this->db->get($this->table)->result();
    }

    function searchItem($key, $item)
    {
        if ($item != null or $item != '') {
            for ($i = 0; $i < count($item); $i++) {
                $this->db->where_not_in('kditem', $item[$i]['value']);
            }
        }
        $this->db->group_start();
        $this->db->like('itemname', $key);
        $this->db->or_like('kditem', $key);
        $this->db->group_end();
        $this->db->order_by('kditem', 'ASC');
        $this->db->limit(10);
        return $this->db->get($this->table)->result();
    }

    public function getAllItemExcapt($query)
    {
        $row = [];
        foreach ($query as $dt) {
            $row[] = $dt->kditem;
        }
        $this->db->where_not_in('kditem', $row);
        return $this->db->get($this->table)->result();
    }

    public function getItemWarehouse($warehouse)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = detailitemwh.kditem');
        $this->db->where('kdwarehouse', $warehouse);
        return $this->db->get('detailitemwh')->result();
    }

    public function getItemByWh($warehouse)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = detailitemwh.kditem');
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where_not_in('curbal', 0);
        return $this->db->get('detailitemwh')->result();
    }

    public function searchItemByWarehouse($key, $item, $warehouse, $slot)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = detailitemwh.kditem');
        $this->db->group_start();
        $this->db->like('itemmaster.itemname', $key);
        $this->db->or_like('detailitemwh.kditem', $key);
        $this->db->group_end();
        if ($item != null or $item != '') {
            for ($i = 0; $i < count($item); $i++) {
                $this->db->where_not_in('detailitemwh.kditem', $item[$i]['value']);
            }
        }
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('slotid', $slot);
        $this->db->where_not_in('curbal', 0);
        $this->db->order_by('detailitemwh.kditem', 'ASC');
        $this->db->limit(10);
        return $this->db->get('detailitemwh')->result();
    }

    public function getItemByWarehouse($warehouse, $slot)
    {
        $this->db->join('itemmaster', 'itemmaster.kditem = detailitemwh.kditem');
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('slotid', $slot);
        $this->db->where_not_in('curbal', 0);
        return $this->db->get('detailitemwh')->result();
    }

    public function getItemName($kditem)
    {
        $query = $this->db->select('itemname')
            ->from('itemmaster')
            ->where('kditem', $kditem)
            ->get()->row();
        return $query->itemname;
    }

    public function getSelectedItem($kditem)
    {
        return $this->db->get_where('itemmaster', ['kditem' => $kditem])->row();
    }

    public function getDetail($kditem, $warehouse, $slot)
    {
        $this->db->where('kditem', $kditem);
        $this->db->where('kdwarehouse', $warehouse);
        $this->db->where('slotid', $slot);
        return $this->db->get('detailitemwh');
    }

    public function getDetailWh($kditem)
    {
        $this->db->join('warehouse', 'warehouse.kdwarehouse = detailitemwh.kdwarehouse');
        $this->db->join('slot', 'slot.slotid = detailitemwh.slotid');
        return $this->db->get_where('detailitemwh', ['kditem' => $kditem])->result();
    }

    public function getById($kditem)
    {
        $this->db->join('category', 'category.catid = itemmaster.catid');
        $this->db->join('uom', 'uom.uom = itemmaster.uom');
        return $this->db->get_where($this->table, ['kditem' => $kditem])->row();
    }

    public function addProduct()
    {
        $hargamodal = explode(",", $this->input->post('hargamodal', true));
        $hargajual = explode(",", $this->input->post('hargajual', true));

        $data = [
            'kditem' => $this->input->post('kditem', true),
            'itemname' => $this->input->post('itemname', true),
            'brand' => $this->input->post('brand', true),
            // 'type' => $this->input->post('type', true),
            'createdate' => date('Y-m-d'),
            'status' => "AKTIF",
            'stocktype' => $this->input->post('stocktype', true),
            'hargamodal' => implode($hargamodal),
            'catid' => $this->input->post('catid', true),
            'hargajual' => implode($hargajual),
            'minqty' => $this->input->post('minqty', true),
            'uom' => $this->input->post('uom', true),
        ];

        $this->db->insert($this->table, $data);
    }

    public function editProduct()
    {
        $hargamodal = explode(",", $this->input->post('hargamodal', true));
        $hargajual = explode(",", $this->input->post('hargajual', true));

        $data = [
            'itemname' => $this->input->post('itemname', true),
            'brand' => $this->input->post('brand', true),
            // 'type' => $this->input->post('type', true),
            'status' => "AKTIF",
            'stocktype' => $this->input->post('stocktype', true),
            'hargamodal' => implode($hargamodal),
            'catid' => $this->input->post('catid', true),
            'hargajual' => implode($hargajual),
            'minqty' => $this->input->post('minqty', true),
            'uom' => $this->input->post('uom', true),
        ];

        $this->db->where('kditem', $this->input->post('kditem'));
        $this->db->update($this->table, $data);
    }

    public function deleteProduct($kditem)
    {
        $this->db->delete($this->table, ['kditem' => $kditem]);
    }
}

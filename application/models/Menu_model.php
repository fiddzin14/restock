<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    private $table = 'user_menu';

    public function getAll()
    {
        return $this->db->get($this->table)->result();
    }

    public function addMenu()
    {
        $data = [
            'menu' => $this->input->post('menu'),
        ];

        $this->db->insert($this->table, $data);
    }

    public function deleteMenu($id)
    {
        return $this->db->delete($this->table, ['menuid' => $id]);
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends CI_Model
{
    private $table = "sales_group";
    var $column_order = array(null, 'groupname', 'descwarehouse', 'isactive', null); //set column field database for datatable orderable
    var $column_search = array('groupname', 'descwarehouse', 'isactive'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('groupid' => 'asc'); // default order 

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        $this->db->join('warehouse', 'warehouse.kdwarehouse = sales_group.kdwarehouse');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($groupid)
    {
        $this->db->join('warehouse', 'warehouse.kdwarehouse = sales_group.kdwarehouse');
        return $this->db->get_where($this->table, ['groupid' => $groupid])->row();
    }

    public function getDetailId($groupid)
    {
        return $this->db->get_where('sales_group_detail', ['groupid' => $groupid])->result();
    }

    public function getDetailGroup($groupid)
    {
        $this->db->join('login_web', 'login_web.username = sales_group_detail.salesid');
        return $this->db->get_where('sales_group_detail', ['groupid' => $groupid])->result();
    }

    public function getUser()
    {
        return $this->db->get_where('login_web', ['loginlevel' => 2])->result();
    }

    public function addUser()
    {
        $data = [
            'groupid' => $this->input->post('groupid', true),
            'salesid' => $this->input->post('name', true),
            'createdt' => date('Y-m-d'),
            'createby' => $this->session->userdata('username', true),
        ];

        $this->db->insert('sales_group_detail', $data);
    }

    public function getId()
    {
        $this->db->select_max('groupid');
        $query = $this->db->get($this->table)->row();
        return $query->groupid + 1;
    }

    public function addGroup()
    {
        $user = $this->input->post('user');
        $id = $this->getId();

        $data = [
            'groupid' => $id,
            'groupname' => $this->input->post('groupname', true),
            'kdwarehouse' => $this->input->post('kdwarehouse', true),
            'isactive' => $this->input->post('isactive', true),
            'createby' => $this->session->userdata('username'),
            'createdt' => date('Y-m-d'),
            'updateby' => $this->session->userdata('username'),
            'updatedt' => date('Y-m-d'),
        ];

        $this->db->insert($this->table, $data);

        for ($i = 0; $i < count($user); $i++) {
            $this->db->insert('sales_group_detail', [
                'groupid' => $id,
                'salesid' => $user[$i],
                'createdt' => date('Y-m-d'),
                'createby' => $this->session->userdata('username'),
            ]);
        }
    }

    public function editGroup()
    {
        $user = $this->input->post('user');

        $data = [
            'groupname' => $this->input->post('groupname', true),
            'kdwarehouse' => $this->input->post('kdwarehouse', true),
            'isactive' => $this->input->post('isactive', true),
            'updateby' => $this->session->userdata('username'),
            'updatedt' => date('Y-m-d'),
        ];


        $this->db->where('groupid', $this->input->post('groupid'));
        $this->db->update($this->table, $data);

        $this->db->delete('sales_group_detail', ['groupid' => $this->input->post('groupid')]);

        for ($i = 0; $i < count($user); $i++) {
            $this->db->insert('sales_group_detail', [
                'groupid' => $this->input->post('groupid'),
                'salesid' => $user[$i],
                'createdt' => date('Y-m-d'),
                'createby' => $this->session->userdata('username'),
            ]);
        }
    }

    public function deleteGroup($groupid)
    {
        $this->db->delete($this->table, ['groupid' => $groupid]);
        $this->db->delete('sales_group_detail', ['groupid' => $groupid]);
    }
}

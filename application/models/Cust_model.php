<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cust_model extends CI_Model
{
    private $table = "customer";
    var $column_order = array(null, 'custid', 'custname', 'alamat', 'telp', 'isactive', 'createdt', null);
    var $column_search = array('custid', 'custname', 'telp', 'isactive', 'createdt', 'alamat');
    var $order = array('custid' => 'asc');

    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getById($custid)
    {
        return $this->db->get_where($this->table, ['custid' => $custid])->row();
    }

    public function getCustomer()
    {
        return $this->db->get_where($this->table, ['isactive' => 1])->result();
    }

    public function getCustID()
    {
        $this->db->select_max('custid');
        $query = $this->db->get($this->table)->row();
        return $query->custid;
    }

    public function addCustomer($salesid)
    {
        $cust = $this->getCustID();
        if ($cust == NULL) {
            $custID = "CST00001";
        } else {
            $ID = substr($cust, 3, 5);
            $No = $ID + 1;
            $custID = "CST" . sprintf("%05s", $No);
        }

        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'custid' => $custID,
            'custname' => $this->input->post('custname', true),
            'telp' => $this->input->post('telp', true),
            'isactive' => $isactive,
            'alamat' => $this->input->post('alamat', true),
            'createdt' => date('Y-m-d'),
            'createby' => $salesid,
        ];

        $this->db->insert($this->table, $data);
    }

    public function editCustomer()
    {
        if ($this->input->post('isactive')) {
            $isactive = 1;
        } else {
            $isactive = 0;
        }
        $data = [
            'custname' => $this->input->post('custname', true),
            'telp' => $this->input->post('telp', true),
            'isactive' => $isactive,
            'alamat' => $this->input->post('alamat', true),
        ];

        $this->db->where('custid', $this->input->post('custid'));
        $this->db->update($this->table, $data);
    }

    public function deleteCustomer($custid)
    {
        $this->db->delete($this->table, ['custid' => $custid]);
    }
}

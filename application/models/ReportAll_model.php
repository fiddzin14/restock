<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportAll_model extends CI_Model
{
    private $table = "view_laporanpenjualan_dt";
    var $column_order = array(null, 'TransID', 'TransDt', 'KdItem', 'ItemName', 'Qty', 'HargaJual', 'Harga_Modal', 'TotalSalesPrice', 'TotalHPP', 'Benefit');
    var $column_search = array('TransID', 'TransDt', 'KdItem', 'ItemName', 'Qty', 'HargaJual', 'Harga_Modal', 'TotalSalesPrice', 'TotalHPP', 'Benefit');
    var $order = array('TransID' => 'asc');

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        if ($this->input->post('kdwarehouse')) {
            $this->db->where('kdwarehouse', $this->input->post('kdwarehouse'));
        }
        if ($this->input->post('catname')) {
            $this->db->where('catname', $this->input->post('catname'));
        }
        if ($this->input->post('KdItem')) {
            $this->db->where('KdItem', $this->input->post('KdItem'));
        }
        // if ($this->input->post('ItemName')) {
        //     $this->db->where('ItemName', $this->input->post('ItemName'));
        // }

        if ($this->input->post('TransDt')) {
            $date = explode('-', $this->input->post('TransDt'));
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('TransDt BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getAll()
    {
        $catname = $this->input->post('category');
        $item = $this->input->post('kditem');
        $dateR = $this->input->post('TransDt');
        if ($this->input->post('kdwh')) {
            $this->db->where('kdwarehouse', $this->input->post('kdwh'));
        }
        if ($catname != '') {
            $this->db->where('catname', $catname);
        }
        if ($item != '') {
            $this->db->where('KdItem', $item);
        }
        if ($dateR != '') {
            $date = explode('-', $dateR);
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('TransDt BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }
        return $this->db->get($this->table)->result();
    }

    public function getDate()
    {
        $this->db->select_min('TransDt');
        $query = $this->db->get($this->table)->row();
        if ($query->TransDt == NULL) {
            return time();
        } else {
            $date = strtotime($query->TransDt);
            return $date;
        }
    }

    function sumCount($catname = '', $item = '', $dateR = '', $kdwarehouse = '')
    {
        $this->db->select_sum('Qty', 'qty');
        $this->db->select_sum('Benefit', 'benefit');
        $this->db->select_sum('TotalHPP', 'hpp');
        $this->db->select_sum('TotalSalesPrice', 'total');
        if ($kdwarehouse != '') {
            $this->db->where('kdwarehouse', $kdwarehouse);
        }
        if ($catname != '') {
            $this->db->where('catname', $catname);
        }
        if ($item != '') {
            $this->db->where('KdItem', $item);
        }
        if ($dateR != '') {
            $date = explode('-', $dateR);
            $SDate = date('Y-m-d 00:00:00', strtotime($date[0]));
            $EDate = date('Y-m-d 23:59:59', strtotime($date[1]));
            $this->db->where('TransDt BETWEEN "' . $SDate . '" and "' . $EDate . '"');
        }
        $this->db->from($this->table);
        return $this->db->get()->row();
    }
}

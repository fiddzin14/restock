<!-- Login form -->
<form class="login-form form-validate" method="POST" action="">
    <!-- <?php echo $this->session->flashdata('message') ?> -->
    <div class="card mb-0">
        <div class="card-body">
            <div class="text-center mb-3">
                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="mb-0">Login to your account</h5>
                <span class="d-block text-muted">Enter your credentials below</span>
            </div>

            <div class="mb-3 error-mess">

            </div>
            <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" id="btn-login" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
            </div>

            <div class="text-center">
                <a href="<?= base_url('auth/forgotPassword'); ?>">Forgot password?</a>
            </div>
        </div>
    </div>
</form>
<!-- Login form -->
<form class="login-form form-validate" method="POST" action="<?php echo base_url('auth'); ?>">
    <!-- <?php echo $this->session->flashdata('message') ?> -->
    <div class="card mb-0">
        <div class="card-body">
            <div class="text-center mb-3">
                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="mb-0">Login to your account</h5>
                <span class="d-block text-muted">Enter your credentials below</span>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="password" name="password" class="form-control" placeholder="Password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left">
                <input type="password" name="confirmPassword" class="form-control" placeholder="Confirm Password" required>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
            </div>

            <div class="text-center">
                <a href="<?= base_url('auth/forgotPassword'); ?>">Forgot password?</a>
            </div>
        </div>
    </div>
</form>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Report Penjualan | Forgot password</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/justdo/vendors/iconfonts/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/justdo/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/justdo/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/justdo/css/horizontal-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logo.png" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="main-panel">
                <div class="content-wrapper d-flex align-items-center auth px-0">
                    <div class="row w-100 mx-0">
                        <div class="col-lg-4 mx-auto">
                            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                                <h4>Change your password</h4>
                                <?php echo $this->session->flashdata('message') ?>
                                <form class="pt-3" action="<?php echo base_url('auth/changePassword') ?>" method="POST">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control form-control-lg <?php echo form_error('password') == true ? 'is-invalid' : '' ?>" id="" placeholder="Password">
                                        <?php echo form_error('password', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirmPassword" class="form-control form-control-lg <?php echo form_error('confirmPassword') == true ? 'is-invalid' : '' ?>" id="" placeholder="Confirm Password">
                                        <?php echo form_error('confirmPassword', '<small class="text-danger pl-3">', '</small>') ?>
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="">SUBMIT</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url(); ?>assets/justdo/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?php echo base_url(); ?>assets/justdo/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?php echo base_url(); ?>assets/justdo/js/off-canvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/justdo/js/hoverable-collapse.js"></script>
    <script src="<?php echo base_url(); ?>assets/justdo/js/template.js"></script>
    <script src="<?php echo base_url(); ?>assets/justdo/js/settings.js"></script>
    <script src="<?php echo base_url(); ?>assets/justdo/js/todolist.js"></script>
    <!-- endinject -->
</body>

</html>
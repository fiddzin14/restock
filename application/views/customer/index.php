<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Master Customer</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Customer">
            <i class="fa fa-plus mr-1">
            </i>Add Customer
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Master Customer" id="form" method="POST" action="<?php echo base_url('customer/add'); ?>">
                        <input type="hidden" name="custid" id="custid">
                        <div class="form-group row">
                            <label for="custname" class="col-sm-2 col-form-label">*Customer Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="custname" name="custname" class="form-control" placeholder="Customer Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telp" class="col-sm-2 col-form-label">*Contact</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="telp" name="telp" class="form-control" placeholder="Contact">
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="alamat" class="col-sm-2 col-form-label">*Address</label>
                            <div class="col-sm-7 col-inp">
                                <textarea id="alamat" name="alamat" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isactive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 col-inp">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isactive" name="isactive" class="form-check-input" value="1" checked>
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Customer ID</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Contact</th>
                        <th>Status</th>
                        <th>Date Create</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
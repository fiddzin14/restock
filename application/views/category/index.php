<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Category Item</h5>
        <a href="#" class="btn btn-primary collapse-show btn-sm act add float-right" title="Add Category">
            <i class="fa fa-plus mr-1">
            </i>Add Category
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Category Item" id="form" method="POST" action="<?php echo base_url('category/add'); ?>">
                        <div class="form-group row">
                            <label for="catid" class="col-sm-2 col-form-label">*Category ID</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="catid" name="catid" class="form-control" placeholder="Category ID">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="catname" class="col-sm-2 col-form-label">*Category Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="catname" name="catname" class="form-control" placeholder="Category Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isActive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isActive" name="isActive" class="form-check-input" value="1" checked>
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th>Category ID</th>
                        <th>Category Name</th>
                        <th>Status</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

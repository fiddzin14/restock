<script>
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('slotRack/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('Slot Rack');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Slot');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#slotid').removeAttr('readonly', 'readonly');
            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Slot');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                var option = $('#warehouse option:nth-child(1)').text();
                $('#card-title').text('Add Slot');
                $('#slotid').val("");
                $('#slotnm').val("");

                $('#warehouse option').removeAttr('selected', 'selected');
                $('#warehouse option:nth-child(1)').attr('selected', 'selected');
                $('#select2-warehouse-container').text(option).attr('title', option);

                $('#isactive').attr('checked', 'checked');
                $('#form').attr('action', '<?php echo $action; ?>');
            }

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#slotid').val(data.slotid);
                    $('#slotid').attr('readonly', 'readonly');
                    $('#slotnm').val(data.slotnm);
                    if (data.isactive == 1) {
                        $('#isactive').attr('checked', 'checked');
                    } else {
                        $('#isactive').removeAttr('checked', 'checked');
                    }

                    $('#warehouse option[value="' + data.kdwarehouse + '"]').attr('selected', 'selected');
                    $('#select2-warehouse-container').text(data.kdwarehouse).attr('title', data.kdwarehouse);
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });
    });
</script>

</body>

</html>
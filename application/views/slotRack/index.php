<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Slot Rack</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Slot">
            <i class="fa fa-plus mr-1">
            </i>Add Slot
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Slot Rack" id="form" method="POST" action="<?php echo base_url('slotRack/add'); ?>">
                        <input type="hidden" name="slotid" id="idw">
                        <div class="form-group row">
                            <label for="slotid" class="col-sm-2 col-form-label">*Slot ID</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="slotid" name="slotid" class="form-control" placeholder="Slot ID">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="slotnm" class="col-sm-2 col-form-label">*Slot Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="slotnm" name="slotnm" class="form-control" placeholder="Slot Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse</label>
                            <div class="col-sm-7 col-inp">
                                <select id="warehouse" name="warehouse" class="form-control select-search" data-fouc>
                                    <?php foreach ($warehouse as $row) : ?>
                                        <option value="<?= $row->kdwarehouse; ?>"><?= $row->kdwarehouse; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isactive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 col-inp">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isactive" name="isactive" class="form-check-input" value="1" checked>
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Slot ID</th>
                        <th>Slot Name</th>
                        <th>Status</th>
                        <th>In Warehouse?</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
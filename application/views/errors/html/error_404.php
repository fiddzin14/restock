<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>404 Not Found</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/vendor/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/limitless/css/colors.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logo.png" />
	<!-- Core JS files -->
	<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url(); ?>assets/limitless/js/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">

				<!-- Content area -->
				<div class="content d-flex justify-content-center align-items-center mb-5" style="margin-top:8px ;">

					<!-- Container -->
					<div class="flex-fill">

						<!-- Error title -->
						<div class="text-center mb-4">
							<img src="https://demo.interface.club/limitless/demo/Template/global_assets/images/error_bg.svg" class="img-fluid mb-3" alt="" height="230">
							<h1 class="display-2 font-weight-semibold line-height-1 mb-2">404</h1>
							<h6 class="w-md-25 mx-md-auto">Oops, an error has occurred. <br> The resource requested could not be found on this server.</h6>
						</div>
						<!-- /error title -->


						<!-- Error content -->
						<div class="text-center">
							<a href="#" onclick="window.history.back();" class="btn btn-primary"><i class="icon-home4 mr-2"></i> Back to Previous Page</a>
						</div>
						<!-- /error wrapper -->

					</div>
					<!-- /container -->

				</div>
				<!-- /content area -->


				<!-- Footer -->
				<!-- <div class="navbar navbar-expand-lg navbar-light">
					<div class="text-center d-lg-none w-100">
						<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
							<i class="icon-unfold mr-2"></i>
							Footer
						</button>
					</div>

					<div class="navbar-collapse collapse" id="navbar-footer">
						<span class="navbar-text">
							Copyright © 2021. <a href="#">Jasa Solution</a>. All rights reserved.
						</span>
					</div>
				</div> -->
				<!-- /footer -->

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>

</body>

</html>
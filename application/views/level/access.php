<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Manage Access for <?= $row->level; ?></h5>
    </div>
    <div class="card-body">
        <form class="forms-sample" id="form" method="POST" action="<?php echo base_url('level/update'); ?>">
            <input type="hidden" name="level" value="<?= $row->id; ?>">
            <?php foreach ($menu as $mn) : ?>
                <div class="form-group row">
                    <label for="warehouse" class="col-sm-2 col-form-label">*Menu / Sub Menu</label>
                    <div class="col-sm-4 col-inp">
                        <input type="text" name="menu" class="form-control" id="menu" value="<?= $mn->menu; ?>" readonly>
                    </div>
                    <div class="col-sm-4 col-inp">
                        <select id="sub" name="sub[]" data-placeholder="No Access" class="form-control multiselect-select-all" multiple="multiple" data-fouc>
                            <?php foreach ($sub as $sb) : ?>
                                <?php if ($mn->menuid == $sb->menu_id) : ?>
                                    <option value="<?= $sb->subid; ?>" <?= in_array($sb->subid, $access) ? 'Selected' : ''; ?>><?= $sb->title; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="form-group row mt-3">
                <label for="" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-7">
                    <button type="submit" class="btn btn-success btn-sm mr-2" id="btsave">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
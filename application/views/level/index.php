<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">User Level</h5>
        <a href="<?php echo base_url('level/add'); ?>" class="btn btn-primary btn-sm float-right btn-plus" title="Add Level">
            <i class="fa fa-plus mr-1">
            </i>Add Level
        </a>
    </div>
    <div class="card-body">
        <table id="datatables" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th width="50">#</th>
                    <th>Level</th>
                    <th width="400">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($level as $row) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $row->level; ?></td>
                        <td>
                            <?php if ($row->level != "ADMIN") : ?>
                                <a href="level/access?id=<?= strtr(base64_encode($row->id), '+/=', '._-'); ?>" class="btn btn-sm btn-outline-primary" title="Manage Access"><i class="fa fa-lock"></i> Access</a>&nbsp;
                                <!-- <a href="level/edit?id=<?= strtr(base64_encode($row->id), '+/=', '._-'); ?>" class="btn btn-sm btn-outline-primary" title="Edit Level"><i class="fa fa-pencil"></i> Edit</a>&nbsp; -->
                                <a href="level/delete?id=<?= strtr(base64_encode($row->id), '+/=', '._-'); ?>" class="btn btn-sm btn-outline-danger btn-del" title="Delete"><i class="fa fa-trash"></i> Delete</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
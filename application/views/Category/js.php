<script>
    var table;
    $(document).ready(function() {

        //datatables
        $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            "ajax": {
                "url": "<?php echo base_url('category/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('Category Item');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Category');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#catid').removeAttr('readonly', 'readonly');
            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Category');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                $('#card-title').text('Add Category');
                $('#catid').val("");
                $('#catname').val("");
                $('#isActive').attr('checked', 'checked');
                $('#form').attr('action', '<?php echo $action; ?>');
            }
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#catid').val(data.catid);
                    $('#catid').attr('readonly', 'readonly');
                    $('#catname').val(data.catname);
                    if (data.isactive == 1) {
                        $('#isActive').attr('checked', 'checked');
                    } else {
                        $('#isActive').removeAttr('checked', 'checked');
                    }
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);
            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });

    });
</script>

</body>

</html>
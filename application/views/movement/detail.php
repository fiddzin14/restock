<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Detail Transaction</h5>
        <a href="print?id=<?= strtr(base64_encode($row->invusenum), '+/=', '._-'); ?>" target="_blank" class="btn print btn-danger btn-sm float-right" title="Add Item to Warehouse">
            <i class="fas fa-print mr-1">
            </i> Print Transaction
        </a>
    </div>
    <div class="card-body">
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">No Transaction <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->invusenum; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Status <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->status; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Date <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= date('d M Y', strtotime($row->usedate)); ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">From Location <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->fromwarehouse; ?> / <?= $row->fromslotid; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Action By <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->issuedby; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">To Location <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->towarehouse; ?> / <?= $row->toslotid; ?>
            </div>
        </div>
        <table id="table" class="table mt-5 datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Code</th>
                    <th>Brand/Type</th>
                    <th>Item Name</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->brand; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= $it->qty; ?></td>
                        <?php $qty[] = $it->qty; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <th colspan="4">Total Quantity :</th>
                <th><?= array_sum($qty); ?></th>
            </tfoot>
        </table>
    </div>
</div>

<?php $this->load->view('_partials/footer'); ?>
</body>

</html>
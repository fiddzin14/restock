<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Add Transaction</h5>
        <a href="<?= base_url('movement'); ?>" id="btback" class="btn btn-warning btn-sm float-right" title="">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a>
        <a href="#" id="btsave" title="Draft" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-save mr-1">
            </i>Save
        </a>
        <a href="#" id="btsave" title="Complete" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-check-circle mr-1">
            </i>Compelete Input
        </a>
    </div>
    <div class="card-body">
        <form class="ml-3 mb-5" id="form" method="POST" action="<?php echo base_url('movement/create'); ?>">
            <div class="form-group row">
                <label for="fromWarehouse" class="col-sm-3 col-form-label">*From Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="fromWarehouse" data-placeholder="Select Warehouse" name="fromWarehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="fromSlot" data-placeholder="Select slot" name="fromSlot" class="form-control select-search" data-fouc>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="toWarehouse" class="col-sm-3 col-form-label">*Transfer To Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="toWarehouse" data-placeholder="Select Warehouse" name="toWarehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="toSlot" data-placeholder="Select slot" name="toSlot" class="form-control select-search" data-fouc>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-3 col-form-label">*Usage Type</label>
                <div class="col-sm-6">
                    <input type="text" id="type" class="form-control" name="type" value="Transfer" readonly>
                </div>
            </div>
            <input type="hidden" name="status" id="stats">
            <div class="form-group row">
                <label for="description" class="col-sm-3 col-form-label">Description</label>
                <div class="col-sm-6">
                    <textarea name="description" id="description" rows="3" class="form-control">-</textarea>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label for="sItem" class="col-sm-3 col-form-label">Item</label>
                <div class="col-sm-6 col-inp">
                    <div class="input-group">
                        <input type="text" id="sItem" class="form-control border-right-0" placeholder="Search Item">
                        <span class="input-group-append">
                            <button class="btn btn-primary" id="addItem">Insert</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="memo" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-6">
                    <button type="submit" title="Draft" class="btn btn-primary mr-1" id="btsave">
                        <i class="fa fa-save mr-1">
                        </i>Save
                    </button>
                    <button type="submit" title="Complete" class="btn btn-primary mr-2" id="btsave">
                        <i class="fa fa-check-circle mr-1">
                        </i>Compelete Input
                    </button>
                </div>
            </div> -->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="300">Item</th>
                            <th>Brand/Type</th>
                            <th>Qty</th>
                            <th>units</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<script>
    var count = 0;
</script>
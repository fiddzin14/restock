<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Edit Transaction</h5>
        <a href="<?= base_url('movement'); ?>" id="btback" class="btn btn-warning btn-sm float-right" title="">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a>
        <a href="#" id="btsave" title="Draft" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-save mr-1">
            </i>Save
        </a>
        <a href="#" id="btsave" title="Complete" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-check-circle mr-1">
            </i>Compelete Input
        </a>
    </div>
    <div class="card-body">
        <form class="ml-3 mb-5" id="form" method="POST" action="<?php echo base_url('movement/update'); ?>">
            <input type="hidden" name="invusenum" value="<?= $row->invusenum; ?>">
            <div class="form-group row">
                <label for="fromWarehouse" class="col-sm-3 col-form-label">*From Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="fromWarehouse" data-placeholder="Select Warehouse" name="fromWarehouse" class="form-control select-search" data-fouc>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>" <?= $dt->kdwarehouse == $row->fromwarehouse ? 'selected' : ''; ?>><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="fromSlot" data-placeholder="Select slot" name="fromSlot" class="form-control select-search" data-fouc>
                        <?php foreach ($slot as $st) : ?>
                            <option value="<?= $st->slotid; ?>"><?= $st->slotnm; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="toWarehouse" class="col-sm-3 col-form-label">*Transfer To Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="toWarehouse" data-placeholder="Select Warehouse" name="toWarehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>" <?= $dt->kdwarehouse == $row->towarehouse ? 'selected' : ''; ?>><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="toSlot" data-placeholder="Select slot" name="toSlot" class="form-control select-search" data-fouc>
                        <?php foreach ($slot_to as $st) : ?>
                            <option value="<?= $st->slotid; ?>"><?= $st->slotnm; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-3 col-form-label">*Usage Type</label>
                <div class="col-sm-6">
                    <input type="text" id="type" class="form-control" name="type" value="Transfer" readonly>
                </div>
            </div>
            <input type="hidden" name="status" id="stats">
            <div class="form-group row">
                <label for="description" class="col-sm-3 col-form-label">Description</label>
                <div class="col-sm-6">
                    <textarea name="description" id="description" rows="3" class="form-control"><?= $row->description; ?></textarea>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label for="sItem" class="col-sm-3 col-form-label">Item</label>
                <div class="col-sm-6 col-inp">
                    <div class="input-group">
                        <input type="text" id="sItem" class="form-control border-right-0" placeholder="Search Item">
                        <span class="input-group-append">
                            <button class="btn btn-primary" id="addItem">Insert</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="memo" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-6">
                    <button type="submit" title="Draft" class="btn btn-primary mr-1" id="btsave">
                        <i class="fa fa-save mr-1">
                        </i>Save
                    </button>
                    <button type="submit" title="Complete" class="btn btn-primary mr-2" id="btsave">
                        <i class="fa fa-check-circle mr-1">
                        </i>Compelete Input
                    </button>
                </div>
            </div> -->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="300">Item</th>
                            <th>Brand/Type</th>
                            <th>Qty</th>
                            <th>units</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detail as $tl) : ?>
                            <?php $no++ ?>
                            <tr id="row<?= $no; ?>">
                                <td><?= $tl->kditem . ' ' . $tl->itemname; ?></td>
                                <td><?= $tl->brand; ?></td>
                                <td><input type="number" id="qty<?= $no; ?>" name="qty[]" min="1" style="width:80px;" value="<?= $tl->qty; ?>" class="form-control qty"></td>
                                <td><?= $tl->uom; ?></td>
                                <td>
                                    <button class="btn btn-danger btn-sm btn-remove" id="<?= $no; ?>"><i class="fas fa-trash"></i></button>
                                </td>
                                <input type="hidden" class="item" name="item[]" value="<?= $tl->kditem; ?>">
                                <?php foreach ($item as $id) : ?>
                                    <?php if ($tl->kditem == $id->kditem) : ?>
                                        <input type="hidden" name="cost[]" value="<?= $id->hargamodal; ?>">
                                        <input type="hidden" name="uom[]" value="<?= $id->uom; ?>">
                                        <input type="hidden" name="hargajual[]" value="<?= $id->hargajual; ?>">
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <?php foreach ($detailFrom as $df) : ?>
                                    <?php if ($tl->kditem == $df->kditem) : ?>
                                        <input type="hidden" name="curbal[]" value=<?= $df->curbal; ?>>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <?php if ($detail_to != '') : ?>
                                    <?php foreach ($detail_to as $dto) : ?>
                                        <?php if ($tl->kditem == $dto->kditem) : ?>
                                            <input type="hidden" name="curbal_to[]" value=<?= $dto->curbal; ?>>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<script>
    var count = <?php echo $num; ?>;
</script>
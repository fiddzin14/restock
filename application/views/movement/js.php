<script>
    var table;
    var previous;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('movement/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
                "data": function(data) {
                    data.fromwarehouse = $('#kdwh').val();
                    data.usedate = $('#usedate').val();
                }
            },

            "columnDefs": [{
                "targets": [0, -1],
                "orderable": false,
            }, ],
        });

        $('#loading').css('transform', 'translate(450px,580px)')

        $('#btn-filter').click(function() {
            table.ajax.reload();
        });

        $('#fromWarehouse').on('change', function() {
            var warehouse = $(this).val();
            var item = $('.item').val();
            $('#fromWarehouse').on('select2:selecting', function(evt) {
                previous = $('#fromWarehouse').val();
            });
            if (item != null) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "you want to change warehouse? your curent input will be reset.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Reset!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        $('#fromSlot').val('');
                        $('#toWarehouse').val('');
                        $('#toSlot option').remove();
                        $('.table tbody tr').remove();
                        $.ajax({
                            url: "<?php echo base_url('movement/getSlotByWarehouse'); ?>",
                            method: "POST",
                            data: {
                                warehouse: warehouse
                            },
                            async: true,
                            dataType: 'json',
                            success: function(data) {
                                var slot = '';
                                var warehouse = '';
                                var i;
                                for (i = 0; i < data.slot.length; i++) {
                                    slot += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                                        '<option value="' + data.slot[i].slotid + '">' + data.slot[i].slotnm + '</option>';
                                }
                                for (i = 0; i < data.warehouse.length; i++) {
                                    warehouse += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                                        '<option value="' + data.warehouse[i].kdwarehouse + '">' + data.warehouse[i].descwarehouse + '</option>';
                                }
                                $('#fromSlot').html(slot);
                                $('#toWarehouse').html(warehouse);
                            }
                        });
                    } else {
                        $('#fromWarehouse').val(previous);
                        var text = $('#fromWarehouse option[value="' + previous + '"]').text();
                        $('#select2-fromWarehouse-container').text(text).attr('title', text);
                    }
                });
            } else {
                $.ajax({
                    url: "<?php echo base_url('movement/getSlotByWarehouse'); ?>",
                    method: "POST",
                    data: {
                        warehouse: warehouse
                    },
                    async: true,
                    dataType: 'json',
                    success: function(data) {
                        var slot = '';
                        var warehouse = '';
                        var i;
                        for (i = 0; i < data.slot.length; i++) {
                            slot += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                                '<option value="' + data.slot[i].slotid + '">' + data.slot[i].slotnm + '</option>';
                        }
                        for (i = 0; i < data.warehouse.length; i++) {
                            warehouse += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                                '<option value="' + data.warehouse[i].kdwarehouse + '">' + data.warehouse[i].descwarehouse + '</option>';
                        }
                        $('#fromSlot').html(slot);
                        $('#toWarehouse').html(warehouse);
                    }
                });
            }
        });

        $('#toWarehouse').on('change', function() {
            var warehouse = $(this).val();

            $.ajax({
                url: "<?php echo base_url('movement/getSlotByWarehouse'); ?>",
                method: "POST",
                data: {
                    warehouse: warehouse
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var slot = '';
                    var i;
                    for (i = 0; i < data.slot.length; i++) {
                        slot += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                            '<option value="' + data.slot[i].slotid + '">' + data.slot[i].slotnm + '</option>';
                    }
                    $('#toSlot').html(slot);
                }
            });
            return false;
        });

        $('#fromSlot').change(function() {
            var warehouse = $('#fromWarehouse').val();
            var slot = $(this).val();
            $.ajax({
                url: "<?php echo base_url('movement/getItemBySlot'); ?>",
                method: "POST",
                data: {
                    warehouse: warehouse,
                    slot: slot
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    var item = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        item += '<option value="" class="text-secondary">Select Item</option> ' +
                            '<option value="' + data[i].kditem + '">' + data[i].kditem + ' ' + data[i].brand + ' ' + data[i].itemname + '</option>';
                    }
                    $('.item').html(item);
                }
            });
            return false;
        });

        // $('#btplus').click(function() {
        //     var warehouse = $('#fromWarehouse').val();
        //     var slot = $('#fromSlot').val();
        //     $.ajax({
        //         url: "<?php echo base_url('movement/getItemBySlot'); ?>",
        //         method: "POST",
        //         data: {
        //             warehouse: warehouse,
        //             slot: slot
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var item = '';
        //             var i;
        //             count += 1;
        //             for (i = 0; i < data.length; i++) {
        //                 item += '<option value="" class="text-secondary">Select Item</option> ' +
        //                     '<option value="' + data[i].kditem + '">' + data[i].kditem + ' ' + data[i].brand + ' ' + data[i].itemname + '</option>';
        //             }
        //             html += '<tr id="row' + count + '">' +
        //                 '<td class="col-inp">' +
        //                 '<select id="item' + count + '" data-placeholder="Select Item" name="item[]" class="form-control item select-search" data-fouc>' +
        //                 item +
        //                 '</select>' +
        //                 '</td>' +
        //                 '<td></td>' +
        //                 '<td></td>' +
        //                 '<td></td>' +
        //                 '<td><button class="btn btn-danger btn-sm btn-remove" id="' + count + '"><i class="fas fa-trash"></i></button></td>' +
        //                 '</tr>';
        //             $('#btplus').closest('tbody').append(html);
        //             $('.select-search').select2();
        //         }
        //     });
        //     return false;
        // });

        $('#sItem').autocomplete({
            // source: "",
            source: function(request, response) {
                $.ajax({
                    url: "<?php echo base_url('movement/getAutocomplete'); ?>",
                    method: "POST",
                    dataType: "json",
                    data: {
                        term: request.term,
                        item: $('.item').serializeArray(),
                        warehouse: $('#fromWarehouse').val(),
                        slot: $('#fromSlot').val(),
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            select: function(event, ui) {
                $(this).val(ui.item.label);
                $("#addItem").click();
            }
        });

        $('#addItem').click(function(event) {
            event.preventDefault();

            var Sitem = $('#sItem').val();
            var itemSplit = Sitem.split(" ");
            var item = itemSplit[0];
            var itemSel = $('.item').serializeArray();
            if (itemSel == null) {
                itemSel == '';
            } else {
                itemSel == $('.item').serializeArray();
            }

            var fromWarehouse = $('#fromWarehouse').val();
            var fromSlot = $('#fromSlot').val();
            var toWarehouse = $('#toWarehouse').val();
            var toSlot = $('#toSlot').val();
            var id = $(this).attr('id');

            $('#fromWarehouse').closest('.col-inp').find('.form-text').remove();
            $('#toWarehouse').closest('.col-inp').find('.form-text').remove();
            $('#fromSlot').closest('.col-inp').find('.form-text').remove();
            $('#toSlot').closest('.col-inp').find('.form-text').remove();
            $('#sItem').closest('.col-inp').find('.form-text').remove();

            if (fromWarehouse == '') {
                $('#fromWarehouse').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Warehouse first.</strong></span>')
            } else if (fromSlot == '') {
                $('#fromSlot').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Slot first.</strong></span>')
            } else if (fromSlot == '') {
                $('#toWarehouse').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Warehouse first.</strong></span>')
            } else if (fromSlot == '') {
                $('#toSlot').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Slot first.</strong></span>')
            } else if (item == '') {
                $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Item first.</strong></span>')
            } else {
                $.ajax({
                    url: "<?php echo base_url('movement/getSelectedItem'); ?>",
                    method: "POST",
                    data: {
                        item: item,
                        itemSelect: itemSel,
                        fromWarehouse: fromWarehouse,
                        toWarehouse: toWarehouse,
                        fromSlot: fromSlot,
                        toSlot: toSlot,
                    },
                    async: true,
                    dataType: 'json',
                    success: function(data) {
                        // $('#sItem option[value="' + data.row.kditem + '"]').remove();
                        if (data.error == true) {
                            $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Item has been selected.</strong></span>');
                            $("#sItem").blur().focus();
                        } else if (data.item == null) {
                            $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Item not found.</strong></span>');
                            $("#sItem").blur().focus();
                        } else {
                            $('#sItem').val('');
                            var html = '';
                            var detail = '';
                            var i;
                            count += 1;
                            if (data.detail_to != null) {
                                detail += '<input type="hidden" name="curbal_to[]" value="' + data.detail_to.curbal + '">'
                            } else {
                                detail += '<input type="hidden" name="curbal_to[]" value="0">'
                            }
                            html += '<tr id="row' + count + '">' +
                                '<td>' + data.item.kditem + ' ' + data.item.itemname + '</td>' +
                                '<td>' + data.item.brand + '</td>' +
                                '<td><input type="number" oninput="this.value = this.value.replace(/[^0-9.,]/g);" id="qty' + count + '" name="qty[]" min="1" style="width:80px;" size="10" value="0" class="form-control qty"></td>' +
                                '<td>' + data.detail_from.issueunit + '</td>' +
                                '<td><button class="btn btn-danger btn-sm btn-remove" id="' + count + '"><i class="fas fa-trash"></i></button></td>' +
                                '<input type="hidden" name="item[]" class="item" value="' + data.item.kditem + '">' +
                                '<input type="hidden" name="uom[]" value="' + data.item.uom + '">' +
                                '<input type="hidden" name="cost[]" value="' + data.detail_from.cost + '">' +
                                '<input type="hidden" name="hargajual[]" value="' + data.item.hargajual + '">' +
                                '<input type="hidden" name="curbal[]" value="' + data.detail_from.curbal + '">' +
                                detail +
                                '</tr>';
                            $('tbody').append(html);
                        }
                    }
                });
            }
            return false;
        });

        $('body').on('click', '.btn-remove', function() {
            var me = $(this).closest('tr');
            me.remove();
        });

        // $('body').on('change', '.item', function() {
        //     var me = $(this);
        //     var item = me.val();
        //     var fromWarehouse = $('#fromWarehouse').val();
        //     var toWarehouse = $('#toWarehouse').val();
        //     var fromSlot = $('#fromSlot').val();
        //     var toSlot = $('#toSlot').val();
        //     var id = me.attr('id');
        //     $("#" + id).closest('tr').find('input').remove();

        //     $.ajax({
        //         url: "<?php echo base_url('movement/getSelectedItem'); ?>",
        //         method: "POST",
        //         data: {
        //             item: item,
        //             fromWarehouse: fromWarehouse,
        //             toWarehouse: toWarehouse,
        //             fromSlot: fromSlot,
        //             toSlot: toSlot,
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             count += 1;
        //             $("#" + id).closest('tr').children('td:nth-child(2)').text(data.item.brand);
        //             $("#" + id).closest('tr').children('td:nth-child(3)').html('<input type="number" id="qty' + count + '" name="qty[]" min="1" style="width:80px;" value="0" class="form-control qty">');
        //             $("#" + id).closest('tr').children('td:nth-child(4)').text(data.detail_from.issueunit);
        //             $("#" + id).closest('tr').append('<input type="hidden" name="uom[]" value="' + data.detail_from.issueunit + '">');
        //             $("#" + id).closest('tr').append('<input type="hidden" name="cost[]" value="' + data.detail_from.cost + '">');
        //             $("#" + id).closest('tr').append('<input type="hidden" name="hargajual[]" value="' + data.detail_from.hargajual + '">');
        //             $("#" + id).closest('tr').append('<input type="hidden" name="curbal[]" value="' + data.detail_from.curbal + '">');
        //             if (data.detail_to != null) {
        //                 $("#" + id).closest('tr').append('<input type="hidden" name="curbal_to[]" value="' + data.detail_to.curbal + '">');
        //             } else {
        //                 $("#" + id).closest('tr').append('<input type="hidden" name="curbal_to[]" value=0>');
        //             }
        //         }
        //     });
        //     return false;
        // });

        $('body').on('change', '.qty', function() {
            var me = $(this);
            var qty = me.val();
            var item = me.closest('tr').find('.item').val();
            var warehouse = $('#fromWarehouse').val();
            var slot = $('#fromSlot').val();

            var id = me.attr('id');

            $.ajax({
                url: "<?php echo base_url('movement/checkQty'); ?>",
                method: "POST",
                data: {
                    item: item,
                    warehouse: warehouse,
                    slot: slot,
                    qty: qty
                },
                async: true,
                dataType: 'json',
                success: function(response) {
                    if (response.status == false) {
                        $("#" + id).val(0);
                        Swal.fire({
                            type: 'error',
                            title: 'Warning...!',
                            confirmButtonClass: 'btn btn-primary',
                            text: 'There is no Stock for this Item. Curent item Qty ' + response.data.curbal + ' ' + response.data.issueunit,
                        });
                    }
                }
            });
            return false;
        });

        $('body').on('click', '#btcomplete', function(event) {
            event.preventDefault();
            var me = $(this),
                url = me.attr('href');

            Swal.fire({
                title: 'Are you sure?',
                text: "you want to complete this transaction!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Complete!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        success: function(response) {
                            table.ajax.reload();
                            Swal.fire({
                                title: 'success!',
                                text: 'Data has been deleted!',
                                type: 'success',
                                confirmButtonClass: 'btn btn-primary',
                                timer: 2000
                            });
                        },
                        error: function(xhr) {
                            Swal.fire({
                                type: 'error',
                                title: 'Error...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Something went wrong!',
                            });
                        }
                    });
                }
            });
        });

        $('body').on('click', '#btsave', function(event) {
            event.preventDefault();

            var form = $('#form'),
                url = form.attr('action'),
                method = 'POST';

            var status = $(this).attr('title');

            if (status == "Complete") {
                $('#stats').val('IN PROGRESS');
            } else {
                $('#stats').val('DRAFT');
            }

            form.find('.form-text').remove();
            form.find('.form-group').removeClass('has-error');
            form.find('.form-control').removeClass('is-invalid');

            $.ajax({
                url: url,
                method: method,
                data: form.serialize(),
                success: function(response) {
                    if (response.status == false) {
                        if (response.error_item == true) {
                            Swal.fire({
                                type: 'error',
                                title: 'Warning...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Item field is required.',
                            });
                        } else if (response.error_qty == true) {
                            Swal.fire({
                                type: 'error',
                                title: 'Error...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Something went wrong! Please check your input.',
                            });
                        }
                        $.each(response.errors, function(key, value) {
                            $('.form-group').addClass('has-error');
                            $('#' + key)
                                .closest('.form-control')
                                .addClass('is-invalid')
                                .closest('.col-inp')
                                .addClass('has-error')
                                .append('<span class="form-text text-danger"><strong>' + value + '</strong></span>')
                        });
                    } else {
                        Swal.fire({
                            type: 'success',
                            title: 'success!',
                            text: 'Data has been saved!',
                            confirmButtonClass: 'btn btn-primary',
                            timer: 2000,
                        });

                        setTimeout(function() {
                            window.location.href = '<?php echo base_url('movement') ?>';
                        }, 2000);
                    }
                },
                error: function(xhr) {
                    Swal.fire({
                        type: 'error',
                        title: 'Error...!',
                        confirmButtonClass: 'btn btn-primary',
                        text: 'Something went wrong! Please check your input.',
                    });
                }
            });
        });
    });
</script>

</body>

</html>
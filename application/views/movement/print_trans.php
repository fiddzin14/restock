<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <title>Print Transfer</title>

    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }

        header .header {
            font-size: 18px;
            font-weight: bold;
            text-transform: uppercase;
            margin-bottom: 40px;
            text-align: center;
        }

        .sub {
            position: relative;
        }

        .tabl {
            float: left;
            margin-right: 120px;
        }

        .tabl td {
            font-size: 14px;
            padding-bottom: 10px;
            padding-right: 10px;
        }

        main {
            position: relative;
            top: 150px;
        }

        .table {
            width: 700px;
            border-collapse: collapse;
        }

        .table td {
            font-size: 11px;
        }

        .table th {
            font-size: 13px;
            border-bottom: 0.5px solid black;
            border-top: 0.5px solid black;
        }

        .table,
        .table td,
        .table th {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>

<body>
    <header>
        <div class="header">Transfer</div>
    </header>

    <div class="sub">
        <table class="tabl" style="margin-left: 20px;">
            <tr>
                <td>No Transaction</td>
                <td>:</td>
                <td><?= $row->invusenum; ?></td>
            </tr>
            <tr>
                <td>Date</td>
                <td>:</td>
                <td><?= date('d M Y', strtotime($row->usedate)); ?></td>
            </tr>
            <tr>
                <td>Action By</td>
                <td>:</td>
                <td><?= $row->salesnm; ?></td>
            </tr>
        </table>

        <table class="tabl" style="transform: translateY(-3px);">
            <tr>
                <td>Status</td>
                <td>:</td>
                <td><?= $row->status; ?></td>
            </tr>
            <tr>
                <td>From Location</td>
                <td>:</td>
                <td><?= $row->fromwarehouse; ?> / <?= $row->fromslotid; ?></td>
            </tr>
            <tr>
                <td>To Location</td>
                <td>:</td>
                <td><?= $row->towarehouse; ?> / <?= $row->toslotid; ?></td>
            </tr>
        </table>
    </div>

    <main>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Item Code</th>
                    <th>Brand/Type</th>
                    <th>Spesifikasi</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->brand; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= $it->qty; ?></td>
                    </tr>
                    <?php $qty[] = $it->qty; ?>
                <?php endforeach; ?>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="3">Total Quantity :</th>
                    <th><?= array_sum($qty); ?></th>
                </tr>
            </tfoot>
        </table>
    </main>
</body>

</html>
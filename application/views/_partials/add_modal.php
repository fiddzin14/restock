<div id="addModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="forms-sample" id="form" method="POST" action="<?php echo base_url('product/toWarehouse'); ?>">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="modal-title">Add Item to Warehouse</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal-body">
                    <div class="form-group row">
                        <label for="kditem" class="col-sm-2 col-form-label">*Item</label>
                        <div class="col-sm-4 col-inp">
                            <input type="text" id="kditem" name="kditem" class="form-control" placeholder="Item Code" value="<?= $row->kditem; ?>" readonly>
                        </div>
                        <div class="col-sm-4 col-inp">
                            <input type="text" id="itemname" name="itemname" class="form-control" placeholder="Item Name" value="<?= $row->itemname; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="warehouse" class="col-sm-2 col-form-label">*To Warehouse / Slot</label>
                        <div class="col-sm-4 col-inp">
                            <select id="warehouse" data-placeholder="Select Warehouse" name="warehouse" class="form-control select-custom" data-fouc>
                                <option value="" class="text-secondary">Select Warehouse</option>
                                <?php foreach ($warehouse as $dt) : ?>
                                    <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-4 col-inp">
                            <select id="slot" data-placeholder="Select slot" name="slot" class="form-control select-custom" data-fouc>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="qty" class="col-sm-2 col-form-label">*Quantity / Unit</label>
                        <div class="col-sm-4 col-inp">
                            <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="1">
                        </div>
                        <div class="col-sm-4 col-inp">
                            <select id="uom" name="uom" class="form-control select-custom" data-placeholder="Select a state" data-fouc>
                                <?php foreach ($uom as $dt) : ?>
                                    <option value="<?= $dt->uom; ?>"><?= $dt->uom; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hargamodal" class="col-sm-2 col-form-label">*Price / Cost</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-primary border-primary text-white">Rp</span>
                                </span>
                                <input type="text" id="hargamodal" name="hargamodal" value="<?= nominal($row->hargamodal); ?>" class="form-control rp_modal" placeholder="Harga Modal">
                                <span class="input-group-append">
                                    <span class="input-group-text bg-primary border-primary text-white">.00</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hargajual" class="col-sm-2 col-form-label">*Sell Price</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-primary border-primary text-white">Rp</span>
                                </span>
                                <input type="text" id="hargajual" name="hargajual" value="<?= nominal($row->hargajual); ?>" class="form-control rp_modal" placeholder="Harga Jual">
                                <span class="input-group-append">
                                    <span class="input-group-text bg-primary border-primary text-white">.00</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-7 card-title">
                            (*)Mandatory
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
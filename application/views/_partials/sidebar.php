<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->

        <!-- Sidebar content -->
        <div class="sidebar-content">
            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/default.jpg" width="38" height="38" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold"><?= $login->displayname; ?></div>
                            <div class="font-size-xs opacity-50">
                                <?= $login->level; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    <?php
                    $level = $this->session->userdata('level');
                    $menu = $this->db->get('user_menu')->result();

                    ?>
                    <?php foreach ($menu as $m) :
                        $access = $this->db->get_where('user_access_menu', [
                            'menu_id' => $m->menuid,
                            'loginlevel' => $login->loginlevel,
                        ])->row();
                        if ($level == "ADMIN") : ?>
                            <li class="nav-item-header">
                                <div class="text-uppercase font-size-xs line-height-xs"><?= $m->menu; ?></div> <i class="icon-menu" title="Page kits"></i>
                            </li>
                        <?php else : ?>
                            <?php if ($access) : ?>
                                <li class="nav-item-header">
                                    <div class="text-uppercase font-size-xs line-height-xs"><?= $m->menu; ?></div> <i class="icon-menu" title="Page kits"></i>
                                </li>
                            <?php endif; ?>
                        <?php endif;
                        $this->db->join('user_menu', 'user_menu.menuid = user_sub_menu.menu_id');
                        if ($level != "ADMIN") {
                            $this->db->join('user_access_menu', 'user_access_menu.sub_menu_id = user_sub_menu.subid');
                            $this->db->where('loginlevel', $login->loginlevel);
                        }
                        $this->db->where('user_sub_menu.menu_id', $m->menuid);
                        $this->db->where('isactive', 1);
                        $sub = $this->db->get('user_sub_menu')->result();

                        ?>
                        <?php foreach ($sub as $s) : ?>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $this->uri->segment(1) == $s->url ? 'active' : ''; ?>" href="<?php echo base_url($s->url); ?>">
                                    <i class="<?= $s->icon; ?> menu-icon"></i>
                                    <span><?= $s->title; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">
        <?php $this->load->view('_partials/breadcrumb'); ?>
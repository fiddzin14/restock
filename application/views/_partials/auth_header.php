<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/vendor/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/limitless/css/colors.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logo.png" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/notifications/sweet_alert.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/limitless/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/login_validation.js"></script>
    <!-- /theme JS files -->

</head>

<body>
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
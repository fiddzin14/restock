<!-- /login form -->

</div>
<!-- /content area -->


<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
        <span class="navbar-text">
            Copyright © 2021. <a href="#">Jasa Solution</a>. All rights reserved.
        </span>
    </div>
</div>
<!-- /footer -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->
<script>
    $(document).ready(function() {
        $('body').on('click', '#btn-login', function(event) {
            event.preventDefault();

            var username = $('#username').val();
            var password = $('#password').val();

            $('form').find('.error-mess').empty();

            if (username == "" && password == "") {
                $('form').submit();
            } else {
                $.ajax({
                    url: "<?php echo base_url('auth/login'); ?>",
                    method: "POST",
                    data: {
                        username: username,
                        password: password,
                    },
                    async: true,
                    dataType: 'json',
                    success: function(response) {
                        if (response.status == false) {
                            $('.error-mess').html('<label id="" class="validation-invalid-label">' + response.errors + '</label>');
                        } else {
                            window.location.href = '<?php echo base_url('dashboard') ?>';
                        }
                    }
                });
            }
        });
    });
</script>

</body>

</html>
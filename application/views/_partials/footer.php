</div>
<!-- /content area -->
<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
        <span class="navbar-text">
            Copyright © 2021. <a href="#">Jasa Solution</a>. All rights reserved.
        </span>
    </div>
</div>
<!-- /footer -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<!-- Core JS files -->
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/main/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/autocomplate/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/selects/select2.min.js"></script>
<!-- Theme JS files -->
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/styling/uniform.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/styling/switchery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/selects/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/visualization/d3/d3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/ui/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/anytime.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/pickadate/picker.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/pickadate/picker.date.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/pickadate/picker.time.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/pickers/pickadate/legacy.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/notifications/jgrowl.min.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/forms/selects/bootstrap_multiselect.js"></script>

<!-- <script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/autonumeric/autoNumeric.js"></script> -->
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/plugins/tables/footable/footable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<!-- /theme JS files -->
<script src="<?php echo base_url(); ?>assets/limitless/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/form_select2.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/datatables_basic.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/picker_date.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/dashboard.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/table_responsive.js"></script>
<script src="<?php echo base_url(); ?>assets/limitless/vendor/js/demo_pages/form_multiselect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
<script>
    $(document).ready(function() {
        $('.rp_modal').mask('000,000,000,000,000', {
            reverse: true,
            dropdownParent: $("#addModal .modal-body"),
        });
        $('.rp').mask('000,000,000,000,000', {
            reverse: true
        });

        $(".select-custom").select2({
            dropdownParent: $("#addModal .modal-body"),
            placeholder: 'Select a State',
        });
        $('#order-listing').DataTable();
    });
</script>
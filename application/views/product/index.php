<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Master Product</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Warehouse">
            <i class="fa fa-plus mr-1">
            </i>Add Product
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Add Product" id="form" method="POST" action="<?php echo base_url('product/add'); ?>">
                        <div class="form-group row">
                            <label for="kditem" class="col-sm-2 col-form-label">*Item Code</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="kditem" name="kditem" class="form-control" placeholder="Item Code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="itemname" class="col-sm-2 col-form-label">*Item Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="itemname" name="itemname" class="form-control" placeholder="Item Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="brand" class="col-sm-2 col-form-label">*Brand</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="brand" name="brand" class="form-control" placeholder="Brand">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="catid" class="col-sm-2 col-form-label">*Category</label>
                            <div class="col-sm-7 col-inp">
                                <select id="catid" name="catid" class="form-control select-search" data-fouc>
                                    <?php foreach ($category as $row) : ?>
                                        <option value="<?= $row->catid; ?>"><?= $row->catname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="minqty" class="col-sm-2 col-form-label">*Min Qty</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="minqty" name="minqty" class="form-control" value="0" placeholder="Min Qty">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stocktype" class="col-sm-2 col-form-label">*Item Type</label>
                            <div class="col-sm-7 col-inp">
                                <select id="stocktype" name="stocktype" class="form-control select-search" data-fouc>
                                    <option value="Stock">Stock</option>
                                    <option value="Spareparts">Spareparts</option>
                                    <option value="Service">Service</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hargamodal" class="col-sm-2 col-form-label">*Price / Cost</label>
                            <div class="col-sm-7 col-inp">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text bg-primary border-primary text-white">Rp</span>
                                    </span>
                                    <input type="text" id="hargamodal" name="hargamodal" value="0" class="form-control rp" placeholder="Harga Modal">
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">.00</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hargajual" class="col-sm-2 col-form-label">*Sell Price</label>
                            <div class="col-sm-7 col-inp">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text bg-primary border-primary text-white">Rp</span>
                                    </span>
                                    <input type="text" id="hargajual" name="hargajual" value="0" class="form-control rp" placeholder="Harga Jual">
                                    <span class="input-group-append">
                                        <span class="input-group-text bg-primary border-primary text-white">.00</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="uom" class="col-sm-2 col-form-label">*Unit</label>
                            <div class="col-sm-7 col-inp">
                                <select id="uom" name="uom" class="form-control select-search" data-fouc>
                                    <?php foreach ($uom as $row) : ?>
                                        <option value="<?= $row->uom; ?>"><?= $row->uom; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>Brand</th>
                        <th>Category</th>
                        <th>stock Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
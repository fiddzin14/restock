<script>
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('product/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('Master Product');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Product');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            $('.rp').mask('000,000,000,000,000', {
                reverse: true
            });

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#kditem').removeAttr('readonly', 'readonly');
            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Product');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                $('#card-title').text('Add Product');
                $('#kditem').val("");
                $('#itemname').val("");
                $('#brand').val("");
                $('#minqty').val(0);
                $('#hargamodal').val(0);
                $('#hargajual').val(0);

                var option = $('#catid option:nth-child(1)').text();
                var option2 = $('#uom option:nth-child(1)').text();
                var option3 = $('#stocktype option:nth-child(1)').text();
                $('select option').removeAttr('selected', 'selected');

                $('#catid option:nth-child(1)').attr('selected', 'selected');
                $('#select2-catid-container').text(option).attr('title', option);

                $('#uom option:nth-child(1)').attr('selected', 'selected');
                $('#select2-uom-container').text(option2).attr('title', option2);

                $('#stocktype option:nth-child(1)').attr('selected', 'selected');
                $('#select2-stocktype-container').text(option3).attr('title', option2);

                $('#form').attr('action', '<?php echo $action; ?>');
            }

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#kditem').attr('readonly', 'readonly');
                    $('#kditem').val(data.row.kditem);
                    $('#itemname').val(data.row.itemname);
                    $('#brand').val(data.row.brand);
                    $('#minqty').val(data.row.minqty);
                    $('#hargamodal').val(data.hargaM);
                    $('#hargajual').val(data.hargaJ);

                    $('#catid option[value="' + data.row.catid + '"]').attr('selected', 'selected');
                    $('#select2-catid-container').text(data.row.catname).attr('title', data.row.catname);

                    $('#uom option[value="' + data.row.uom + '"]').attr('selected', 'selected');
                    $('#select2-uom-container').text(data.row.uom).attr('title', data.row.uom);

                    $('#stocktype option[value="' + data.row.stocktype + '"]').attr('selected', 'selected');
                    $('#select2-stocktype-container').text(data.row.stocktype).attr('title', data.row.stocktype);
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });
    });
</script>

</body>

</html>
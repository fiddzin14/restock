<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Detail Product</h5>
        <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-primary btn-sm float-right" title="Add Item to Warehouse">
            <i class="fas fa-sync-alt mr-1">
            </i> Add to Warehouse
        </a>
    </div>
    <div class="card-body">
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Item Code <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->kditem; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Price / Cost <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= rupiah($row->hargamodal); ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Item Name <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->itemname; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Sell Price <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= rupiah($row->hargajual); ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Category <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->catname; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Stock Type <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->stocktype; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Brand <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->brand; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Unit Of Measure <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->uom; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Min Qty <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->minqty; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Created at <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= date('d M Y', strtotime($row->createdate)); ?>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h5 class="card-title d-inline-block" id="card-title">Item in Warehouse</h5>
    </div>
    <div class="card-body">
        <table id="table" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>Warehouse Name</th>
                    <th>Slot Name</th>
                    <th>Last In</th>
                    <th>Last Out</th>
                    <th>Curent Ballance</th>
                    <th>Uom</th>
                    <th>Qty Reserved</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('_partials/footer'); ?>
<?php $this->load->view('_partials/add_modal'); ?>

<script>
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('product/item_list?id=' . strtr(base64_encode($row->kditem), '+/=', '._-')) ?>",
                "type": "POST"
            },
        });
    });

    $('#warehouse').change(function() {
        var id = $(this).val();
        $.ajax({
            url: "<?php echo base_url('product/sendSlot'); ?>",
            method: "POST",
            data: {
                id: id
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].slotid + '">' + data[i].slotnm + '</option>';
                }
                $('#slot').html(html);
            }
        });
        return false;
    });
</script>
</body>

</html>
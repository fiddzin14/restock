<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Unit Of Measure</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Uom">
            <i class="fa fa-plus mr-1">
            </i>Add Uom
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Unit Of Measure" id="form" method="POST" action="<?php echo base_url('unitOfMeasure/add'); ?>">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group row">
                            <label for="uom" class="col-sm-2 col-form-label">*Unit Of Measure</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="uom" name="uom" class="form-control" placeholder="Unit Of Measure">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descr" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="descr" name="descr" class="form-control" placeholder="Description">
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Unit Of Measure</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('group/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('User Group');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Group');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Group');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                var option = $('#warehouse option:nth-child(1)').text();
                $('#card-title').text('Add Group');
                $('#groupname').val("");

                $('#warehouse option').removeAttr('selected', 'selected');
                $('#warehouse option:nth-child(1)').attr('selected', 'selected');
                $('#select2-warehouse-container').text(option).attr('title', option);

                $('#isactive').attr('checked', 'checked');
                $('#form').attr('action', '<?php echo $action; ?>');
            }

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#idw').val(data.row.groupid);
                    $('#user').val(data.row.username);
                    $('#groupname').val(data.row.groupname);
                    if (data.row.isactive == 1) {
                        $('#isactive').attr('checked', 'checked');
                    } else {
                        $('#isactive').removeAttr('checked', 'checked');
                    }

                    $('#warehouse option[value="' + data.row.kdwarehouse + '"]').attr('selected', 'selected');
                    $('#select2-warehouse-container').text(data.row.descwarehouse).attr('title', data.row.descwarehouse);

                    var user = '';
                    var i;
                    var detail = [];
                    detail = data.detail.salesid;

                    for (i = 0; i < data.user.length; i++) {
                        if (jQuery.inArray(data.user[i].username, detail) != -1) {
                            user += '<option value="' + data.user[i].username + '" selected>' + data.user[i].displayname + '</option>';
                        } else {
                            user += '<option value="' + data.user[i].username + '">' + data.user[i].displayname + '</option>';
                        }
                    }
                    $('#user').html(user);
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });

        // $('#warehouse').change(function(e) {
        //     var warehouse = $(this).val();

        //     $.ajax({
        //         url: "<?php echo base_url('Group/getUserByWarehouse'); ?>",
        //         method: "POST",
        //         data: {
        //             warehouse: warehouse
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var slot = '';
        //             var i;
        //             for (i = 0; i < data.length; i++) {
        //                 slot += '<option value="" class="text-secondary" id="">Select Slot</option>' +
        //                     '<option value="' + data[i].slotid + '">' + data[i].slotnm + '</option>';
        //             }
        //             $('#slot').html(slot);
        //         }
        //     });
        //     return false;
        // })

        // $('body').on('click', '#btDetail', function(event) {
        //     event.preventDefault();

        //     var me = $(this),
        //         url = me.attr('href');

        //     $.ajax({
        //         url: url,
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var i;
        //             for (i = 0; i < data.length; i++) {
        //                 html += '<tr">' +
        //                     '<td>' + data.displayname + '</td>' +
        //                     '</tr>';
        //             }

        //             $('#modal-body tbody').html(html);
        //         }
        //     });
        //     $('#modal').modal('show');
        // });
    });
</script>

</body>

</html>
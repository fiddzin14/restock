<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">User Group</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Group">
            <i class="fa fa-plus mr-1">
            </i>Add Group
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-2">
                <div class="col-12">
                    <form class="forms-sample" title="User Group" id="form" method="POST" action="<?php echo base_url('group/add'); ?>">
                        <input type="hidden" name="groupid" id="idw">
                        <div class="form-group row">
                            <label for="groupname" class="col-sm-2 col-form-label">*Group Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="groupname" name="groupname" class="form-control" placeholder="Group Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse</label>
                            <div class="col-sm-7 col-inp">
                                <select id="warehouse" name="kdwarehouse" class="form-control select-search" data-fouc>
                                    <?php foreach ($warehouse as $row) : ?>
                                        <option value="<?= $row->kdwarehouse; ?>"><?= $row->descwarehouse; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user" class="col-sm-2 col-form-label">*User</label>
                            <div class="col-sm-7 col-inp" id="selectUser">
                                <select id="user" name="user[]" data-placeholder="Select User" class="form-control multiselect" multiple="multiple" data-fouc>
                                    <?php foreach ($user as $row) : ?>
                                        <option value="<?= $row->username; ?>"><?= $row->displayname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isactive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isactive" name="isactive" class="form-check-input" value="1" checked>
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Group Name</th>
                        <th>Warehouse</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
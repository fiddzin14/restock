<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="forms-sample" id="" method="POST" action="<?php echo base_url('group/add_user') ?>">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="modal-title">Add User</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal-body">
                    <input type="hidden" name="groupid" value="<?= $row->groupid; ?>">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">*Name</label>
                        <div class="col-sm-8">
                            <select name="name" id="name" class="form-control select-search" data-fouc>
                                <?php foreach ($user as $row) : ?>
                                    <option value="<?= $row->username; ?>"><?= $row->displayname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-add" type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
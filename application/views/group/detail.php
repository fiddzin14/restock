<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">User in Group</h5>
        <!-- <a href="#" data-toggle="modal" data-target="#modal" class="btn btn-primary btn-sm float-right" title="Add User to Group">
            <i class="fas fa-sync-alt mr-1">
            </i> Add User
        </a> -->
    </div>
    <div class="card-body">
        <table id="datatables" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th width=200>#</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($group as $data) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $data->displayname; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
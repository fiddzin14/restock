<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Sub Menu</h5>
        <a href="#" class="btn btn-primary collapse-show btn-sm act add float-right" title="Add Sub Menu">
            <i class="fa fa-plus mr-1">
            </i>Add Sub Menu
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Sub Menu" id="form" method="POST" action="<?php echo base_url('subMenu/add'); ?>">
                        <input type="hidden" name="id" id="subid">
                        <div class="form-group row">
                            <label for="menu" class="col-sm-2 col-form-label">*Menu</label>
                            <div class="col-sm-7 col-inp">
                                <select id="menu" name="menu" class="form-control select-search" data-fouc>
                                    <?php foreach ($menu as $row) : ?>
                                        <option value="<?= $row->menuid; ?>"><?= $row->menu; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-sm-2 col-form-label">*Title</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="title" name="title" class="form-control" placeholder="Title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="url" class="col-sm-2 col-form-label">*Url</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="url" name="url" class="form-control" placeholder="Url">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="icon" class="col-sm-2 col-form-label">*Icon</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="icon" name="icon" class="form-control" placeholder="Icon">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isactive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isactive" name="isactive" class="form-check-input" value="1">
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th>Menu</th>
                        <th>Title</th>
                        <th>Url</th>
                        <th>Icon</th>
                        <th>Status</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('subMenu/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('Sub Menu');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Sub Menu');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Sub Menu');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                var option = $('#menu option:nth-child(1)').text();
                $('#card-title').text('Add Sub Menu');
                $('#title').val("");
                $('#url').val("");
                $('#icon').val("");

                $('#menu option').removeAttr('selected', 'selected');
                $('#menu option:nth-child(1)').attr('selected', 'selected');
                $('#select2-menu-container').text(option).attr('title', option);

                $('#isactive').attr('checked', 'checked');
                $('#form').attr('action', '<?php echo $action; ?>');
            }

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#subid').val(data.subid);
                    $('#title').val(data.title);
                    $('#url').val(data.url);
                    $('#icon').val(data.icon);
                    if (data.isactive == 1) {
                        $('#isactive').attr('checked', 'checked');
                    } else {
                        $('#isactive').removeAttr('checked', 'checked');
                    }

                    $('#menu option[value="' + data.menuid + '"]').attr('selected', 'selected');
                    $('#select2-menu-container').text(data.menu).attr('title', data.menu);
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });
    });
</script>

</body>

</html>
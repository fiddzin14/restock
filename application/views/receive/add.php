<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Add Transaction</h5>
        <a href="<?= base_url('receive'); ?>" id="btback" class="btn btn-warning btn-sm float-right" title="">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a>
        <a href="#" id="btsave" title="Draft" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-save mr-1">
            </i>Save
        </a>
        <a href="#" id="btsave" title="Complete" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-check-circle mr-1">
            </i>Compelete Input
        </a>
    </div>
    <div class="card-body">
        <form class="ml-3 mb-5" id="form" method="POST" action="<?php echo base_url('receive/create'); ?>">
            <div class="form-group row">
                <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="warehouse" data-placeholder="Select Warehouse" name="warehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="slot" data-placeholder="Select slot" name="slot" class="form-control select-search" data-fouc>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="supplier" class="col-sm-2 col-form-label">*Supplier</label>
                <div class="col-sm-6">
                    <select id="supplier" data-placeholder="Select supplier" name="supplier" class="form-control select-search" data-fouc>
                        <?php foreach ($supplier as $row) : ?>
                            <option value="<?= $row->kdsupplier; ?>"><?= $row->descsupplier; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-2 col-form-label">Type Receive</label>
                <div class="col-sm-6">
                    <select name="type" id="type" class="form-control select-search" data-fouc>
                        <option value="normal">Normal</option>
                        <option value="retur">Retur</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-6">
                    <textarea name="description" id="description" rows="3" class="form-control">-</textarea>
                </div>
            </div>
            <input type="hidden" name="status" id="stats" value="DRAFT">
            <div class="form-group row mt-4">
                <label for="sItem" class="col-sm-2 col-form-label">Item</label>
                <div class="col-sm-6 col-inp">
                    <div class="input-group">
                        <input type="text" id="sItem" class="form-control border-right-0" placeholder="Search Item">
                        <span class="input-group-append">
                            <button class="btn btn-primary" id="addItem">Insert</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group row mt-4">
                <label for="sItem" class="col-sm-2 col-form-label">Item</label>
                <div class="col-sm-6 col-inp">
                    <select id="sItem" data-placeholder="Select Item" class="form-control select-search" required data-fouc>
                        <option value="" class="text-secondary">Select Item</option>
                        <?php foreach ($item as $it) : ?>
                            <option value="<?= $it->kditem; ?>"><?= $it->kditem; ?> <?= $it->itemname; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-2">
                    <button class="btn-sm btn btn-primary" id="addItem">Add to Table</button>
                </div>
            </div> -->
            <!-- <div class="form-group row">
                <label for="memo" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-6">
                    <button type="submit" title="Draft" class="btn btn-primary mr-1" id="btsave">
                        <i class="fa fa-save mr-1">
                        </i>Save
                    </button>
                    <button type="submit" title="Complete" class="btn btn-primary mr-2" id="btsave">
                        <i class="fa fa-check-circle mr-1">
                        </i>Compelete Input
                    </button>
                </div>
            </div> -->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 350px;">Item</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<!-- <div style="position: fixed;bottom: 0;left:-5px;width: 100%;box-shadow: -1px -1px 4px #888888;" class="navbar navbar-expand-lg navbar-light">
    <div class="" id="">
        <span class="navbar-text">
            <a href="#" id="btsave" class="btn btn-primary text-white">
                <i class="fa fa-save mr-1">
                </i>Submit
            </a>
        </span>
    </div>
</div> -->
<script>
    var count = 0;
</script>
<div id="receiveModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="forms-sample" id="form-receive" method="POST" action="<?php echo base_url('receive/receiveItem') ?>">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="modal-title">Receive Item</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal-body">
                    <input type="hidden" name="reqnum" id="reqnum">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-receive" type="submit" class="btn btn-success">Receive</button>
                </div>
            </form>
        </div>
    </div>
</div>
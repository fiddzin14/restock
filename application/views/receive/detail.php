<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Detail Receive Transaction</h5>
        <a href="print_receive?id=<?= strtr(base64_encode($row->reqnum), '+/=', '._-'); ?>" target="_blank" class="btn print btn-danger btn-sm float-right" title="Add Item to Warehouse">
            <i class="fas fa-print mr-1">
            </i> Print
        </a>
    </div>
    <div class="card-body">
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Receiving Number <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->reqnum; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Status <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->stat; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Date <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= date('d M Y', strtotime($row->reqdate)); ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Notes <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->descr; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Supplier <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->descsupplier; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Type <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->typereceive; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Location <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $item[0]->kdwarehouse; ?> / <?= $item[0]->slotid; ?>
            </div>
        </div>
        <table id="table" class="table mt-5 datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Code</th>
                    <th>Item Name</th>
                    <th>Qty Received</th>
                    <th>Qty Out Standing</th>
                    <th>All Qty</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= $it->qtyreceived; ?></td>
                        <td><?= $it->qtyoutstanding; ?></td>
                        <td><?= $it->qty; ?></td>
                        <td><?= rupiah($it->price); ?></td>
                        <td><?= rupiah($it->total); ?></td>
                    </tr>
                    <?php $item[] = $it->total; ?>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <th colspan="7">Sub Total :</th>
                <th><?= rupiah(array_sum($item)); ?></th>
            </tfoot>
        </table>
    </div>
</div>

<?php $this->load->view('_partials/footer'); ?>
<!-- <script>
    $('.print').click(function(e) {
        e.preventDefault();
        window.print();
    })
</script> -->
</body>

</html>
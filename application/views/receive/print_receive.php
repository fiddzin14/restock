<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <title>Print Receive</title>

    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }

        header .header {
            font-size: 17px;
            font-weight: bold;
            text-transform: uppercase;
            margin-bottom: 40px;
            text-align: center;
        }

        .sub {
            position: relative;
        }

        .tabl {
            float: left;
            margin-right: 70px;
        }

        .tabl td {
            font-size: 14px;
            padding-bottom: 10px;
            padding-right: 10px;
        }

        main {
            position: relative;
            top: 150px;
        }

        .table {
            width: 700px;
            border-collapse: collapse;
        }

        .table td {
            font-size: 11px;
        }

        .table th {
            font-size: 13px;
            border-bottom: 0.5px solid black;
            border-top: 0.5px solid black;
        }

        .table,
        .table td,
        .table th {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>

<body>
    <header style="text-align: center;">
        <div class="header">Purchase Receiving</div>
    </header>

    <div class="sub">
        <table class="tabl" style="margin-left: 10px;">
            <tr>
                <td style="width: 80px;">No. Receive</td>
                <td>:</td>
                <td><?= $row->reqnum; ?></td>
            </tr>
            <tr>
                <td>Date</td>
                <td>:</td>
                <td><?= date('d M Y', strtotime($row->reqdate)); ?></td>
            </tr>
            <tr>
                <td>Supplier</td>
                <td>:</td>
                <td><?= $row->descsupplier ?></td>
            </tr>
            <tr>
                <td>Type</td>
                <td>:</td>
                <td><?= $row->typereceive ?></td>
            </tr>
        </table>

        <table class="tabl" style="width:350px">
            <tr>
                <td>Status</td>
                <td style="width: 5px;">:</td>
                <td><?= $row->stat; ?></td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 60px;">Notes</td>
                <td style="width: 5px;height: 46px;vertical-align: top;">:</td>
                <td style="vertical-align: top;"><?= $row->descr; ?></td>
            </tr>
            <tr>
                <td style="vertical-align: top;">Warehouse</td>
                <td style="vertical-align: top;">:</td>
                <td style="vertical-align: top;"><?= $item[0]->kdwarehouse; ?> / <?= $item[0]->slotid; ?></td>
            </tr>
        </table>
    </div>

    <main>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Item Code</th>
                    <th>Brand/Type</th>
                    <th>Spesifikasi</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->brand; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= rupiah($it->price); ?></td>
                        <td><?= $it->qty; ?></td>
                        <td><?= rupiah($it->price * $it->qty); ?></td>
                    </tr>
                    <?php $item[] = $it->price * $it->qty; ?>
                <?php endforeach; ?>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5">Sub Total :</th>
                    <th><?= rupiah(array_sum($item)); ?></th>
                </tr>
            </tfoot>
        </table>
    </main>
</body>

</html>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Filter Report</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <form id="form-filter" action="" method="POST">
                    <div class="form-group row">
                        <label for="kdwarehouse" class="col-md-3 col-form-label">Warehouse</b></label>
                        <div class="col-md-8">
                            <select name="kdwarehouse" id="kdwarehouse" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($warehouse as $wh) : ?>
                                    <?php if ($kdwarehouse == $wh->kdwarehouse) : ?>
                                        <option value="<?php echo $wh->kdwarehouse; ?>" selected><?php echo $wh->kdwarehouse; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $wh->kdwarehouse; ?>"><?php echo $wh->kdwarehouse; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="transdt" class="col-md-3 col-form-label">Date</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <input type="text" name="transdt" id="transdt" class="form-control daterange-basic" value="<?php echo date('Y/m/d', $date); ?> - <?php echo date('Y/m/d'); ?>">
                                <span class="input-group-append">
                                    <span class="input-group-text"><i class="icon-calendar22"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="" class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" name="filter" class="btn btn-primary btn-sm"><i class="fas fa-filter mr-1"></i> FIlter</button>
                            <input type="submit" name="export" class="btn btn-success btn-sm" value="Export">
                            <!-- <button type="submit" name="export" id="bt-export" class="btn btn-success btn-sm"><i class='fas fa-download mr-1'></i> Export</button> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6" style="font-size: 15px;">
                <div class="row ml-3" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Tota lHPP <span class="float-right">:</span></div>
                    <div class="col-md-7"><?php echo rupiah($count->hpp); ?></div>
                </div>
                <div class="row ml-3" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total Penjualan <span class="float-right">:</span></div>
                    <div class="col-md-7"><?php echo rupiah($count->total); ?></div>
                </div>
                <div class="row ml-3" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total Profit <span class="float-right">:</span></div>
                    <div class="col-md-7"><?php echo rupiah($count->total - $count->hpp); ?></div>
                </div>
                <div class="row ml-3" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">QTY <span class="float-right">:</span></div>
                    <div class="col-md-7"><?php echo $count->qty; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Report Penjualan</h5>
    </div>
    <div class="card-body">
        <table id="datatables" class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Transaction ID</th>
                    <th>Date</th>
                    <th>Warehouse</th>
                    <th>QTY</th>
                    <th>Customer</th>
                    <th>Sales Name</th>
                    <th>TotalHPP</th>
                    <th>Total</th>
                    <th>Profit</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $dt) : ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $dt->TransID; ?></td>
                        <td><?php echo date('d M y', strtotime($dt->TransDt)); ?></td>
                        <td><?php echo $dt->kdwh; ?></td>
                        <td><?php echo $dt->NumOfItem; ?></td>
                        <td><?php echo $dt->CustName; ?></td>
                        <td><?php echo $dt->SalesNm; ?></td>
                        <td><?php echo rupiah($dt->TotalHPP); ?></td>
                        <td><?php echo rupiah($dt->Total); ?></td>
                        <td><?php echo rupiah($dt->Total - $dt->TotalHPP); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php $this->load->view('_partials/footer'); ?>
<script>
    $(document).ready(function() {
        $('body').on('click', '#bt-export', function(event) {
            event.preventDefault();

            window.location.href = '<?php echo base_url('Laporan_penjualan/export'); ?>';
            $('#form-filter').submit();
        })
    })
</script>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title">Filter Report</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-7">
                <form action="" method="POST">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Warehouse / Category</label>
                        <div class="col-sm-5">
                            <select name="kdwh" id="kdwh" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($warehouse as $wh) : ?>
                                    <option value="<?php echo $wh->kdwarehouse; ?>"><?php echo $wh->descwarehouse; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select name="category" id="category" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($category as $ct) : ?>
                                    <option value="<?php echo $ct->catname; ?>"><?php echo $ct->catname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Item Code / Item Name</label>
                        <div class="col-sm-5">
                            <select name="kditem" id="kditem" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($item as $it) : ?>
                                    <option value="<?php echo $it->kditem; ?>"><?php echo $it->kditem; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select name="itemname" id="itemname" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($item as $in) : ?>
                                    <option value="<?php echo $in->kditem; ?>"><?php echo $in->itemname; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TransDt" class="col-sm-2 col-form-label">Range</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" name="TransDt" id="date" class="form-control daterange-basic" value="<?php echo date('Y/m/d', $date); ?>- <?php echo date('Y/m/d'); ?>">
                                <span class="input-group-append">
                                    <span class="input-group-text"><i class="icon-calendar22"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kdwh" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-5">
                            <button type="button" name="filter" id="btn-filter" class="btn btn-primary btn-sm"><i class="fas fa-filter mr-1"></i> FIlter</button>
                            <input type="submit" name="export" class="btn btn-success btn-sm" value="Export">
                            <!-- <a href="<?= base_url('ReportPenjualanAll/export'); ?>" id="btexport" class="btn btn-success btn-sm"><i class='fas fa-download mr-1'></i> Export</a> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-5 pl-5 pt-2" style="font-size: 15px;">
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total HPP <span class="float-right">:</span></div>
                    <div class="col-md-5" id="hpp"><?= rupiah($count->hpp); ?></div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total Penjualan <span class="float-right">:</span></div>
                    <div class="col-md-5" id="total"><?= rupiah($count->total); ?></div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total Profit <span class="float-right">:</span></div>
                    <div class="col-md-5" id="profit"><?= rupiah($count->benefit); ?></div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">QTY <span class="float-right">:</span></div>
                    <div class="col-md-5" id="qty"><?= nominal($count->qty); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Report Penjualan</h5>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table" class="table">
                <thead>
                    <tr>
                        <th>Transaction ID</th>
                        <th>Date</th>
                        <th>Item Code</th>
                        <th style="width: 150px;">Item Name</th>
                        <th>Qty</th>
                        <th>Sell Price</th>
                        <th>Cost</th>
                        <th>Total</th>
                        <th>Total HPP</th>
                        <th>Benefit</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
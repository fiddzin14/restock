<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Filter Report</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <form action="" method="POST">
                    <div class="form-group row">
                        <label for="kdwarehouse" class="col-md-3 col-form-label">Warehouse</b></label>
                        <div class="col-md-8">
                            <select name="kdwarehouse" id="kdwarehouse" class="form-control select-search" data-fouc>
                                <option value="">Show All</option>
                                <?php foreach ($warehouse as $wh) : ?>
                                    <?php if ($kdwarehouse == $wh->kdwarehouse) : ?>
                                        <option value="<?php echo $wh->kdwarehouse; ?>" selected><?php echo $wh->kdwarehouse; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $wh->kdwarehouse; ?>"><?php echo $wh->kdwarehouse; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="transdt" class="col-md-3 col-form-label">Date</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <input type="text" name="transdt" id="transdt" class="form-control daterange-basic" value="<?php echo $oldate == "" ? date('Y/m/d', $date) . '-' . date('Y/m/d') : $oldate; ?>">
                                <span class="input-group-append">
                                    <span class="input-group-text"><i class="icon-calendar22"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="" class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" name="filter" class="btn btn-primary btn-sm"><i class="fas fa-filter mr-1"></i> FIlter</button>
                            <input type="submit" name="export" class="btn btn-success btn-sm" value="Export">
                            <!-- <button type="submit" name="export" id="bt-export" class="btn btn-success btn-sm"><i class="fas fa-download mr-1"></i> Export</button> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6" style="font-size: 15px;">
                <div class="row ml-3" style="margin-bottom: 15px;">
                    <div class="col-sm-5 font-weight-bold">Total Qty <span class="float-right">:</span></div>
                    <div class="col-md-7"><?php echo $count->qty; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Report Item Keluar</h5>
    </div>
    <div class="card-body">
        <table id="order-listing" class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Item Name</th>
                    <th>Transaction ID</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Gudang</th>
                    <th>Action By</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $dt) : ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $dt->ItemName; ?></td>
                        <td><?php echo $dt->invusenum; ?></td>
                        <td><?php echo date('d M y', strtotime($dt->usedate)); ?></td>
                        <td><?php echo $dt->Type; ?></td>
                        <td><?php echo $dt->FromWarehouse; ?></td>
                        <td><?php echo $dt->SalesNm; ?></td>
                        <td><?php echo $dt->JumlahKeluar; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
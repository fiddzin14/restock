<script>
    var table;

    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            "ajax": {
                "url": "<?php echo base_url('ReportPenjualanAll/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
                "data": function(data) {
                    data.TransDt = $('#date').val();
                    data.kdwarehouse = $('#kdwh').val();
                    data.catname = $('#category').val();
                    data.KdItem = $('#kditem').val();
                    data.ItemName = $('#itemname').val();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('change', '#itemname', function() {
            var me = $(this).val();

            if (me == '') {
                $('#kditem').val('');
                $('#select2-kditem-container').text('Show All').attr('title', 'Show All');
            } else {
                $('#kditem').val(me);
                $('#select2-kditem-container').text(me).attr('title', me);
            }
        });

        $('body').on('change', '#kditem', function() {
            var me = $(this).val();
            if (me != '') {
                $.ajax({
                    url: "<?php echo base_url('reportPenjualanAll/getItemName'); ?>",
                    method: "POST",
                    data: {
                        item: me,
                    },
                    async: true,
                    dataType: 'json',
                    success: function(data) {
                        $('#itemname').val(data);
                        $('#select2-itemname-container').text(data).attr('title', data);
                    }
                });
            } else {
                $('#itemname').val('');
                $('#select2-itemname-container').text('Show All').attr('title', 'Show All');
            }
        });

        $('#btn-filter').click(function() {
            table.ajax.reload();
            var catname = $('#category').val();
            var item = $('#kditem').val();
            var date = $('#date').val();
            var kdwh = $('#kdwh').val();

            $.ajax({
                url: "<?php echo base_url('reportPenjualanAll/getCount'); ?>",
                method: "POST",
                data: {
                    catname: catname,
                    item: item,
                    date: date,
                    kdwarehouse: kdwh,
                },
                async: true,
                dataType: 'json',
                success: function(data) {
                    $('#hpp').text(data.hpp);
                    $('#total').text(data.total);
                    $('#qty').text(data.qty);
                    $('#profit').text(data.benefit);
                }
            });
        });

        // $('body').on('click', '#btexport', function(event) {
        //     event.preventDefault();

        //     var me = $(this);
        //     var url = me.attr('href');
        //     var catname = $('#category').val();
        //     var item = $('#kditem').val();
        //     var date = $('#date').val();

        //     $.ajax({
        //         url: url,
        //         method: "POST",
        //         data: {
        //             catname: catname,
        //             item: item,
        //             date: date,
        //         },
        //         success: function() {
        //             window.location.href = url;
        //         }
        //     });
        // });

        $('#loading').css('transform', 'translate(450px,550px)')
    });
</script>
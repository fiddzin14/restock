<script>
    $(document).ready(function() {
        $('body').on('click', '.btn-plus', function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            Swal.fire({
                title: 'Enter New User Menu',
                input: 'text',
                inputPlaceholder: 'Menu',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger',
                inputClass: 'form-control',
                inputValidator: function(value) {
                    return !value && 'This field is required!'
                }
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: {
                            menu: result.value,
                        },
                        async: true,
                        dataType: 'json',
                        success: function(response) {
                            if (response.status == false) {
                                Swal.fire({
                                    type: 'error',
                                    title: 'Warning...!',
                                    confirmButtonClass: 'btn btn-primary',
                                    text: 'This Menu has already exist',
                                });
                            } else {
                                Swal.fire({
                                    title: 'success!',
                                    text: 'Data has been save!',
                                    type: 'success',
                                    confirmButtonClass: 'btn btn-primary',
                                    timer: 2000
                                });
                                setTimeout(function() {
                                    window.location.href = '<?php echo base_url('menu') ?>';
                                }, 1000);
                            }
                        }
                    });
                }
            });
        });
        $('body').on('click', '.btn-del', function(event) {
            event.preventDefault();

            var me = $(this),
                url = me.attr('href'),
                title = me.attr('title');

            Swal.fire({
                title: 'Are you sure?',
                text: "you won't to revert this data!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        title: 'success!',
                        text: 'Data has been deleted!',
                        type: 'success',
                        confirmButtonClass: 'btn btn-primary',
                        timer: 2000
                    });
                    setTimeout(function() {
                        window.location.href = url;
                    }, 1000);
                }
            });
        });
    });
</script>
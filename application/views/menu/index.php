<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">User Menu</h5>
        <a href="<?php echo base_url('menu/add'); ?>" class="btn btn-primary btn-sm float-right btn-plus" title="Add Menu">
            <i class="fa fa-plus mr-1">
            </i>Add Menu
        </a>
    </div>
    <div class="card-body">
        <table id="datatables" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th width="50">#</th>
                    <th>Menu</th>
                    <th width="400">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($menu as $row) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $row->menu; ?></td>
                        <td>
                            <!-- <a href="menu/edit?id=<?= strtr(base64_encode($row->menuid), '+/=', '._-'); ?>" class="btn btn-sm btn-outline-primary" title="Edit Menu"><i class="fa fa-pencil"></i> Edit</a>&nbsp; -->
                            <a href="menu/delete?id=<?= strtr(base64_encode($row->menuid), '+/=', '._-'); ?>" class="btn btn-sm btn-outline-danger btn-del" title="Delete"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
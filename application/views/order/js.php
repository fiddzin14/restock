<script>
    var table;
    var count = 0;
    var previous;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('order/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
                "data": function(data) {
                    data.kdwh = $('#kdwh').val();
                    data.transdt = $('#transdt').val();
                }
            },

            "columnDefs": [{
                "targets": [0, -1],
                "orderable": false,
            }, ],
        });

        $('#loading').css('transform', 'translate(450px,580px)')

        $('#btn-filter').click(function() {
            table.ajax.reload();
        });

        $('#warehouse').on('change', function() {
            var warehouse = $(this).val();
            var item = $('.item').val();
            $('#warehouse').on('select2:selecting', function(evt) {
                previous = $('#warehouse').val();
            });
            if (item != null) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "you want to change warehouse? your curent input will be reset.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Reset!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        $('#slot').val('');
                        $('.table tbody tr').remove();
                        $.ajax({
                            url: "<?php echo base_url('order/getSlotByWarehouse'); ?>",
                            method: "POST",
                            data: {
                                warehouse: warehouse
                            },
                            async: true,
                            dataType: 'json',
                            success: function(data) {
                                var slot = '';
                                var warehouse = '';
                                var i;
                                for (i = 0; i < data.length; i++) {
                                    slot += '<option value="" class="text-secondary" id="">Select Slot</option>' +
                                        '<option value="' + data[i].slotid + '">' + data[i].slotnm + '</option>';
                                }
                                $('#slot').html(slot);
                            }
                        });
                    } else {
                        $('#warehouse').val(previous);
                        var text = $('#warehouse option[value="' + previous + '"]').text();
                        $('#select2-warehouse-container').text(text).attr('title', text);
                    }
                });
            } else {
                $.ajax({
                    url: "<?php echo base_url('order/getSlotByWarehouse'); ?>",
                    method: "POST",
                    data: {
                        warehouse: warehouse
                    },
                    async: true,
                    dataType: 'json',
                    success: function(data) {
                        var slot = '';
                        var i;
                        for (i = 0; i < data.length; i++) {
                            slot += '<option value="" class="text-secondary">Select Slot</option>' +
                                '<option value="' + data[i].slotid + '">' + data[i].slotnm + '</option>';
                        }
                        $('#slot').html(slot);
                    }
                });
            }
        });

        // $('#slot').change(function() {
        //     var warehouse = $('#warehouse').val();
        //     var slot = $(this).val();
        //     $.ajax({
        //         url: "<?php echo base_url('order/getItemBySlot'); ?>",
        //         method: "POST",
        //         data: {
        //             warehouse: warehouse,
        //             slot: slot
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var item = '';
        //             var i;
        //             for (i = 0; i < data.length; i++) {
        //                 item += '<option value="" class="text-secondary">Select Item</option> ' +
        //                     '<option value="' + data[i].kditem + '">' + data[i].kditem + ' ' + data[i].brand + ' ' + data[i].itemname + '</option>';
        //             }
        //             $('.item').html(item);
        //         }
        //     });
        //     return false;
        // });

        // $('#btplus').click(function() {
        //     var warehouse = $('#warehouse').val();
        //     var slot = $('#slot').val();
        //     $.ajax({
        //         url: "<?php echo base_url('order/getItemBySlot'); ?>",
        //         method: "POST",
        //         data: {
        //             warehouse: warehouse,
        //             slot: slot
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var item = '';
        //             var i;
        //             count += 1;
        //             for (i = 0; i < data.length; i++) {
        //                 item += '<option value="" class="text-secondary">Select Item</option> ' +
        //                     '<option value="' + data[i].kditem + '">' + data[i].kditem + ' ' + data[i].brand + ' ' + data[i].itemname + '</option>';
        //             }
        //             html += '<tr id="row' + count + '">' +
        //                 '<td class="col-inp">' +
        //                 '<select id="item' + count + '" data-placeholder="Select Item" name="item[]" class="form-control item select-search" data-fouc>' +
        //                 item +
        //                 '</select>' +
        //                 '</td>' +
        //                 '<td></td>' +
        //                 '<td></td>' +
        //                 '<td></td>' +
        //                 '<td><button class="btn btn-danger btn-sm btn-remove" id="' + count + '"><i class="fas fa-trash"></i></button></td>' +
        //                 '</tr>';
        //             $('#btplus').closest('tbody').append(html);
        //             $('.select-search').select2();
        //         }
        //     });
        //     return false;
        // });

        // $('body').on('change', '.item', function() {
        //     var me = $(this);
        //     var item = me.val();
        //     var warehouse = $('#warehouse').val();
        //     var slot = $('#slot').val();
        //     var id = me.attr('id');
        //     $("#" + id).closest('tr').find('input').remove();

        //     $.ajax({
        //         url: "<?php echo base_url('order/getSelectedItem'); ?>",
        //         method: "POST",
        //         data: {
        //             item: item,
        //             warehouse: warehouse,
        //             slot: slot,
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             count += 1;
        //             $("#" + id).closest('tr').children('td:nth-child(2)').html('<input type="text" id="price' + count + '" name="price[]" style="width:150px;" value="' + data.detail.hargajual + '" class="form-control price rp_table">');
        //             $("#" + id).closest('tr').children('td:nth-child(3)').html('<input type="number" id="qty' + count + '" name="qty[]" style="width:80px;" min="1" size="10" value="0" class="form-control qty">');
        //             $("#" + id).closest('tr').children('td:nth-child(4)').html('<input type="text" id="subtotal" name="total[]" value="0" style="width:200px;" class="form-control border-0 bg-white" readonly>');
        //             $("#" + id).closest('tr').append('<input type="hidden" name="hpp[]" value="' + data.detail.cost + '">')
        //             $("#" + id).closest('tr').append('<input type="hidden" class="totalhpp" name="totalhpp[]" value="0">')
        //             $("#" + id).closest('tr').append('<input type="hidden" name="uom[]" value="' + data.detail.issueunit + '">')
        //             $("#" + id).closest('tr').append('<input type="hidden" name="curbal[]" value="' + data.detail.curbal + '">')
        //         }
        //     });
        //     return false;
        // });

        $('#sItem').autocomplete({
            // source: "",
            source: function(request, response) {
                $.ajax({
                    url: "<?php echo base_url('order/getAutocomplete'); ?>",
                    method: "POST",
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse: $('#warehouse').val(),
                        slot: $('#slot').val(),
                        item: $('.item').serializeArray(),
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            select: function(event, ui) {
                $(this).val(ui.item.label);
                $("#addItem").click();
            }
        });

        $('#addItem').click(function(event) {
            event.preventDefault();

            var Sitem = $('#sItem').val();
            var itemSplit = Sitem.split(" ");
            var item = itemSplit[0];
            var itemSel = $('.item').serializeArray();
            if (itemSel == null) {
                itemSel == '';
            } else {
                itemSel == $('.item').serializeArray();
            }

            var warehouse = $('#warehouse').val();
            var slot = $('#slot').val();
            var id = $(this).attr('id');

            $('#warehouse').closest('.col-inp').find('.form-text').remove();
            $('#slot').closest('.col-inp').find('.form-text').remove();
            $('#sItem').closest('.col-inp').find('.form-text').remove();

            if (warehouse == '') {
                $('#warehouse').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Warehouse first.</strong></span>');
            } else if (slot == '') {
                $('#slot').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Slot first.</strong></span>');
            } else if (item == '') {
                $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Please select the Item first.</strong></span>');
            } else {
                $.ajax({
                    url: "<?php echo base_url('receive/getSelectedItem'); ?>",
                    method: "POST",
                    data: {
                        item: item,
                        itemSelect: itemSel,
                        warehouse: warehouse,
                        slot: slot,
                    },
                    async: true,
                    dataType: 'json',
                    success: function(data) {
                        // $('#sItem option[value="' + data.row.kditem + '"]').remove();
                        if (data.error == true) {
                            $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Item has been selected.</strong></span>');
                            $("#sItem").blur().focus();
                        } else if (data.row == null) {
                            $('#sItem').closest('.col-inp').append('<span class="form-text text-danger"><strong>Item not found.</strong></span>');
                            $("#sItem").blur().focus();
                        } else {
                            $('#sItem').val('');
                            var html = '';
                            var detail = '';
                            var i;
                            count += 1;
                            if (data.detail != null) {
                                detail += '<input type="hidden" name="curbal[]" value="' + data.detail.curbal + '">'
                            } else {
                                detail += '<input type="hidden" name="curbal[]" value="0">'
                            }
                            html += '<tr id="row' + count + '">' +
                                '<td>' + data.row.kditem + ' ' + data.row.itemname + '</td>' +
                                '<td><input type="text" id="price' + count + '" name="price[]" style="width:150px;" value="' + data.row.hargajual + '" class="form-control rpjs price"></td>' +
                                '<td><input type="number" autocomplete=o oninput="this.value = this.value.replace(/[^0-9.,]/g);" id="qty' + count + '" name="qty[]" min="1" style="width:80px;" size="10" value="0" class="form-control qty"></td>' +
                                '<td><input type="text" id="subtotal' + count + '" name="total[]" value="0" style="width:150px;" class="form-control sub border-0 bg-white" readonly></td>' +
                                '<td><button class="btn btn-danger btn-sm btn-remove" id="' + count + '"><i class="fas fa-trash"></i></button></td>' +
                                '<input type="hidden" name="item[]" class="item" value="' + data.row.kditem + '">' +
                                '<input type="hidden" name="uom[]" value="' + data.row.uom + '">' +
                                '<input type="hidden" name="hpp[]" value="' + data.row.hargamodal + '">' +
                                detail +
                                '</tr>';
                            $('tbody').append(html);
                            new AutoNumeric('#price' + count, 'integerPos');
                        }
                    }
                });
            }
            return false;
        });

        $('body').on('click', '.btn-remove', function() {
            var me = $(this).closest('tr');
            // var kditem = me.find('input.item').val();
            // var item = me.children('td:nth-child(1)').html();
            // $('#sItem').append('<option value="' + kditem + '">' + item + '</option>');

            me.remove();
        });

        $('body').on('change', '.price', function() {
            var me = $(this);
            var price = me.val();
            var id = me.attr('id');
            var qty = $("#" + id).closest('tr').find('input.qty').val();
            var total = me.closest('tr').find('input.sub');
            var totalid = total.attr('id');

            var subTotal = price.replace(/,/g, '') * parseInt(qty);
            total.val(subTotal);
            new AutoNumeric('#' + totalid, 'integerPos');
        });

        $('body').on('change', '.qty', function() {
            var me = $(this);
            var qty = me.val();
            var id = me.attr('id');
            var price = me.closest('tr').find('input.price').val();
            var item = me.closest('tr').find('.item').val();
            var warehouse = $('#warehouse').val();
            var slot = $('#slot').val();
            var total = me.closest('tr').find('input.sub');
            var totalid = total.attr('id');

            $.ajax({
                url: "<?php echo base_url('order/checkQty'); ?>",
                method: "POST",
                data: {
                    item: item,
                    warehouse: warehouse,
                    slot: slot,
                    qty: qty
                },
                async: true,
                dataType: 'json',
                success: function(response) {
                    if (response.status == true) {
                        var subTotal = price.replace(/,/g, '') * parseInt(qty);

                        total.val(subTotal);
                        new AutoNumeric('#' + totalid, 'integerPos');
                    } else if (response.status == false) {
                        $("#" + id).val(0);
                        total.val(0);
                        Swal.fire({
                            type: 'error',
                            title: 'Warning...!',
                            confirmButtonClass: 'btn btn-primary',
                            text: 'There is no Stock for this Item. Curent item Qty ' + response.data.curbal + ' ' + response.data.issueunit,
                        });

                    }
                }
            });
            return false;
        });

        $('body').on('click', '#btsave', function(event) {
            event.preventDefault();

            var form = $('#form'),
                url = form.attr('action'),
                method = 'POST';

            form.find('.form-text').remove();
            form.find('.form-group').removeClass('has-error');
            form.find('.form-control').removeClass('is-invalid');

            $.ajax({
                url: url,
                method: method,
                data: form.serialize(),
                success: function(response) {
                    if (response.status == false) {
                        if (response.error_item == true) {
                            Swal.fire({
                                type: 'error',
                                title: 'Warning...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Item field is required.',
                            });
                        } else if (response.error_qty == true) {
                            Swal.fire({
                                type: 'error',
                                title: 'Error...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Qty field and Price field is required.',
                            });
                        }
                        $.each(response.errors, function(key, value) {
                            $('.form-group').addClass('has-error');
                            $('#' + key)
                                .closest('.form-control')
                                .addClass('is-invalid')
                                .closest('.col-inp')
                                .addClass('has-error')
                                .append('<span class="form-text text-danger"><strong>' + value + '</strong></span>')
                        });
                    } else {
                        Swal.fire({
                            type: 'success',
                            title: 'success!',
                            text: 'Data has been saved!',
                            confirmButtonClass: 'btn btn-primary',
                            timer: 2000,
                        });

                        setTimeout(function() {
                            window.location.href = '<?php echo base_url('order') ?>';
                        }, 2000);
                    }
                },
                error: function(xhr) {
                    Swal.fire({
                        type: 'error',
                        title: 'Error...!',
                        confirmButtonClass: 'btn btn-primary',
                        text: 'Something went wrong! Please check your input.',
                    });
                }
            });
        });
    });
</script>

</body>

</html>
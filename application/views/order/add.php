<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Add Transaction</h5>
        <a href="<?= base_url('order'); ?>" id="btback" class="btn btn-warning btn-sm float-right" title="">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a>
        <a href="#" id="btsave" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-check-circle mr-1">
            </i>Submit
        </a>
    </div>
    <div class="card-body">
        <form class="ml-3 mb-5" id="form" method="POST" action="<?php echo base_url('order/create'); ?>">
            <div class="form-group row">
                <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse / Slot</label>
                <div class="col-sm-3 col-inp">
                    <select id="warehouse" data-placeholder="Select Warehouse" name="warehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-3 col-inp">
                    <select id="slot" data-placeholder="Select slot" name="slot" class="form-control select-search" data-fouc>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="customer" class="col-sm-2 col-form-label">*Customer</label>
                <div class="col-sm-6">
                    <select id="customer" data-placeholder="Select customer" name="customer" class="form-control select-search" data-fouc>
                        <?php foreach ($customer as $cust) : ?>
                            <option value="<?= $cust->custid; ?>"><?= $cust->custname; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="address" class="col-sm-2 col-form-label">Delivery Address</label>
                <div class="col-sm-6">
                    <textarea name="address" id="address" rows="3" class="form-control">-</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="memo" class="col-sm-2 col-form-label">Memo</label>
                <div class="col-sm-6">
                    <textarea name="memo" id="memo" rows="3" class="form-control">-</textarea>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label for="sItem" class="col-sm-2 col-form-label">Item</label>
                <div class="col-sm-6 col-inp">
                    <div class="input-group">
                        <input type="text" id="sItem" class="form-control border-right-0" placeholder="Search Item">
                        <span class="input-group-append">
                            <button class="btn btn-primary" id="addItem">Insert</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="memo" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary mr-2" id="btsave"><i class="fa fa-check-circle mr-1"></i>Submit</button>
                </div>
            </div> -->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
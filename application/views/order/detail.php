<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Detail Order Transaction</h5>
        <a href="print_invoice?id=<?= strtr(base64_encode($row->transid), '+/=', '._-'); ?>" target="_blank" class="btn print btn-danger btn-sm float-right" title="Add Item to Warehouse">
            <i class="fas fa-print mr-1">
            </i> Print Invoice
        </a>
    </div>
    <div class="card-body">
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">No Invoice <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->transid; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Customer <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->custname; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Date <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= date('d M Y', strtotime($row->transdt)); ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Address <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->deliveryaddress; ?>
            </div>
        </div>
        <div class="row ml-4 row-detail mb-3">
            <div class="col-sm-2 font-size-lg font-weight-bold">Memo <span class="float-right">:</span></div>
            <div class="col-sm-4 col-m">
                <?= $row->notes; ?>
            </div>
            <div class="col-sm-2 font-size-lg font-weight-bold">Number of Item <span class="float-right">:</span></div>
            <div class="col-sm-4">
                <?= $row->numofitem; ?>
            </div>
        </div>
        <table id="table" class="table mt-5 datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Code</th>
                    <th>Brand/Type</th>
                    <th>Spesifikasi</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->brand; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= rupiah($it->hargajual); ?></td>
                        <td><?= $it->qty; ?></td>
                        <td><?= rupiah($it->hargajual * $it->qty); ?></td>
                    </tr>
                    <?php $item[] = $it->hargajual * $it->qty; ?>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <th colspan="6">Sub Total :</th>
                <th><?= rupiah(array_sum($item)); ?></th>
            </tfoot>
        </table>
    </div>
</div>

<?php $this->load->view('_partials/footer'); ?>
<!-- <script>
    $('.print').click(function(e) {
        e.preventDefault();
        window.print();
    })
</script> -->
</body>

</html>
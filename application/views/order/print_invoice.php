<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <title>Print Invoice</title>

    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }

        header .header {
            font-size: 17px;
            font-weight: bold;
            text-transform: uppercase;
            margin-bottom: 7px;
            text-align: center;
        }

        header .sub-header {
            text-align: center;
            font-size: 17px;
            margin-bottom: 5px;
        }

        .sub {
            position: relative;
        }

        .tabl {
            float: left;
            margin-right: 120px;
        }

        .tabl td {
            font-size: 14px;
            padding-bottom: 10px;
            padding-right: 10px;
        }

        main {
            position: relative;
            top: 150px;
        }

        .table {
            width: 700px;
            border-collapse: collapse;
        }

        .table td {
            font-size: 11px;
        }

        .table th {
            font-size: 13px;
            border-bottom: 0.5px solid black;
            border-top: 0.5px solid black;
        }

        .table,
        .table td,
        .table th {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>

<body>
    <header>
        <div class="header">Nota Pembelian</div>
        <div class="sub-header">Gudang</div>
        <div class="sub-header">Harco Mangga dua Plaza no 18 Kemang Jakarta Selatan</div>
        <div class="sub-header" style="margin-bottom: 50px;">0891876128736</div>
    </header>

    <div class="sub">
        <table class="tabl" style="margin-left: 10px;">
            <tr>
                <td>No. Invoice</td>
                <td>:</td>
                <td><?= $row->transid; ?></td>
            </tr>
            <tr>
                <td>Date</td>
                <td>:</td>
                <td><?= date('d M Y', strtotime($row->transdt)); ?></td>
            </tr>
            <tr>
                <td>Memo</td>
                <td>:</td>
                <td><?= $row->notes == "" ? '-' : $row->notes; ?></td>
            </tr>
        </table>

        <table class="tabl" style="transform: translateY(-3px); width:500px">
            <tr>
                <td>Customer</td>
                <td style="width: 5px;">:</td>
                <td><?= $row->custname; ?></td>
            </tr>
            <tr>
                <td>Number Of Item</td>
                <td style="width: 5px;">:</td>
                <td><?= $row->numofitem; ?></td>
            </tr>
            <tr>
                <td style="min-height: 20px;width:100px;max-height:60px;vertical-align:top;">Address</td>
                <td style="width: 5px;vertical-align:top;">:</td>
                <td style="vertical-align:top;"><?= $row->deliveryaddress; ?></td>
            </tr>
        </table>
    </div>

    <main>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Item Code</th>
                    <th>Brand/Type</th>
                    <th>Spesifikasi</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($item as $it) : ?>
                    <tr>
                        <td><?= $it->kditem; ?></td>
                        <td><?= $it->brand; ?></td>
                        <td><?= $it->itemname; ?></td>
                        <td><?= rupiah($it->hargajual); ?></td>
                        <td><?= $it->qty; ?></td>
                        <td><?= rupiah($it->hargajual * $it->qty); ?></td>
                    </tr>
                    <?php $item[] = $it->hargajual * $it->qty; ?>
                <?php endforeach; ?>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5">Sub Total :</th>
                    <th><?= rupiah(array_sum($item)); ?></th>
                </tr>
            </tfoot>
        </table>
    </main>
</body>

</html>
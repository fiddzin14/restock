<script>
    var table;
    var count = 0;
    var previous;
    var tbl;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('opname/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
                "data": function(data) {
                    data.kdwh = $('#kdwh').val();
                    data.stodate = $('#date').val();
                }
            },

            "columnDefs": [{
                "targets": [0, -1],
                "orderable": false,
            }, ],
        });

        $('#loading').css('transform', 'translate(450px,580px)')

        $('#btn-filter').click(function() {
            table.ajax.reload();
        });

        $('#warehouse').on('change', function() {
            var warehouse = $(this).val();
            var item = $('.item').val();
            $('#warehouse').on('select2:selecting', function(evt) {
                previous = $('#warehouse').val();
            });

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('opname/getItemWarehouse') ?>',
                async: true,
                data: {
                    warehouse: warehouse
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#loading').show();
                },
                complete: function() {
                    $('#loading').hide();
                },
                success: function(data) {
                    for (i = 0; i < data.length; i++) {
                        $('#datatabl').DataTable().row.add([
                            data[i].kditem + '<input type="hidden" name="item[]" value="' + data[i].kditem + '">',
                            data[i].itemname,
                            data[i].curbal + '<input type="hidden" name="qtysys[]" value="' + data[i].curbal + '">',
                            '<input type="number" oninput="this.value = this.value.replace(/[^0-9.,]/g);" id="qty' + count + '" name="qtyact[]" min="1" style="width:80px;" value="0" class="form-control qty">' +
                            '<input type="hidden" name="cost[]" value="' + data[i].hargamodal + '">',
                            data[i].issueunit,
                            '<input type="text" oninput="this.value = this.value.replace(/[^0-9.,]/g);" id="qtyDiff' + count + '" name="qtydif[]" min="1" style="width:80px;" value="" class="form-control border-0 bg-white diff" readonly>',
                        ]).draw(false);
                        // html += '<tr>' +
                        //     '<td>' + data[i].kditem + '</td>' +
                        //     '<td>' + data[i].itemname + '</td>' +
                        //     '<td>' + data[i].curbal + '</td>' +
                        //     '<td><input type="number" oninput="this.value = this.value.replace(/[^0-9.,]/g);" id="qty' + count + '" name="qty[]" min="1" style="width:80px;" value="0" class="form-control qty"></td>' +
                        //     '<td>' + data[i].issueunit + '</td>' +
                        //     '<td></td>' +
                        //     '</tr>';
                    }
                }
            });
            // $(this).attr('disabled', 'disabled');
        });

        $('body').on('change', '.qty', function() {
            var me = $(this);
            var qty = me.val();
            var qtyDiff = me.closest('tr').find('input.diff');
            var qtySystem = me.closest('tr').children('td:nth-child(3)').text();
            var diff = qty - parseInt(qtySystem);

            qtyDiff.val(diff);
        });

        $('body').on('click', '#btsave', function(event) {
            event.preventDefault();

            var form = $('#form'),
                url = form.attr('action'),
                method = 'POST';

            form.find('.form-text').remove();
            form.find('.form-group').removeClass('has-error');
            form.find('.form-control').removeClass('is-invalid');

            $.ajax({
                url: url,
                method: method,
                data: form.serialize(),
                success: function(response) {
                    if (response.status == false) {
                        if (response.error_qty == true) {
                            Swal.fire({
                                type: 'error',
                                title: 'Error...!',
                                confirmButtonClass: 'btn btn-primary',
                                text: 'Qty field is required.',
                            });
                        }
                        $.each(response.errors, function(key, value) {
                            $('.form-group').addClass('has-error');
                            $('#' + key)
                                .closest('.form-control')
                                .addClass('is-invalid')
                                .closest('.col-inp')
                                .addClass('has-error')
                                .append('<span class="form-text text-danger"><strong>' + value + '</strong></span>')
                        });
                    } else {
                        Swal.fire({
                            type: 'success',
                            title: 'success!',
                            text: 'Data has been saved!',
                            confirmButtonClass: 'btn btn-primary',
                            timer: 1500,
                        });

                        setTimeout(function() {
                            window.location.href = '<?php echo base_url('opname') ?>';
                        }, 1500);
                    }
                },
                error: function(xhr) {
                    Swal.fire({
                        type: 'error',
                        title: 'Error...!',
                        confirmButtonClass: 'btn btn-primary',
                        text: 'Something went wrong! Please check your input.',
                    });
                }
            });
        });
    });
</script>
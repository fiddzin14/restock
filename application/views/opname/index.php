<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title">Filter</h5>
    </div>
    <div class="card-body">
        <form action="" method="POST" class="ml-3">
            <div class="form-group row">
                <label for="kdwh" class="col-sm-2 col-form-label">Warehouse</label>
                <div class="col-sm-5">
                    <select name="kdwh" id="kdwh" class="form-control select-search" data-fouc>
                        <option value="">Show All</option>
                        <?php foreach ($warehouse as $wh) : ?>
                            <option value="<?php echo $wh->kdwarehouse; ?>"><?php echo $wh->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-2 col-form-label">Periode</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" name="date" id="date" class="form-control daterange-basic" value="<?php echo date('Y/m/d', $date); ?>- <?php echo date('Y/m/d'); ?>">
                        <span class="input-group-append">
                            <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="kdwh" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-5">
                    <button type="button" name="filter" id="btn-filter" class="btn btn-primary btn-sm"><i class="fas fa-filter mr-1"></i> FIlter</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Stock Movement</h5>
        <a href="<?= base_url('opname/add'); ?>" class="btn btn-primary btn-sm float-right" title="Add Sales Order">
            <i class="fa fa-plus mr-1">
            </i>Add Data
        </a>
    </div>
    <div class="card-body">
        <table id="table" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Id Opname</th>
                    <th>Date</th>
                    <th>Notes</th>
                    <th>Status</th>
                    <th>Count Items</th>
                    <th>Warehouse</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
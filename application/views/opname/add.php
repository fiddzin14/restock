<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Add Stock Opname</h5>
        <a href="<?= base_url('opname'); ?>" id="btback" class="btn btn-warning btn-sm float-right" title="">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a>
        <a href="#" id="btsave" class="btn btn-primary mr-1 btn-sm float-right">
            <i class="fa fa-check-circle mr-1">
            </i>Submit
        </a>
    </div>
    <div class="card-body">
        <form class="ml-3 mb-5" id="form" method="POST" action="<?php echo base_url('opname/create'); ?>">
            <div class="form-group row">
                <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse</label>
                <div class="col-sm-6 col-inp">
                    <select id="warehouse" data-placeholder="Select Warehouse" name="warehouse" class="form-control select-search" data-fouc>
                        <option value="" class="text-secondary">Select Warehouse</option>
                        <?php foreach ($warehouse as $dt) : ?>
                            <option value="<?= $dt->kdwarehouse; ?>"><?= $dt->descwarehouse; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse</label>
                <div class="col-sm-6 col-inp">
                    <select id="slot" data-placeholder="Select slot" name="slot" class="form-control select-search" data-fouc>

                    </select>
                </div>
            </div> -->
            <div class="form-group row mb-5">
                <label for="notes" class="col-sm-2 col-form-label">Notes</label>
                <div class="col-sm-6">
                    <textarea name="notes" id="notes" rows="3" class="form-control">-</textarea>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="memo" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary mr-2" id="btsave"><i class="fa fa-check-circle mr-1"></i>Submit</button>
                </div>
            </div> -->
            <div class="table-responsive">
                <table id="datatabl" class="table">
                    <thead>
                        <tr>
                            <th>Item Code</th>
                            <th>Item Name</th>
                            <th>Qty(System)</th>
                            <th>Qty(Actual)</th>
                            <th>Uom</th>
                            <th>Qty Difference</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
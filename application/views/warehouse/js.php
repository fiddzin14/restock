<script>
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('warehouse/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('Master Warehouse');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add Warehouse');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('#card-title').text('Edit Warehouse');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                $('#card-title').text('Add Warehouse');
                $('#warehouse').val("");
                $('#desc').val("");
                $('#address').val("");
                $('#contact').val("");
                $('#form').attr('action', '<?php echo $action; ?>');
            }

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#idw').val(data.idwarehouse);
                    $('#warehouse').val(data.kdwarehouse);
                    $('#desc').val(data.descwarehouse);
                    $('#address').val(data.address);
                    $('#contact').val(data.contact);
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });
    });
</script>

</body>

</html>
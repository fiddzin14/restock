<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">Master Warehouse</h5>
        <?php if ($login->level == "ADMIN") : ?>
            <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add Warehouse">
                <i class="fa fa-plus mr-1">
                </i>Add Warehouse
            </a>
        <?php endif; ?>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="Master Warehouse" id="form" method="POST" action="<?php echo base_url('warehouse/add'); ?>">
                        <input type="hidden" name="idwarehouse" id="idw">
                        <div class="form-group row">
                            <label for="warehouse" class="col-sm-2 col-form-label">*Warehouse Code</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="warehouse" name="warehouse" class="form-control" placeholder="Warehouse">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="desc" class="col-sm-2 col-form-label">*Warehouse Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="desc" name="desc" class="form-control" placeholder="Warehouse Name">
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="address" class="col-sm-2 col-form-label">*Address</label>
                            <div class="col-sm-7 col-inp">
                                <textarea id="address" name="address" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact" class="col-sm-2 col-form-label">*Contact</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="contact" name="contact" class="form-control" placeholder="Contact">
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Warehouse Code</th>
                        <th>Warehouse Name</th>
                        <th>Address</th>
                        <th>Contact</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
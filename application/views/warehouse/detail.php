<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block">Filter Product</h5>
        <!-- <a href="<?= base_url('warehouse'); ?>" class="btn btn-primary btn-sm float-right" title="Back to Warehouse">
            <i class="fa fa-sign-out mr-1">
            </i>Back
        </a> -->
    </div>
    <div class="card-body">
        <form class="forms-sample" id="form" method="POST" action="">
            <div class="form-group row">
                <label for="slotid" class="col-sm-2 col-form-label">Slot Name</label>
                <div class="col-sm-4">
                    <select name="slotid" id="slotnm" class="form-control select-search" data-fouc>
                        <?php if ($countSlot > 1) : ?>
                            <option value="">Show All</option>
                        <?php endif; ?>
                        <?php foreach ($slot as $row) : ?>
                            <option value="<?= $row->slotid; ?>" <?php echo $slotid == $row->slotid ? 'selected' : '' ?>><?= $row->slotnm; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary btn-sm ml-2">Filter</button>
                </div>
                <div class="col-sm-4">
                    <h5 class="d-inline-block mt-1">Total Qty : <?php echo $countQty->qty == 0 ? "0" : $countQty->qty; ?></h5>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block" id="card-title">Item in <?php echo $row->kdwarehouse; ?></h5>
    </div>
    <div class="card-body">
        <table id="datatables" class="table datatable-responsive-column-controlled table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Code</th>
                    <th>Spesifikasi</th>
                    <th>Brand/Type</th>
                    <th>Satuan</th>
                    <th>QTY</th>
                    <th>Stock Type</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($product as $data) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $data->kditem; ?></td>
                        <td><?= $data->itemname; ?></td>
                        <td><?= $data->brand; ?></td>
                        <td><?= $data->issueunit; ?></td>
                        <td><?= $data->curbal; ?></td>
                        <td><?= $data->stocktype; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
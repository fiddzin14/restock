<script>
    var table;
    $(document).ready(function() {

        //datatables
        $('#table').DataTable({

            "processing": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            "serverSide": true,
            "order": [
                [1, 'asc']
            ],

            "ajax": {
                "url": "<?php echo base_url('users/ajax_list') ?>",
                "type": "POST",
                "beforeSend": function() {
                    $('#loading').show();
                },
                "complete": function() {
                    $('#loading').hide();
                },
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0, -1], //last column
                "orderable": false, //set not orderable
            }, ],

        });

        $('body').on('click', '.out', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('.add').addClass('collapse-show').removeClass('out');
            $('#collapseTable').collapse('show');
            $('#collapseAdd').collapse('hide');
            $('#card-title').text('User Login');
            $('.add').html('<i class="fa fa-plus mr-1"></i>Add User');
        });

        $('body').on('click', '.collapse-show', function(event) {
            event.preventDefault();

            var me = $(this),
                title = me.attr('title');
            url = me.attr('href');

            $('#username').removeAttr('readonly', 'readonly');
            $('#form').find('.form-text').remove();
            $('#form').find('.form-group').removeClass('has-error');
            $('#form').find('.form-control').removeClass('is-invalid');

            if (me.hasClass('edit')) {
                $('.pass').addClass('none');
                $('#card-title').text('Edit User');
                $('#form').attr('action', '<?php echo $action_edit; ?>');
            } else {
                $('#card-title').text('Add User');
                $('.pass').removeClass('none');
                $('#username').val("");
                $('#displayname').val("");
                $('#email').val("");
                $('#isActive').attr('checked', 'checked');
                $('#form').attr('action', '<?php echo $action; ?>');
            }
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#username').val(data.username);
                    $('#username').attr('readonly', 'readonly');
                    $('#displayname').val(data.displayname);
                    $('#email').val(data.email);
                    if (data.isactive == 1) {
                        $('#isActive').attr('checked', 'checked');
                    } else {
                        $('#isActive').removeAttr('checked', 'checked');
                    }
                }
            });

            $("html, body").animate({
                scrollTop: 0
            }, 500);
            $('.add').addClass('out').removeClass('collapse-show');
            $('.add').html('<i class="fa fa-sign-out mr-1"></i>Back');
            $('#collapseTable').collapse('hide');
            $('#collapseAdd').collapse('show');
        });

    });
</script>

</body>

</html>
<div class="card">
    <div class="card-header bg-white">
        <h5 class="card-title d-inline-block mt-1" id="card-title">User Login</h5>
        <a href="#" class="btn btn-primary btn-sm collapse-show act add float-right" title="Add User">
            <i class="fa fa-plus mr-1">
            </i>Add User
        </a>
    </div>
    <div class="collapse multi-collapse" id="collapseAdd">
        <div class="card-body">
            <div class="row ml-5">
                <div class="col-12">
                    <form class="forms-sample" title="User Login" id="form" method="POST" action="<?php echo base_url('users/add'); ?>">
                        <div class="form-group row">
                            <label for="username" class="col-sm-2 col-form-label">*Username</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="displayname" class="col-sm-2 col-form-label">*Display Name</label>
                            <div class="col-sm-7 col-inp">
                                <input type="text" id="displayname" name="displayname" class="form-control" placeholder="Display Name">
                            </div>
                        </div>
                        <div class="form-group row pass">
                            <label for="password" class="col-sm-2 col-form-label">*Password</label>
                            <div class="col-sm-7 col-inp">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group row pass">
                            <label for="confirmPassword" class="col-sm-2 col-form-label">*Confirm Password</label>
                            <div class="col-sm-7 col-inp">
                                <input type="password" id="confirmPassword" name="confirmPassword" class="form-control" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">*Email</label>
                            <div class="col-sm-7 col-inp">
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-2 col-form-label">*Level</label>
                            <div class="col-sm-7 col-inp">
                                <select id="level" name="level" class="form-control select-search" data-fouc>
                                    <?php foreach ($level as $row) : ?>
                                        <option value="<?= $row->id; ?>"><?= $row->level; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isActive" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="isActive" name="isactive" class="form-check-input" value="1">
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success btn-sm mr-2" id="btn-save">Save</button>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7 card-title">
                                (*)Mandatory
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse show" id="collapseTable">
        <div class="card-body">
            <table id="table" class="table datatable-responsive-column-controlled table-hover">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th>Username</th>
                        <th>Display Name</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>